## How to use this repo

Build, installation an run summary.

1. Build the RPM
    1. For 32bit builds: ant -q rpm 
    1. For 64bit builds: ant -q -Dbit=64 rpm
1. Install the rpm: sudo rpm -ivh build/XNotesNG*.rpm
1. Run the program: xnotesng

The application will start an load itself into the system tray.

I hope you enjoy XNotesNG!

Michael J. Hammel

mjhammel@graphics-muse.org

November 2009

## License

0BSD
