%define rev 1
%define xnotesngdir opt/XNotesNG

Name: XNotesNG
Summary: Next generation XNotesPlus java-based application environment.
Version: [--VERSION--]
Release: %{rev}
Source: %{name}-%{version}.tar.gz
Vendor: Michael J. Hammel
License: MIT
URL:  http://www.graphics-muse.org/
Packager: Michael J. Hammel <mjhammel@graphics-muse.org>

# Group is based on the Fedora Groups hierarchy.  See
# /usr/share/doc/rpm-<version>/GROUPS for details.
Group: Applications/Productivity

AutoReqProv: no
Prereq: bash
Requires: perl
BuildRoot: %{_tmppath}/%{name}-root

%description
This package contains the Java binary, libraries and associated files
required to run XNotesNG, a PIM application based on java and JPF.

%prep
%setup
                                                                                
%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/%{xnotesngdir}
mkdir -p $RPM_BUILD_ROOT/usr/bin

cp -r XNotesNG/* $RPM_BUILD_ROOT/%{xnotesngdir}
mkdir -p $RPM_BUILD_ROOT/%{xnotesngdir}/logs
mkdir -p $RPM_BUILD_ROOT/%{xnotesngdir}/temp
mkdir -p $RPM_BUILD_ROOT/%{xnotesngdir}/data
ln -s /%{xnotesngdir}/scripts/xnotesng.sh $RPM_BUILD_ROOT/usr/bin/xnotesng

%clean
rm -rf $RPM_BUILD_ROOT

%files
%attr(0755,root,root) /%{xnotesngdir}
%attr(0777,root,root) /%{xnotesngdir}/logs
%attr(0777,root,root) /%{xnotesngdir}/temp
%attr(0777,root,root) /%{xnotesngdir}/data
/usr/bin/xnotesng
/%{xnotesngdir}/scripts/xnotesng.sh
