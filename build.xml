<?xml version="1.0"?>
<!-- =============================================================
  -  XnotesNG Build File
  -  This build file is the driver for the full XNotesNG build.
  -  It uses include files found under config/build.
  -
  - Copyright (c) 2009 Michael J. Hammel
  -
  - Permission is hereby granted, free of charge, to any person obtaining a copy
  - of this software and associated documentation files (the "Software"), to deal
  - in the Software without restriction, including without limitation the rights
  - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  - copies of the Software, and to permit persons to whom the Software is
  - furnished to do so, subject to the following conditions:
  -
  - The above copyright notice and this permission notice shall be included in
  - all copies or substantial portions of the Software.
  -
  - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  - THE SOFTWARE.
  -  ============================================================= -->

<!-- Point to additional build configurations, which we'll import later. -->
<!DOCTYPE project [
   <!ENTITY version     SYSTEM "./config/build/version.xml">
   <!ENTITY tsMsg       SYSTEM "./config/build/tsMsg.xml">
   <!ENTITY taskdefs    SYSTEM "./config/build/taskdefs.xml">
   <!ENTITY common      SYSTEM "./config/build/common.xml">
   <!ENTITY core        SYSTEM "./config/build/core.xml">
   <!ENTITY apps        SYSTEM "./config/build/apps.xml">
   <!ENTITY javadoc     SYSTEM "./config/build/javadoc.xml">
   <!ENTITY rpm         SYSTEM "./config/build/rpm.xml">
   <!ENTITY deb         SYSTEM "./config/build/deb.xml">
   <!ENTITY tools       SYSTEM "./config/build/tools.xml">
]>


<project name="XNotesNG" default="all" basedir=".">

    <!-- Include the Ant Contrib packages, like "for" and "foreach" -->
    <taskdef resource="net/sf/antcontrib/antlib.xml">
        <classpath>
            <pathelement location="/usr/share/java/ant-contrib.jar"/>
        </classpath>
    </taskdef>

    <!-- ===============================================
      - Which platform type are we building for?
      - -Dbit=64    # Build for a 64 bit platform
      -  default of 32 means to build for 32 bit platforms
      ================================================== -->
    <property name="bit" value="64"/>

    <!-- =============================================================
       - Global configuration:  see scripts/cdtools
       ============================================================= -->
    <property environment="env"/>
    <property name="gm.home" value="${env.JAVA_HOME}"/>

    <!-- JAVA library. -->
    <property name="java.lib.dir" value="${gm.home}/lib"/>

    <!-- Library directories. -->
    <property name="lib.dir"        value="${basedir}/lib"/>
    <property name="lib.javadoc"    value="${lib.dir}/javadoc"/>
    <property name="lib.jakarta"    value="${lib.dir}/jakarta"/>
    <property name="lib.jpf"        value="${lib.dir}/jpf"/>
    <property name="lib.swt"        value="${lib.dir}/swt"/>

    <!-- Source code directories. -->
    <property name="src.dir"            value="${basedir}/com/ximba/xnotesng"/>
    <property name="src.dir.common"     value="${src.dir}/common"/>
    <property name="src.dir.appmgr"     value="${src.dir}/appmgr"/>
    <property name="src.dir.datamgr"    value="${src.dir}/datamgr"/>
    <property name="src.dir.launcher"   value="${src.dir}/launcher"/>
    <property name="src.dir.server"     value="${src.dir}/server"/>
    <property name="src.dir.tools"      value="${src.dir}/tools"/>


    <!-- Build and deploy directories. -->
    <property name="build.dir"          value="${env.GM_BUILD}"/>
    <property name="build.dir.pkg"      value="${build.dir}/pkg"/>
    <property name="dependency.dir"     value="${build.dir}/dependencies/"/>
    <property name="dir.scripts"        value="${basedir}/scripts"/>


    <!-- =============================================================
       - Common classpaths
       ============================================================= -->

    <!-- Libraries used for building and may or may not get installed with the pkg. -->
    <path id="classpath.jakarta">
        <fileset dir="${lib.jakarta}">
            <include name="*.jar"/>
        </fileset>
    </path>

    <!-- Installed libraries that get distributed with the package. -->
    <path id="classpath.installed">
        <fileset dir="${build.dir.pkg}/lib">
            <include name="*.jar"/>
        </fileset>
    </path>

    <!-- Library path that resolves libraries at run time. -->
    <path id="jnilib.path">
        <pathelement location="${build.dir.pkg}/lib" />
    </path>
    <pathconvert pathsep=":" property="libpath" refid="jnilib.path"/>


    <!-- =============================================================
       - Import target files specified above.
       - We do this here because some of the properties set eariler
       - need to be available before these files can be imported.
       ============================================================= -->
    &version;       <!-- Provide versioning information for the build. -->
    &tsMsg;         <!-- Build macros for output messages. -->
    &taskdefs;      <!-- Build taskdefs needed for the build. -->
    &common;        <!-- Build common library. -->
    &core;          <!-- The core plugin. -->
    &apps;          <!-- The application plugins. -->
    &javadoc;       <!-- Javadoc documentation. -->
    &rpm;           <!-- RPM packaging. -->
    &deb;           <!-- Debian packaging. -->
    &tools;         <!-- Generic build tools. -->

    <!-- =======================================================================
       - Initialization: Prepares the build directory.
       - ======================================================================= -->
    <target name="init" description="Initialize build directories">
        <tstamp prefix="init">
            <format property="today" pattern="yyyy-MM-dd"/>
            <format property="starttime" pattern="HH:mm:ss"/>
        </tstamp>
        <echo message="Build Started: ${init.today} @ ${init.starttime}"/>

        <mkdir dir="${build.dir}"/>
        <mkdir dir="${build.dir.pkg}"/>
        <mkdir dir="${dependency.dir}"/>

        <!-- Build the required TaskDefs, if needed. -->
        <antcall target="taskdefs" /> 
    </target>

    <!-- ==================================================================
       - Clean up
       - ================================================================== -->
    <target name="clean" 
        depends="common.clean,rpm.clean"
        description="Cleans up the build directory.">

        <delete dir="${build.dir}"/>

        <!--
          - "*.out" files are captures of the build output from the command line
          - or generated from cscope using MJHammel's bash functions.
          -->
        <delete>
            <fileset dir=".">
                <include name="**/*.out"/>
            </fileset>
        </delete>
    </target>

    <target name="clobber" depends="clean" description="Cleans up everything." >
        <delete file="lib/taskdefs/XNotesNGTaskDefs.jar"/>
    </target>


    <!-- ==================================================================
       - Build and run everything
       - ================================================================== -->
    <target name="xnotesng" description="Build everything." depends="apps" />

    <target name="xnotesng.run" depends="apps" description="Run the program">
        <java dir="${build.dir.pkg}" jar="${build.dir.pkg}/lib/jpf-boot.jar" fork="true" failonerror="true">
            <jvmarg value="-Djava.library.path=${libpath}"/>
            <arg value="-dataDir" /> 
            <arg value=".xnotesng-test" /> 
        </java>
    </target>

</project>

