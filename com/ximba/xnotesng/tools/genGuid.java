package tools;

import java.io.*;
import org.apache.commons.cli.*;

import com.ximba.xnotesng.common.*;

/**
 * <p>
 * Build tools:  Generate a random GUID string.
 * </p>
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class genGuid {

    private static final String S_OPTION_h = "Print this message.";
    private static final String S_OPTION_G = "Generate a GUID";

    private boolean doGuid = false;

    public void run(String[] args) {

        /* Parse command line arguments first. */
        parseArgs(args);

        if ( doGuid )
            System.out.println("Generated GUID: " + new RandomGUID().toString());
    }

    /*
     * ----------------------------------------------------------------
     * Parse command line arguments.
     * We use the Apache Commons CLI library for this.
     * ----------------------------------------------------------------
     */
    private void parseArgs(String[] args)
    {
        /* Define our boolean options */
        Option o_help1      = new Option( "?", S_OPTION_h );
        Option o_help2      = new Option( "h", false, S_OPTION_h );

        /* Define our options that take arguments */
        Option o_guid = new Option( "g", false, S_OPTION_G);

        Options options = new Options();
        options.addOption(o_help1);
        options.addOption(o_guid);

        /* create the parser */
        CommandLineParser parser = new GnuParser();
        CommandLine line = null;

        /* parse the command line arguments */
        try {
            line = parser.parse( options, args );
        }
        catch( ParseException exp ) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
            System.exit(1);
        }

        /* Print the help message and exit, if requested. */
        if ( (line.hasOption( "h" )) || (line.hasOption( "?" )) ) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "genGUID", options );
            System.exit(0);
        }

        /* Did they specify a remote server? */
        if ( line.hasOption( "g" ) )
            this.doGuid = true;
    }


    /*
     * main() has to be static, but that gets in the way of doing
     * anything useful.  So we create a new instance and call
     * the run method to get things going.
     */
    public static void main(String[] args) {
        new genGuid().run(args);
    }

}
