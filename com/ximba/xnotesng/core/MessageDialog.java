package com.ximba.xnotesng.core;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/**
 * <p> 
 * Display a message dialog.  Messages may be one of
 * <ul>
 * <li> Info </li>
 * <li> Error </li>
 * <li> Warning </li>
 * </ul>
 * </p>
 * @adm $Revision: 1.3 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class MessageDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.core.MessageDialog");

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private boolean          isRunning = false;
    private String           msg = "Unknown message";
    private int              mode = M_INFO;

    private static java.util.List<MessageDialog> dialogs = new ArrayList<MessageDialog>();

    public static final String S_TITLE_I = "Information";
    public static final String S_TITLE_W = "Warning";
    public static final String S_TITLE_E = "Error";
    public static final String S_TITLE_A = "Alarm";

    public static final int M_INFO  = 1;
    public static final int M_WARN  = 2;
    public static final int M_ERROR = 3;
    public static final int M_ALARM = 4;

    Image warnIcon = null;
    Image infoIcon = null;
    Image errorIcon = null;
    Image alarmIcon = null;

    /* Button Box */
    public static final int B_CLOSE = 0;
    public static final String[] bboxLabels  = {
        "Close",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires the parent for this dialog so it will show up
     * on top of that parent.
     * @param parent        Parent of this dialog.
     */
    public MessageDialog(Shell parent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        Map<String, Image> images = ApplicationManager.getImages();
        warnIcon = images.get("warnIcon");
        infoIcon = images.get("infoIcon");
        errorIcon = images.get("errorIcon");
        alarmIcon = images.get("alarmIcon");

        dialogs.add(this);
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /** Close any dialogs that might be open. */
    static public void closeAll()
    {
        Iterator it = dialogs.iterator();
        while (it.hasNext())
        {
            MessageDialog md = (MessageDialog)it.next();
            md.closeDialog();
        }
        dialogs.clear();
    }

    /** 
     * Set the mode this dialog should run in.  Valid values are
     * <ul>
     * <li> MessageDialog.M_WARN - dialog will show a warning icon. </li>
     * <li> MessageDialog.M_INFO - dialog will show an informational icon. </li>
     * <li> MessageDialog.M_ERROR - dialog will show an error icon. </li>
     * <li> MessageDialog.M_ALARM - dialog will show an alarm icon. </li>
     * </ul>
     * @param mode      The mode this dialog should run as.
     */
    public void setMode(int mode) 
    {
        if ( mode == M_WARN )       this.mode = mode;
        else if ( mode == M_INFO )  this.mode = mode;
        else if ( mode == M_ERROR ) this.mode = mode;                  
        else if ( mode == M_ALARM ) this.mode = mode;                  
    }

    /**
     * Set the message to display.
     * @param msg       The message to display in the dialog.
     */
    public void setMessage(String msg) 
    {
        this.msg = msg;
    }


    /** Close any dialogs that might be open. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /** Build and display the dialog. */
    public void open() 
    {
        isRunning = true;

        shell = new Shell(Display.getDefault());
        if ( mode == M_WARN )       shell.setText(S_TITLE_W);
        else if ( mode == M_INFO )  shell.setText(S_TITLE_I);
        else if ( mode == M_ERROR ) shell.setText(S_TITLE_E);
        else                        shell.setText(S_TITLE_A);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        shell.pack();
        shell.open();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if( !Display.getDefault().readAndDispatch() ) Display.getDefault().sleep();
        }

        shell.close();
        shell.dispose();
        dialogs.remove(this);
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 1 column. */
        gl = new GridLayout();
        gl.marginWidth = 2;
        gl.marginHeight = 2;
        gl.verticalSpacing = 15;
        gl.horizontalSpacing = 15;
        gl.numColumns = 3;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);

        label = new Label(shell, SWT.CENTER);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.horizontalIndent = 5;
        label.setLayoutData(gd);

        if ( mode == M_WARN )       label.setImage(warnIcon);
        else if ( mode == M_INFO )  label.setImage(infoIcon);
        else if ( mode == M_ERROR ) label.setImage(errorIcon);
        else                        label.setImage(alarmIcon);

        Label nameLabel = new Label(shell, SWT.WRAP);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.horizontalIndent = 5;
        gd.verticalIndent = 5;
        nameLabel.setLayoutData(gd);
        nameLabel.setText(msg);

        /* Blank spacer for right side. */
        label = new Label(shell, SWT.CENTER);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.horizontalIndent = 5;
        label.setLayoutData(gd);

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 3;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CLOSE: 
                            closeDialog();   
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
