package com.ximba.xnotesng.core;

/**
 * <p> 
 * Messages, usually passed in Exceptions.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class Messages {

    // public final static String S_INVALIDMODE = "Invalid mode.";

    /* Timer Manager */
    public final static String S_SCHEDFAILED   = "Scheduling failed: ";
    public final static String S_CANCELFAILED  = "Cancel failed: ";
    public final static String S_NAMENULL      = "Name field must not be null.";
    public final static String S_CBNULL        = "Callback field must not be null.";
    public final static String S_STARTNULL     = "Start field must not be null.";
    public final static String S_STARTFUTURE   = "Start field must represent a future date/time.";
    public final static String S_INVALIDREPEAT = "Invalid repeat type.";
    public final static String S_BADOFFSET     = "Offset type must not be set if repeat type is not TimerRepeatData.OFFSET.";
    public final static String S_INVALIDOFFSET = "Invalid offset type.";
    public final static String S_BADPERIOD     = "Offset period must not be set if offsetType is not set.";
    public final static String S_INVALIDPERIOD = "Offset period must > 0.";
    public final static String S_BADDOW        = "Day of week must not be set if repeat type is not TimerRepeatData.DAYOFWEEK";
    public final static String S_INVALIDDOW    = "Invalid day of week requested.";
    public final static String S_UNTILFUTURE   = "Repeat end date must be a future date/time.";
    public final static String S_EVENTEXISTS   = "TimerEvent name already exists.";
    public final static String S_IDEXISTS      = "TimerEvent ID already exists.";
    public final static String S_INVALIDTIMER  = "Invalid TimerEvent: ";
    public final static String S_REGFAILED     = "Registration failed: ";
    public final static String S_NOSUCHTIMER   = "No such timer.";

}
