package com.ximba.xnotesng.core;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* JPF */
import org.java.plugin.boot.Boot;
import org.java.plugin.Plugin;
import org.java.plugin.PluginManager;
import org.java.plugin.registry.Extension;
import org.java.plugin.registry.ExtensionPoint;
import org.java.plugin.registry.PluginDescriptor;
import org.java.plugin.registry.PluginRegistry;
import org.java.plugin.registry.Extension.Parameter;
import org.java.plugin.util.ExtendedProperties;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;


/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.core.ApplicationRegistration.MenuRegistration;


/**
 * <p> 
 * Load and make availble the application manager plugins.
 * </p>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.3 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class ApplicationManager {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.core.ApplicationManager");

    /* Our plugin name, as defined in the plugin.xml. */
    private static final String PLUGIN_NAME = "com.ximba.xnotesng.core";

    /* The classname required of all application plugins. */
    private static final String uiClassname = "Application";

    CoreInitData cid = null;
    boolean isRunning = false;

    /* Plugin management. */
    private PluginManager pm = null;
    private PluginRegistry pr = null;
    private Plugin plugin = null;
    private PluginDescriptor pd = null;
    private String home = null;
    private String id = null;
    private ExtensionPoint extPoint = null;
    private Collection connectedExtensions = null;

    /* The launcher plugin, when we find it. */
    ApplicationInterface launcher = null;

    /* UI Plugins. */
    private static java.util.List<ApplicationInterface> plugins = new LinkedList<ApplicationInterface>();
    private static java.util.List<ApplicationRegistration> pluginRegistrations = new LinkedList<ApplicationRegistration>();

    /* Menus */
    public class UIMenu {
        public Menu menu;
        public Map<String, UIMenu> map;
        public UIMenu(Menu t, Map<String, UIMenu> m) { menu = t; map =m; }
    }
    Map<String, UIMenu> uimenus = new HashMap<String, UIMenu>();

    /* Default application callback. */
    private Callback defaultAppCB = null;

    /* SWT Widgets */
    Shell shell = null;
    Menu menu = null;

    /* Icons */
    String trayIconFile = null;
    String warnIconFile = null;
    String infoIconFile = null;
    String errorIconFile = null;
    String alarmIconFile = null;

    Image trayIcon = null;
    Image warnIcon = null;
    Image infoIcon = null;
    Image errorIcon = null;
    Image alarmIcon = null;

    static Map<String, Image> subImages = new HashMap<String, Image>();

    private static final String TT_SYSTRAY = "XNotesNG";

 
    /**
     * =======================================================
     * Constructor
     * @param cid   Global instance of the CoreInitData object.
     * =======================================================
     */
    public ApplicationManager( CoreInitData cid ) { 
        init (cid);
    }

    /*
     * =======================================================
     * Private methods.
     * =======================================================
     */

    /**
     * The init method allows a manager plugin to do some preparation before
     * the start() method loads the ApplicationManager plugins.
     * @param cid   Global instance of the CoreInitData object.
     */
    private void init( CoreInitData cid ) {
        this.cid = cid;

        /* We have to ask the plugin manager to get ourself - seems weird, but it's the only way this works. */
        try {
            pm = cid.getManager();
            pr = pm.getRegistry();
            plugin = pm.getPlugin(PLUGIN_NAME);
            pd = plugin.getDescriptor();
            home = pd.getLocation().getPath();
            home = home.substring(0,home.indexOf("plugin.xml"));
            id = pd.getId();
            extPoint = pr.getExtensionPoint(id, "Application");
            connectedExtensions = extPoint.getConnectedExtensions();
        }
        catch (Exception e) {
            System.err.println( e.getMessage() );
            System.exit( 1 );
        }

        /**
         * Find the icons, if any. 
         */
        try {
            Display display = Display.getDefault();
            trayIconFile  = home+pd.getAttribute("trayIcon").getValue();
            warnIconFile  = home+pd.getAttribute("warnIcon").getValue();
            infoIconFile  = home+pd.getAttribute("infoIcon").getValue();
            errorIconFile = home+pd.getAttribute("errorIcon").getValue();
            alarmIconFile = home+pd.getAttribute("alarmIcon").getValue();

            if (!new File(warnIconFile).exists()) {
                log.error("XNotes warn icon not found: filename = " + warnIconFile);
            } else {
                warnIcon = new Image(Display.getDefault(), warnIconFile);
            }
            if (!new File(infoIconFile).exists()) {
                log.error("XNotes info icon not found: filename = " + infoIconFile);
            } else {
                infoIcon = new Image(Display.getDefault(), infoIconFile);
            }
            if (!new File(errorIconFile).exists()) {
                log.error("XNotes error icon not found: filename = " + errorIconFile);
            } else {
                errorIcon = new Image(Display.getDefault(), errorIconFile);
            }
            if (!new File(alarmIconFile).exists()) {
                log.error("XNotes alarm icon not found: filename = " + alarmIconFile);
            } else {
                alarmIcon = new Image(Display.getDefault(), alarmIconFile);
            }
            subImages.put("warnIcon", warnIcon);
            subImages.put("infoIcon", infoIcon);
            subImages.put("errorIcon", errorIcon);
            subImages.put("alarmIcon", alarmIcon);
        }
        catch (Exception e) {
            System.err.println( e.getMessage() );
            System.exit( 1 );
        }

    }

    /*
     * Load and register the application plugins.
     */
    private void registerPlugins() throws Exception
    {
        Iterator it = connectedExtensions.iterator();
        while ( it.hasNext() ) {
            Extension ext = (Extension)it.next();

            /* Look for the requested plugin. */
            if ( !uiClassname.equals(ext.getParameter("category").valueAsString()) )
                continue;

            log.info ("Registering ApplicationManager plugin: " + ext.getParameter("name").valueAsString());

            /* Activate the plugin. */
            pm.activatePlugin( ext.getDeclaringPluginDescriptor().getId() );

            /* Get the plugin class loader. */
            ClassLoader classLoader = pm.getPluginClassLoader( ext.getDeclaringPluginDescriptor() );

            /* Load the class. */
            Class<?> cls = classLoader.loadClass( ext.getParameter("class").valueAsString() );

            /* Create an instance of the class. */
            ApplicationInterface am = (ApplicationInterface) cls.newInstance();
            plugins.add( am );
            am.init(cid);

            /* Register the plugin: this lets us know what features it offers. */
            ApplicationRegistration ar = am.register();
            if ( ar != null )
            {
                ar.setName(ext.getParameter("name").valueAsString());
                if (ext.getParameter("position") != null )
                    ar.setPosition(Integer.parseInt(ext.getParameter("position").valueAsString()));
                insertRegistration( ar );
            }
        }
    }

    /**
     * Insert a registration into the appropriate location in the list of registrations.
     * @param ar    The application registration returned from a plugin's register() method.
     */
    private void insertRegistration( ApplicationRegistration ar )
    {
        /* If the position is not specified, it goes at the end of the list. */
        if (ar.getPosition() == -1)
        {
            pluginRegistrations.add(ar);
            return;
        }

        int index = -1;
        Iterator it = pluginRegistrations.iterator();
        while (it.hasNext())
        {
            ApplicationRegistration par = (ApplicationRegistration)it.next();
            if ( (par.getPosition() == -1) || (par.getPosition() >= ar.getPosition()) )
            {
                index = pluginRegistrations.indexOf(par);
                break;
            }
        }
        if ( index == -1 ) pluginRegistrations.add(ar);
        else               pluginRegistrations.add(index,ar);
    }


    /**
     * Create the menu that will go in the System Tray.
     */
    private void createMenu()
    {
        shell = new Shell( Display.getDefault() );
        menu = new Menu (shell, SWT.POP_UP);

        Iterator it = pluginRegistrations.iterator();
        while ( it.hasNext() )
        {
            final ApplicationRegistration ar = (ApplicationRegistration) it.next();
            java.util.List<MenuRegistration> menuPaths = ar.getMenus();
            Iterator menuIT = menuPaths.iterator();
            while (menuIT.hasNext())
            {
                /* The SAX parser doesn't like & in the plugin.xml, so we use underscores there and replace them here. */
                final MenuRegistration mr = (MenuRegistration)menuIT.next();
                if ( mr.getMenuPath() == null)
                {
                    log.error(ar.getName() + ": Null path - skipping.");
                    continue;
                }
                String menuPath = mr.getMenuPath().replace("_", "&");
                log.info ("Adding menu path: " + menuPath);
                addMenu(menu, menuPath, mr.getCallback(), ar.getName(), ar.getDescription());

                /* Check if this is the default app. */
                menuPath = mr.getMenuPath().replace("_", "").toLowerCase();
                if (menuPath.equals( cid.getDefaultApp().toLowerCase() ))
                    defaultAppCB = mr.getCallback();
            }
        }

        /* Quit comes last. */
        MenuItem item = new MenuItem (menu, SWT.PUSH);
        item.setText ("Quit");
        item.addListener (SWT.Selection, new Listener () {
            public void handleEvent (Event e) {
                shutdown();
            }
        });
    }

    /*
     * Add a menu path and its eventual menu item to the system tray menus.
     * @param mainMenu      The root menu of the system tray.
     * @param path          A path string to add to the menu of of the format
     *                      /toplevel/nextlevel/.../menuitem, with underscores inserted for
     *                      menu mnemonics.
     * @param cb            The callback method that will be called when the menu item is selected.
     * @param pluginName    The name of the plugin that is adding the menu entry.
     * @param pluginDesc    The description of the plugin that is adding the menu entry.
     */
    private void addMenu(Menu mainMenu, String path, final Callback cb, final String pluginName, final String pluginDesc)
    {
        MenuItem menuitem;

        String[] fields = path.split("/");
        if ( (fields == null) || (fields.length < 1) )
        {
            log.error(pluginName + ": menu path is too short.");
            return;
        }

        /* If the top level menu does not exist, add it. */
        UIMenu uim = uimenus.get(fields[0].replace("&", ""));
        if ( uim == null )
        {
            if ( fields.length > 1 )
            {
                menuitem = new MenuItem(mainMenu, SWT.CASCADE);
                menuitem.setText(fields[0]);
                Menu menu = new Menu(shell, SWT.DROP_DOWN);
                menuitem.setMenu(menu);
                uimenus.put(fields[0].replace("&",""), new UIMenu(menu, new HashMap<String, UIMenu>()));
                uim = uimenus.get(fields[0].replace("&",""));
            }
            else
            {
                /* Add the final menu item. */
                menuitem = new MenuItem(mainMenu, SWT.CASCADE);
                menuitem.setText(fields[fields.length-1]);
                menuitem.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        cb.run();
                    }
                });
                return;
            }
        }
        if ( fields.length == 1 )
        {
            log.info("Only one menu level specified, but entry already exists: " + fields[0]);
            return;
        }

        /* Add submenus, if needed. */
        Menu lastMenu = uim.menu;
        for(int i=1; i<fields.length-1; i++)
        {
            /* Doe this submenu already exist? */
            if ( uim.map.containsKey(fields[i].replace("&","")) )
                uim = (UIMenu)uim.map.get(fields[i].replace("&",""));
            else
            {
                /* Create the submenu. */
                menuitem = new MenuItem(uim.menu, SWT.CASCADE);
                menuitem.setText(fields[i]);
                Menu menu = new Menu(uim.menu);
                menuitem.setMenu(menu);
                UIMenu uim2 = new UIMenu(menu, new HashMap<String, UIMenu>());
                uim.map.put(fields[i].replace("&",""), uim2);
                uim = uim2;
            }
            lastMenu = uim.menu;
        }

        /* Add the final menu item. */
        menuitem = new MenuItem(lastMenu, SWT.CASCADE);
        menuitem.setText(fields[fields.length-1]);
        menuitem.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                cb.run();
            }
        });
    }


    /**
     * Create the tray icon, re: the Launcher, from which all applications are accessed.
     */
    private void createTray()
    {
        Image trayImage = new Image (Display.getDefault(), trayIconFile);
        final Tray tray = Display.getDefault().getSystemTray();
        if (tray == null) {
            log.error("System Tray is unavailable.");
            System.exit(1);
        } 
        else {
            final TrayItem item = new TrayItem (tray, SWT.NONE);
            item.setToolTipText(TT_SYSTRAY);
            item.setImage (trayImage);

            /*
             * A right click on the tray icon toggles the visibility
             * of the main menu.  A left double-click posts a new note.
             */
            item.addSelectionListener (new SelectionListener () {
                public void widgetDefaultSelected (SelectionEvent event) { 
                    if ( defaultAppCB != null )
                        defaultAppCB.run();
                }
                public void widgetSelected (SelectionEvent event) { }
            });

            item.addMenuDetectListener (new MenuDetectListener () {
                public void menuDetected (MenuDetectEvent event) { 
                    menu.setVisible (true);
                    while (!menu.isDisposed () && menu.isVisible ()) {
                        if (!Display.getDefault().readAndDispatch ()) Display.getDefault().sleep ();
                    }
                    menu.setVisible (false);
                }
            });

        } 
    }


    /*
     * =======================================================
     * Static utilities
     * =======================================================
     */

    /**
     * Utility routine that retrieves an attribute for a plugin.
     * @param   cid         The reference to the global CoreInitData object.
     * @param   pluginName  The name of the plugin making the request.  Used to find the JPF refernce to the plugin.
     * @param   name        The attribute string to retrieve from the plugins plugin.xml file.
     * @return  The value associated with the named attribute, or null if the attribute cannot be found.
     */
    public static String getAttr(CoreInitData cid, String pluginName, String name)
    {
        /* Find the home directory for the plugin. */
        try {
            PluginManager pm = cid.getManager();
            Plugin plugin = pm.getPlugin(pluginName);
            PluginDescriptor pd = plugin.getDescriptor();
            return pd.getAttribute(name).getValue();
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Utility routine that allows plugins to find their home directory.
     * @param   cid         The reference to the global CoreInitData object.
     * @param   pluginName  The name of the plugin making the request.  Used to find the JPF refernce to the plugin.
     * @return  The fully qualified path to the directory where the plugin is installed.
     */
    public static String getHome(CoreInitData cid, String pluginName)
    {
        /* Find the home directory for the plugin. */
        try {
            PluginManager pm = cid.getManager();
            Plugin plugin = pm.getPlugin(pluginName);
            PluginDescriptor pd = plugin.getDescriptor();
            String path = pd.getLocation().getPath();
            return path.substring(0,path.indexOf("plugin.xml"));
        }
        catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    /** Retrieve the images configured for this plugin, if any.  */
    public static Map<String, Image> getImages() { return subImages; }

    /*
     * =======================================================
     * Manager interface
     * =======================================================
     */


    /**
     * Shutdown is used to call the cleanup routines for all plugins managed by 
     * the Application manager
     */
    public void shutdown( ) {
        MessageDialog.closeAll();
        Iterator it = plugins.iterator();
        while( it.hasNext() )
        {
            ApplicationInterface am = (ApplicationInterface) it.next();
            am.shutdown();
        }
        isRunning = false;
    }

    /**
     * Start finds and loads all Application Manager plugins.  It also makes these available
     * to any other plugin that wants to access them.
     */
    public void start( ) {

        /* Load the application plugins */
        try { registerPlugins(); }
        catch (Exception e) {
            log.error(e);
            e.printStackTrace();
            System.err.println("Failed to load Application Manager plugins: " + e.getMessage() );
            System.exit(1);
        }

        /* Create the menu that will go in the System Tray. */
        createMenu();

        /* Create the System Tray entry. */
        createTray();

        /* Hide the splash image. */
        if ( Boot.getSplashHandler() != null )
            Boot.getSplashHandler().setVisible(false);

        /* Wait for an exit request. */
        isRunning = true;
        while (isRunning)
            if ( !Display.getDefault().readAndDispatch() ) Display.getDefault().sleep();
        shutdown();
    }
}
