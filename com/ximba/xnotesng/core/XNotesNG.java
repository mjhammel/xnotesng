package com.ximba.xnotesng.core;

import java.io.*;
import java.util.*;
import java.net.*;
import org.apache.log4j.Logger;
import org.apache.commons.cli.*;

/* JPF Imports */
import org.java.plugin.boot.Application;
import org.java.plugin.boot.ApplicationPlugin;
import org.java.plugin.boot.Boot;
import org.java.plugin.registry.Extension;
import org.java.plugin.registry.ExtensionPoint;
import org.java.plugin.registry.PluginDescriptor;
import org.java.plugin.registry.Extension.Parameter;
import org.java.plugin.util.ExtendedProperties;

/* XNotesNG */
import com.ximba.xnotesng.common.*;

/**
 * <p> 
 * The core is the main() for XNotesNG.  It's purpose is as follows:
 * <ul>
 * <li> Initialize and start up the JPF subsystem.
 * <li> Load and start up the Application Manager
 * </ul>
 * </p>
 * <p>
 * XNotesNG is based on the Java Plugin Framework, which means this main class
 * doesn't run on its own but rather is called from the framework.
 * </p>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.1.1.1 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class XNotesNG extends ApplicationPlugin implements Application {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.core.XNotesNG");
    private static final String CORE_ID = "655B4C52-36A7-7D9F-7807-EBF555C94A13";

    /* Prevent restart. */
    private static boolean pluginStarted = false;

    /* Saved configuration data. */
    private static CoreInitData cid = null;
    private String[] savedArgs = null;

    /* Where xnotes data is stored. Configured in boot.properties. */
    private String xnotesFolder = null;
    private String version = "tbd";
    private boolean saveOnExit = false;
    private boolean autoSave = false;
    private boolean verbose = false;
    private String userid = "";
    private String passwd = "";
    private String server = "";

    /* Help strings. */
    String S_OPTION_VERBOSE         = "Enable verbose output";
    String S_OPTION_H               = "Print this message";
    String S_OPTION_SV              = "Enabled save on exit";
    String S_OPTION_NA              = "Disables the auto save feature";
    String S_OPTION_INTERVAL        = "Specifies the auto-save interval";
    String S_OPTION_DATADIR         = "Folder to store XNotesNG files";
    String S_OPTION_CASCADEOFFSET   = "Offset, in pixels, for X,Y coords of cascaded notes";
    String S_OPTION_NOAUDIO         = "Disable audio for alarms";
    String S_OPTION_VERSION         = "Display XNotesNG version";
    String S_OPTION_USER            = "Userid for login to remote server";
    String S_OPTION_PW              = "Password for login to remote server";
    String S_OPTION_SERVER          = "Remote server";

    /*
     * =======================================================
     * Private methods required by this plugin.
     * =======================================================
     */

    /** Parse the command line. */
    private void parseCmdLine(String[] args)
    {
        /* Save all of the arguments for other plugins to parse. */
        savedArgs = new String[args.length];
        for(int i=0; i<args.length; i++)
            savedArgs[i] = args[i];

        /* Define our boolean options */
        Option o_help1      = new Option( "?", S_OPTION_H );
        Option o_help2      = new Option( "h", false, S_OPTION_H );
        Option o_verbose    = new Option( "verbose", false, S_OPTION_VERBOSE);
        Option o_version    = new Option( "version", false, S_OPTION_VERSION);
        Option o_saveOnExit = new Option( "sv", false, S_OPTION_SV);
        Option o_autoSave   = new Option( "na", false, S_OPTION_NA);

        /* Define our options that take arguments */
        Option o_dataDir        = new Option( "dataDir", true, S_OPTION_DATADIR);

        // Option o_saveInterval   = new Option( "interval", true, S_OPTION_INTERVAL);
        // Option o_cascadeOffset  = new Option( "cascadeOffset", true, S_OPTION_CASCADEOFFSET);
        // Option o_noAudio        = new Option( "noAudio", true, S_OPTION_NOAUDIO);
        // Option o_userid         = new Option( "user", true, S_OPTION_USER);
        // Option o_password       = new Option( "pw", true, S_OPTION_PW);
        // Option o_server         = new Option( "server", true, S_OPTION_SERVER);

        Options options = new Options();
        options.addOption(o_help1);
        options.addOption(o_help2);
        options.addOption(o_verbose);
        options.addOption(o_version);
        options.addOption(o_saveOnExit);
        options.addOption(o_autoSave);
        options.addOption(o_dataDir);

        // options.addOption(o_userid);
        // options.addOption(o_password);
        // options.addOption(o_server);

        // options.addOption(o_saveInterval);
        // options.addOption(o_cascadeOffset);
        // options.addOption(o_noAudio);

        /* create the parser */
        CommandLineParser parser = new GnuParser();
        CommandLine line = null;

        /* parse the command line arguments */
        try {
            line = parser.parse( options, args );
        }
        catch( ParseException exp ) {
            String eMsg = "Parsing failed.  Reason: " + exp.getMessage();
            log.error( eMsg );
            System.err.println( eMsg );
            System.exit(1);
        }

        /* Grab the user id and password. */
        if ( line.hasOption( "verbose" ) ) verbose = true;
        if ( line.hasOption( "saveOnExit" ) ) saveOnExit = true;
        if ( line.hasOption( "autoSave" ) ) autoSave = true;
        if ( line.hasOption( "dataDir" ) ) xnotesFolder = line.getOptionValue( "dataDir" );

        if ( line.hasOption( "user" ) ) userid = line.getOptionValue( "userid" );
        if ( line.hasOption( "pw" ) ) passwd = line.getOptionValue( "pw" );
        if ( line.hasOption( "server" ) ) server = line.getOptionValue( "server" );

        /* Print the version, if requested. */
        if ( line.hasOption( "version" ) ) {
            System.err.println(BuildInfo.getVersion() );
            System.exit(0);
        }

        /* Print the help message and exit, if requested. */
        if ( (line.hasOption( "h" )) || (line.hasOption( "?" )) ) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "XNotesNG", options );
            System.exit(0);
        }

    }


    /*
     * =======================================================
     * Public methods called by JPF
     * =======================================================
     */

    /**
     * @see org.java.plugin.Plugin#doStart()
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * @see org.java.plugin.Plugin#doStop()
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }

    /**
     * Core initialization.  This gets called first by the JPF Boot Library.
     * @param config    The plugin specific parameters specified in the boot.properties file.
     * @param args      The command line args passed to the JPB Boot Library at startup time.
     * @return The application plugin class.
     * @see org.java.plugin.boot.ApplicationPlugin#initApplication(ExtendedProperties, String[])
     */
    @Override
    protected Application initApplication(final ExtendedProperties config, final String[] args) throws Exception {

        /* Prevent plugins from restarting application. */
        if (pluginStarted) return null;

        /* Parse command line options. */
        parseCmdLine(args);

        /* Find the default application to run with a double click. */
        String defaultApp = config.getProperty("defaultApp");
        String todoFilename = config.getProperty("todoFilename");

        /* Save important settings for other plugins to query. */
        cid = new CoreInitData(getManager(), xnotesFolder, userid, passwd, server, 
                savedArgs, verbose, saveOnExit, autoSave, todoFilename, defaultApp);

        return this;
    }

    /**
     * Called by the JPF Library to run the plugin.  This plugin uses this method to setup and call the
     * Client Shell plugin.
     * @see org.java.plugin.boot.Application#startApplication()
     */
    public void startApplication() throws Exception {

        /* Prevent plugins from restarting application. */
        if (pluginStarted) return;

        /* Initialize the CacheMgr */
        CacheMgr.init(cid);

        /* Start the UI. This does not return until the UI exits. */
        pluginStarted = true;
        log.info("Starting XNotesNG core...");
        ApplicationManager am = new ApplicationManager(cid);
        am.start();

        /* The application is exiting.  Clean up. */
        log.info("XNotesNG core is exiting...");
        TimerManager tm = cid.getTimerManager();
        tm.shutdown();
        pluginStarted = false;
    }
}

