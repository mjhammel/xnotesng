package com.ximba.xnotesng.core;

import com.ximba.xnotesng.core.*;

/**
 * <p>
 * Provide registration information for application plugins.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

import java.util.*;
import org.apache.log4j.Logger;
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.CoreInitData;

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class ApplicationRegistration {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.managers.ApplicationRegistration");

    /** Used internally to map a path to a callback. */
    public class MenuRegistration {
        public String menuPath;
        public String toolTip = null;
        public Callback callback;
        public MenuRegistration(String path, Callback callback) {
            this.menuPath = path;
            this.callback = callback;
        }
        public MenuRegistration(String path, String toolTip, Callback callback) {
            this.menuPath = path;
            this.toolTip = toolTip;
            this.callback = callback;
        }
        public String getMenuPath() { return menuPath; };
        public String getToolTip() { return toolTip; };
        public Callback getCallback() { return callback; };
    }

    /* Arrays of plugin registration information. */
    private List<MenuRegistration> menus = new ArrayList<MenuRegistration>();
    private String name = null;
    private String desc = null;
    private ApplicationInterface cls = null;
    private int pos = -1;


    /* 
     * ---------------------------------------------------------------
     * Default Constructor
     * ---------------------------------------------------------------
     */
    public ApplicationRegistration() {}
    public ApplicationRegistration(ApplicationInterface cls) { this.cls = cls; }

    /* 
     * ---------------------------------------------------------------
     * Private Methods
     * ---------------------------------------------------------------
     */

    /* 
     * ---------------------------------------------------------------
     * Setters
     * ---------------------------------------------------------------
     */

    /**
     * Add menu entries with their callbacks.
     * @param path          A path string to add to the menu of of the format
     *                      /toplevel/nextlevel/.../menuitem, with underscores inserted for
     *                      menu mnemonics.
     * @param callback      The callback method that will be called when the menu item is selected.
     */
    public void addMenu(String path, Callback callback) throws Exception {
        if ( path == null ) { log.error("Null path - skipping."); return; }
        if ( callback == null ) { log.error("Null callback - skipping."); return; }
        menus.add( new MenuRegistration(path, callback) );
    }

    /** Save the class instance of the plugin. */
    public void setPlugin(ApplicationInterface cls) { this.cls = cls; }

    /** Set the name for the plugin.  This will be handled automatically by the ApplicationManager. */
    public void setName(String name) { this.name = name; }

    /** Set the description for the plugin.  This will be handled automatically by the ApplicationManager. */
    public void setDescription(String desc) { this.desc = desc; }

    /** Set the toolbar position for the plugin.  This will be handled automatically by the ApplicationManager. */
    public void setPosition(int val) { pos = val; }

    /* 
     * ---------------------------------------------------------------
     * Getters
     * ---------------------------------------------------------------
     */

    /** Retrieve the list of menus configured in this registration. */
    public List<MenuRegistration> getMenus() { return menus; }

    /** Retrieve the name of this registered plugin. */
    public String getName() { return name; }

    /** Retrieve the description of this registered plugin. */
    public String getDescription() { return desc; }

    /** Retrieve the registered plugin instance. */
    public ApplicationInterface getPlugin() { return cls; }

    /** Retrieve the requested position of this plugin, as specified in the plugin.xml. */
    public int getPosition() { return pos; }

}
