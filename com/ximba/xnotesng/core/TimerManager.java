package com.ximba.xnotesng.core;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* JPF */
import org.java.plugin.Plugin;
import org.java.plugin.PluginManager;
import org.java.plugin.registry.Extension;
import org.java.plugin.registry.ExtensionPoint;
import org.java.plugin.registry.PluginDescriptor;
import org.java.plugin.registry.PluginRegistry;
import org.java.plugin.registry.Extension.Parameter;
import org.java.plugin.util.ExtendedProperties;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;

/**
 * <p> 
 * Load and make availble the timer manager plugins.
 * </p>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.7 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class TimerManager {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.core.TimerManager");
    private Timer timer = new Timer();

    /** List of registered TimerEvents. */
    java.util.List<TimerEvent> events = new ArrayList<TimerEvent>();

    /**
     * Default Constructor.
     */
    protected TimerManager() { }

    /*
     * =======================================================
     * Inner classes
     * =======================================================
     */

    private class EventCallback extends TimerTask {
        TimerManager tm = null;
        TimerEvent te = null;
        public EventCallback (TimerManager tm, TimerEvent te) {
            this.tm = tm;
            this.te = te;
        }

        /**
         * Reschedule a timer event.
         */
        private void reschedule() {

            TimerRepeatData trd = te.repeat;

            /* If specified as an offset, compute the offset and reschedule. */
            if ( trd.repeatType == TimerRepeatData.OFFSET )
                te.start.add(trd.offsetType, trd.offsetPeriod);

            /* If type is TimerRepeatData.DAYOFWEEK, then compute a DATE and reschedule. */
            else if ( trd.repeatType == TimerRepeatData.DAYOFWEEK )
            {
                Calendar now = Calendar.getInstance();
                if ( trd.dayOfWeek == now.get(Calendar.DAY_OF_WEEK) )
                    te.start.add(Calendar.DAY_OF_YEAR, 7);
                else if ( trd.dayOfWeek < now.get(Calendar.DAY_OF_WEEK) )
                    te.start.add(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_WEEK) - trd.dayOfWeek);
                else if ( trd.dayOfWeek > now.get(Calendar.DAY_OF_WEEK) )
                    te.start.add(Calendar.DAY_OF_YEAR, 7 - trd.dayOfWeek);
            }

            /* Neither?  Validation must have failed previously! */
            else
                return;

            try { schedule(te); }
            catch (Exception e) {
                log.error(Messages.S_SCHEDFAILED + e.getMessage());
                try { tm.cancel(te.getTimerID()); }
                catch (Exception ce) {
                    log.error(Messages.S_CANCELFAILED + ce.getMessage());
                }
            }
        }

        public void run() {
            /* First, run the registered event callback. */
            te.callback.run(te);

            /* Now check if the timer needs to be rescheduled. */
            if ( te.repeat != null )
            {
                /* If repeating forever, just reschedule it. */
                if ( te.repeat.until == null )
                    reschedule();
                else
                {
                    /* There is an end date. Check if it has passed. */
                    Calendar now = Calendar.getInstance();
                    if ( now.before(te.repeat.until) )
                        reschedule();
                    else
                    {
                        try { tm.cancel(te.getTimerID()); }
                        catch (Exception ce) {
                            log.error(Messages.S_CANCELFAILED + ce.getMessage());
                        }
                    }
                }
            }
        }
    }

    /*
     * =======================================================
     * Private methods.
     * =======================================================
     */

    /**
     * Validate a timer event.  
     * <ul>
     * <li> name field must not be null </li>
     * <li> callback field must not be null </li>
     * <li> start field must not be null </li>
     * <li> start field must be in the future </li>
     * <li> If TimerRepeatData is set, validate it </li>
     * </ul>
     * @throws Exception if the specified event is invalid.
     */
    private void validateEvent( TimerEvent timerEvent ) throws Exception {
        if ( timerEvent.name == null )
            throw new Exception(Messages.S_NAMENULL);
        if ( timerEvent.callback == null )
            throw new Exception(Messages.S_CBNULL);
        if ( timerEvent.start == null )
            throw new Exception(Messages.S_STARTNULL);

        Calendar now = Calendar.getInstance();
        if ( now.after(timerEvent.start) )
            throw new Exception(Messages.S_STARTFUTURE);

        if ( timerEvent.repeat != null )
            validateTimerRepeatData(timerEvent.repeat);
    }

    /**
     * Validate timer repeat data.  
     * <ul>
     * <li> Repeat type must be either TimerRepeatData.OFFSET or TimerRepeatData.DAYOFWEEK </li>
     * <li> Offset type must be -1 or one of:
     *      <ul>
     *      <li> Calendar.MILLISECOND </li>
     *      <li> Calendar.SECOND </li>
     *      <li> Calendar.MINUTE </li>
     *      <li> Calendar.HOUR </li>
     *      <li> Calendar.DAY_OF_MONTH </li>
     *      <li> Calendar.DAY_OF_YEAR </li>
     *      <li> Calendar.MONTH </li>
     *      <li> Calendar.WEEK_OF_YEAR </li>
     *      <li> Calendar.YEAR </li>
     *      </ul>
     *      </li>
     * <li> Offset period must be -1 if offsetType is -1 or > 0 otherwise </li>
     * <li> DayOfWeek must be -1 if repeatType is not TimerRepeatData.DAYOFWEEK or one of 
     *      <ul>
     *      <li> Calendar.SUNDAY </li>
     *      <li> Calendar.MONDAY </li>
     *      <li> Calendar.TUESDAY </li>
     *      <li> Calendar.WEDNESDAY </li>
     *      <li> Calendar.THURSDAY </li>
     *      <li> Calendar.FRIDAY </li>
     *      <li> Calendar.SATURDAY </li>
     *      </ul>
     *      </li>
     * <li> If non-null, until must be in the future. </li>
     * </ul>
     * @throws Exception if the specified event is invalid.
     */
    private void validateTimerRepeatData( TimerRepeatData ted ) throws Exception {
        if ( (ted.repeatType != TimerRepeatData.OFFSET) && (ted.repeatType != TimerRepeatData.DAYOFWEEK) )
            throw new Exception(Messages.S_INVALIDREPEAT);

        if ( (ted.repeatType != TimerRepeatData.OFFSET) && (ted.offsetType != -1) )
            throw new Exception(Messages.S_BADOFFSET);
        if ( (ted.repeatType == TimerRepeatData.OFFSET) &&
             ( (ted.offsetType != Calendar.MILLISECOND) &&
               (ted.offsetType != Calendar.SECOND) &&
               (ted.offsetType != Calendar.MINUTE) &&
               (ted.offsetType != Calendar.HOUR) &&
               (ted.offsetType != Calendar.DAY_OF_MONTH) &&
               (ted.offsetType != Calendar.DAY_OF_YEAR) &&
               (ted.offsetType != Calendar.MONTH) &&
               (ted.offsetType != Calendar.WEEK_OF_YEAR) &&
               (ted.offsetType != Calendar.YEAR) 
             )
           )
            throw new Exception(Messages.S_INVALIDOFFSET + ": type = " + ted.offsetType);

        if ( (ted.offsetType == -1) && (ted.offsetPeriod != -1) )
            throw new Exception(Messages.S_BADPERIOD);
        if ( (ted.offsetType != -1) && (ted.offsetPeriod < 1) )
            throw new Exception(Messages.S_INVALIDPERIOD);

        if ( (ted.repeatType != TimerRepeatData.DAYOFWEEK) && (ted.dayOfWeek != -1) )
            throw new Exception(Messages.S_BADDOW);
        if ( (ted.repeatType == TimerRepeatData.DAYOFWEEK) && 
             ( (ted.dayOfWeek != Calendar.SUNDAY) &&
               (ted.dayOfWeek != Calendar.MONDAY) &&
               (ted.dayOfWeek != Calendar.TUESDAY) &&
               (ted.dayOfWeek != Calendar.WEDNESDAY) &&
               (ted.dayOfWeek != Calendar.THURSDAY) &&
               (ted.dayOfWeek != Calendar.FRIDAY) &&
               (ted.dayOfWeek != Calendar.SATURDAY)
             ) 
           ) 
        {
            throw new Exception(Messages.S_INVALIDDOW + "(value: " + ted.dayOfWeek + ")");
        }

        Calendar now = Calendar.getInstance();
        if ( now.after(ted.until) )
            throw new Exception(Messages.S_UNTILFUTURE);
    }

    /**
     * Check to see if the specified timer already exists.
     * If it does, throw an exception.  If not, do nothing.
     * @throws Exception if the specified event is already registered.
     */
    private void checkRegistration( TimerEvent timerEvent ) throws Exception {
        Iterator it = events.iterator();
        while (it.hasNext())
        {
            TimerEvent te = (TimerEvent) it.next();

            /* If the named timer already exists, reject the event. */
            if ( te.name != null )
            {
                if ( te.name.equals( timerEvent.name ) )
                    throw new Exception(Messages.S_EVENTEXISTS);
            }

            /* If the timer ID already exists, reject the event. */
            if ( te.getTimerID() != null )
            {
                if ( te.getTimerID().equals( timerEvent.getTimerID() ) )
                    throw new Exception(Messages.S_IDEXISTS);
            }
        }
    }

    /**
     * Schedule the specified event.
     * @throws Exception if the Timer class rejects the scheduling.
     */
    private void schedule( TimerEvent timerEvent ) throws Exception {
        EventCallback ec = new EventCallback(this, timerEvent);

        /* Compute the time if alarm is set. */
        Date startTime = timerEvent.start.getTime();
        if ( timerEvent.alarm != null )
        {
            Calendar newCal = Calendar.getInstance();
            newCal.setTimeInMillis( startTime.getTime() );
            newCal.add( Calendar.MINUTE, (-1 * timerEvent.alarm.getMinutes()) );
            startTime = newCal.getTime();
        }
        log.info("Scheduling event (" + timerEvent.name + ") to start @: " +  startTime.toString());
        timer.schedule( new EventCallback(this, timerEvent), startTime );

        timerEvent.eventTask = ec;
    }

    /*
     * =======================================================
     * Internal management interface
     * =======================================================
     */

    /**
     * Shutdown the XNotesNG timer.  This will cancel all registered tasks
     * and kill the global timer thread.
     */
    protected void shutdown() {
        Iterator it = events.iterator();
        while (it.hasNext())
        {
            TimerEvent te = (TimerEvent) it.next();
            te.eventTask.cancel();
            te.callback.cancel(te);
        }
        timer.purge();
        timer.cancel();
    }

    /*
     * =======================================================
     * Manager interface
     * =======================================================
     */

    /**
     * Register a new event.
     * @return A unique identifier for the scheduled timer event.
     * @throws Exception if the cancel fails for any reason.
     */ 
    public String register( TimerEvent timerEvent ) throws Exception {

        if ( timerEvent == null )
        {
            log.error("Registration called but timerEvent is null.  Ignoring.");
            return null;
        }
        log.info("Registration called: " + timerEvent.name);

        /* Validate the event. */
        try { validateEvent(timerEvent); }
        catch (Exception e) {
            log.error( Messages.S_INVALIDTIMER + e.getMessage() );
            throw new Exception(Messages.S_INVALIDTIMER + e.getMessage());
        }

        /* Check to see if this timer event already exists. */
        try { checkRegistration(timerEvent); }
        catch (Exception e) {
            log.error( Messages.S_REGFAILED + e.getMessage() );
            throw new Exception(Messages.S_REGFAILED + e.getMessage());
        }

        /* Schedule it. */
        try { schedule(timerEvent); }
        catch (Exception e) {
            log.error( Messages.S_SCHEDFAILED + e.getMessage() );
            throw new Exception(Messages.S_SCHEDFAILED + e.getMessage());
        }

        /* Add it to the list. */
        events.add(timerEvent);
        return timerEvent.getTimerID();
    }


    /**
     * Cancels the specified event.
     * @param timerEvent   The ID of a previously registered event.
     * @throws Exception if the specified ID does not identify a registered TimerEvent.
     */
    public void cancel(String timerID) throws Exception {
        /* Find then stop the timer task. */
        Iterator it = events.iterator();
        while (it.hasNext())
        {
            TimerEvent te = (TimerEvent) it.next();
            if ( te.getTimerID().equals(timerID) )
            {
                /* Cancel the timer task. */
                te.eventTask.cancel();

                /* Let the caller cleanup too. */
                te.callback.cancel(te);

                /* Remove the entry from the registrations. */
                events.remove( events.indexOf(te) );
                return;
            }
        }
        throw new Exception(Messages.S_NOSUCHTIMER);
    }


    /**
     * Retrieve timer events for the specified application ID.
     * @param appID   The ID of a application associated with the TimerEvents.
     * @return List of TimerEvents that match the appID.  May be an empty list.
     */
    public java.util.List<TimerEvent> getEvents(String appID) {

        java.util.List<TimerEvent> appEvents = new ArrayList<TimerEvent>();
        if ( appID == null )
            return appEvents;

        /* Find matching events. */
        Iterator it = events.iterator();
        while (it.hasNext())
        {
            TimerEvent te = (TimerEvent) it.next();
            if ( appID.equals(te.appID) )
                appEvents.add( te );
        }
        return appEvents;
    }
}
