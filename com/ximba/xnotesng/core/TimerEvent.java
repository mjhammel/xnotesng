package com.ximba.xnotesng.core;

import java.util.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;

/**
 * <p> 
 * Defines an event that will be run at a specific time.
 * </p>
 * <p> 
 * Timer events are data objects that define the following information:
 * </p>
 * <ul>
 * <li> An event name </li>
 * <li> A callback that implements a TimerTask </li>
 * <li> User schedulable: if false, this event should not be browsable by any UI components. Useful for internal timers
 *      such as auto-save features. </li>
 * <li> Initial time - when the callback should be run first time </li>
 * <li> End time - only used to show length of the event in calendars; not used for actual event timing. </li>
 * <li> A TimerRepeatData object if the event should repeat </li>
 * <li> An AlarmData object if the event should issue an alarm </li>
 * </ul>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.2 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class TimerEvent {
    public String name              = null;
    public String appID             = null;
    public TimerCallback callback   = null;
    public TimerTask eventTask      = null;
    public boolean userSchedulable  = false;
    public Calendar start           = null;
    public Calendar end             = null;
    public TimerRepeatData repeat   = null;
    public AlarmData alarm          = null;
    private String timerID          = new RandomGUID().toString();
    public int pn_index             = -1;

    public String getTimerID() { return timerID; }
}
