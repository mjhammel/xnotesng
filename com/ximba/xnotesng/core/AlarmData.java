package com.ximba.xnotesng.core;

import java.util.*;

/**
 * <p> 
 * Defines an alarm attached to a given timer event.
 * </p>
 * <p> 
 * Alarms are data objects that define the following information:
 * </p>
 * <ul>
 * <li> An alarm state </li>
 * <li> The advanced notification amount (must be positive) </li>
 * <li> The advanced notification type </li>
 * </ul>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.1.1.1 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/


public class AlarmData {
    public final static int N_MIN   = 1;
    public final static int N_HOURS = 2;
    public final static int N_DAYS  = 3;

    public boolean state            = false;
    public int amount               = 0;
    public int type                 = -1;

    /**
     * Return the alarm period in minutes.
     */
    public int getMinutes() {
        if ( amount < 0 )
            return 0;

        switch (type) {
            case N_MIN: break;
            case N_HOURS: amount = amount * 60; break;
            case N_DAYS: amount = amount * 60 * 24; break;
        }
        return amount;
    }
}
