package com.ximba.xnotesng.core;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/**
 * <p> 
 * Provide a pop up dialog for selecting the date or time.  Also provides
 * conversion utilities between Calendar objects and DateTime widgets to 
 * strings and those strings back to Calendar objects.
 * </p>
 * @adm $Revision: 1.3 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class DateDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.core.DateDialog");

    /* Saved configuration data. */
    boolean returnOkay = false;

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private boolean          isRunning = false;
    private DateTime         calendar = null;
    private DateTime         time = null;
    private Calendar         cal = null;

    public static final String S_TITLE = "Select a date and time";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_ACCEPT = 1;
    public static final String[] bboxLabels  = {
        "Cancel",
        "Accept",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires the parent for this dialog so it will show up
     * on top of that parent.
     * @param parent        Parent of this dialog.
     */
    public DateDialog(Shell parent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /* Allow dialog to return data. */
    private void accept()
    {
        returnOkay = true;
    }

    /**
     * Build a date/time string.
     * @return A string in the format YYYY/MM/DD:HH.MM
     */
    private String getString()
    {
        String year, month, day;
        String hours, minutes;

        if ( calendar.getYear() < 10 ) year = "0" + calendar.getYear();
        else                           year = "" + calendar.getYear();

        if ( (calendar.getMonth() + 1) < 10 ) month = "0" + calendar.getMonth () + 1;
        else                                  month = "" + calendar.getMonth () + 1;

        if ( calendar.getDay() < 10 ) day = "0" + calendar.getDay ();
        else                          day = "" + calendar.getDay ();

        if ( time.getHours() < 10 ) hours = "0" + time.getHours();
        else                        hours = "" + time.getHours();

        if ( time.getMinutes() < 10) minutes = "0" + time.getMinutes();
        else                         minutes = "" + time.getMinutes();

        return year + "/" + month + "/" + day + ":" + hours + "." + minutes;
    }

    /**
     * Build a Calendar object from the widget.
     */
    private void makeCalendar()
    {
        cal = Calendar.getInstance();
        cal.set( calendar.getYear(), calendar.getMonth(), calendar.getDay(), time.getHours(), time.getMinutes());
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /**
     * Build a date/time string from a Calender that is the same format as the DateDialog returns.
     * @return A string in the format YYYY/MM/DD:HH.MM
     */
    static public String getString(Calendar cal)
    {
        String year, month, day, hours, minutes;
        if ( cal.get(Calendar.YEAR) < 10 )         year    = "0" + cal.get(Calendar.YEAR);
        else                                       year    = ""  + cal.get(Calendar.YEAR);
        if ( cal.get(Calendar.MONTH) < 10 )        month   = "0" + cal.get(Calendar.MONTH);
        else                                       month   = ""  + cal.get(Calendar.MONTH);
        if ( cal.get(Calendar.DAY_OF_MONTH) < 10 ) day     = "0" + cal.get(Calendar.DAY_OF_MONTH);
        else                                       day     = ""  + cal.get(Calendar.DAY_OF_MONTH);
        if ( cal.get(Calendar.HOUR_OF_DAY) < 10 )  hours   = "0" + cal.get(Calendar.HOUR_OF_DAY);
        else                                       hours   = ""  + cal.get(Calendar.HOUR_OF_DAY);
        if ( cal.get(Calendar.MINUTE) < 10 )       minutes = "0" + cal.get(Calendar.MINUTE);
        else                                       minutes = ""  + cal.get(Calendar.MINUTE);
        return year + "/" + month + "/" + day + ":" + hours + "." + minutes;
    }

    /**
     * Build a date/time string from another DateTime widget.
     * @return A string in the format YYYY/MM/DD:HH.MM
     */
    static public String getString(DateTime calendar)
    {
        String year, month, day;
        String hours, minutes;

        if ( calendar.getYear() < 10 ) year = "0" + calendar.getYear();
        else                           year = "" + calendar.getYear();

        if ( (calendar.getMonth() + 1) < 10 ) month = "0" + (calendar.getMonth () + 1);
        else                                  month = "" + (calendar.getMonth () + 1);

        if ( calendar.getDay() < 10 ) day = "0" + calendar.getDay ();
        else                          day = "" + calendar.getDay ();

        if ( calendar.getHours() < 10 ) hours = "0" + calendar.getHours();
        else                        hours = "" + calendar.getHours();

        if ( calendar.getMinutes() < 10) minutes = "0" + calendar.getMinutes();
        else                         minutes = "" + calendar.getMinutes();

        return year + "/" + month + "/" + day + ":" + hours + "." + minutes;
    }

    /**
     * Retrieve the Calendar representation of the selected date.
     * @return A Calendar object initialized to the selected date/time.
     */
    public Calendar getCalendar()
    {
        return cal;
    }

    /**
     * Retrieve the Calendar initialized by the string.  The string 
     * must be in the format YYYY/MM/DD:HH.MM
     * @return A Calendar object initialized to the selected date/time or null if the string cannot be parse.
     */
    public static Calendar getCalendar(String data)
    {
        String[] fields = data.split(":");
        if ( fields.length != 2 )
            return null;
        String[] date = fields[0].split("/");
        String[] time = fields[1].split("\\.");

        if ( date.length != 3 ) return null;
        if ( time.length != 2 ) return null;

        int year, month, day, hour, minute;
        try {
            year    = Integer.parseInt(date[0]);
            month   = Integer.parseInt(date[1]) - 1;
            day     = Integer.parseInt(date[2]);
            hour    = Integer.parseInt(time[0]);
            minute  = Integer.parseInt(time[1]);
        }
        catch (Exception e) { 
            e.printStackTrace();
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        return cal;
    }

    /* Close any dialogs that might be open. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /**
     * Build and display the dialog.
     * @return A string in the format YYYYMMDD:HH.MM
     */
    public String open() 
    {
        isRunning = true;

        shell = new Shell( parent );
        shell.setText(S_TITLE);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        shell.pack();
        shell.open();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if (!Display.getDefault().readAndDispatch ()) Display.getDefault().sleep ();
        }

        String val = null;
        if ( returnOkay )
        {
            makeCalendar();
            val = getString();
        }

        shell.close();
        shell.dispose();
        return val;
    }

    /**
     * Create the dialog content.
     * @param shell     The parent shell for this dialog.
     */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 1 column. */
        gl = new GridLayout();
        gl.marginWidth = 5;
        gl.marginHeight = 5;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        shell.setLayout(gl);

        calendar = new DateTime(shell, SWT.CALENDAR | SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH); 
        calendar.setLayoutData(gd);

        time = new DateTime(shell, SWT.TIME | SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH); 
        time.setLayoutData(gd);

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 1;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            closeDialog();   
                            break;
                        case B_ACCEPT: 
                            accept(); 
                            closeDialog();  
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
