package com.ximba.xnotesng.core;

/**
 * <p> 
 * Interface for Application plugins.
 * </p>
 *
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.1.1.1 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public interface ApplicationInterface {

    /** Shutdown the application (re: plugin). */
    public void shutdown();

    /**
     * This method is called once during application life cycle to allow
     * the plugin to do some one-time setup.
     * @param cid       An instance of the CoreInitData object.
     */
    public void init(CoreInitData cid) throws Exception;

    /** Registration method. */
    public ApplicationRegistration register() throws Exception;

    /** Run the plugin. */
    public void run();
}
