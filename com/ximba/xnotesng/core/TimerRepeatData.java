package com.ximba.xnotesng.core;

import java.util.*;

/**
 * <p> 
 * Configuration information for repeating a TimerEvent.
 * </p>
 * <p>
 * The TimerRepeatData class includes the following information
 * </p>
 * <ul>
 * <li> Repeat Type: OFFSET, DAYOFWEEK
 *      <ul>
 *      <li> OFFSET: If type is OFFSET then there is a single repeat at the given offset. </li>
 *           <ul>
 *           <li> Offset type: Calendar.MINUTE, Calendar.HOUR, Calendar.DAY_OF_MONTH, 
 *                Calendar.WEEK, Calendar.MONTH, Calendar.YEAR </li>
 *           <li> Offset period: numeric, re: the number of minutes, hours, days, etc.  </li>
 *           </ul>
 *           </li>
 *      </ul>
 *      </li>
 * <li> DAYOFWEEK: dayOfWeek field is one of
 *      <ul>
 *      <li> Calendar.MONDAY <li>
 *      <li> Calendar.TUESDAY <li>
 *      <li> Calendar.WEDNESDAY <li>
 *      <li> Calendar.THURSDAY <li>
 *      <li> Calendar.FRIDAY <li>
 *      <li> Calendar.SATURDAY <li>
 *      <li> Calendar.SUNDAY <li>
 *      </ul>
 *      </li>
 * <li> Until: Forever (if field is null) or a Calendar date. Only meaningful if there is a non-offset repeat. </li>
 * </ul>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.1.1.1 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class TimerRepeatData {
    public static final int OFFSET    = 1;
    public static final int DAYOFWEEK = 2;

    public int repeatType       = -1;
    public int offsetType       = -1;
    public int offsetPeriod     = -1;
    public int dayOfWeek        = -1;
    public Calendar until       = null;
}
