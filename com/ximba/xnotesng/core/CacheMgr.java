package com.ximba.xnotesng.core;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;

/**
 * <p> 
 * A singleton class responsible for managing the local cache stores. 
 * </p>
 * @author Michael J. Hammel
 * @since 4.0
 * @adm $Revision: 1.1.1.1 $
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class CacheMgr {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.core.CacheMgr");
    private static CoreInitData cid = null;

    private static final CacheMgr _ref = new CacheMgr();
    private static boolean setupComplete = false;

    /*
     * Class variables.
     */
    private static String dataDir = null;
    private static String _os = null;
    private static String _homeDir = null;

    /* 
     *---------------------------------------------------------------------------------------
     * Version, Platform Strings
     *---------------------------------------------------------------------------------------
     */

    /** Platform Name: Linux */
    public static final String S_PLATFORM_LINUX = "Linux";

    /** Platform Name: Windows */
    public static final String S_PLATFORM_WINDOWS = "Windows";

    /** Platform Name: Mac (currently unsupported) */
    public static final String S_PLATFORM_MAC = "Mac";


    /* 
     *---------------------------------------------------------------------------------------
     * Strings.
     * Prefix all of these with "S_".
     *---------------------------------------------------------------------------------------
     */

    private static final String S_NO_HOMEDIR            = "Can't find user's HOME directory";
    private static final String S_CREATE_DATADIR        = "Created data folder";
    private static final String S_CREATEFAIL_DATADIR    = "Failed to create preferences directory";
    private static final String S_NOCACHEMGRINIT        = "Cache Manager has not been initialized.";
    private static final String S_CACHEMGRINIT          = "Cache Manager has already been initialized.";

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */
    private CacheMgr() {}

    /** Override the parent's clone() method to prevent cloning of this object. */
    public Object clone() throws CloneNotSupportedException
    {
        throw new CloneNotSupportedException();
    }

    /**
     * A synchronized public instantiator guarantees only one instance of the class.
     * @return A reference to the singleton object.
     */
    public static synchronized CacheMgr getInstance()
    {
        return _ref;
    }


    /*
     * =======================================================
     * Private methods.
     * =======================================================
     */

    /**
     * Read the specified cache file and save it's contents to an
     * array of strings.
     * @param cacheName     The name of the file to load.
     * @return A list of Strings representing each line of the file.
     */
    private static List<String> loadCache(String cacheName)
    {
        List<String> entries = new ArrayList<String>();

        /* Read in the cache file, if any. */
        String prefsFilename = getDataDir() + System.getProperty("file.separator") + cacheName;
        File prefsFile = new File(prefsFilename);
        if ( prefsFile.exists() )
        {
            /*
             * The cache files are a simple set of name:value pairs, one per line.
             * Values may also be split by colons if more than one value applies to a single name.
             */
            try {
                FileReader fr = new FileReader(prefsFile);
                BufferedReader in = new BufferedReader(fr);
                String line = null;
                while ( (line=in.readLine()) != null )
                    entries.add(line);
                fr.close();
            }
            catch (Exception e)
            {
                log.error("Can't read servers file (" + prefsFilename + ")");
            }
        }
        return entries;
    }

    /**
     * Saved the specified cache file.
     * @param cacheName     The name of the file to save.
     * @param cache         The array of strings to be saved to file.
     */
    private static void saveCache(String cacheName, List<String> cache)
    {
        String filename = getDataDir() + System.getProperty("file.separator") + cacheName;

        File prefsFile = new File(filename);
        if ( prefsFile.exists() )
            prefsFile.delete();

        /*
         * The cache files are a simple set of name:value pairs, one per line.
         * Values are also split by colons (value:lastused).
         */
        try {
            prefsFile.createNewFile();
            FileWriter fw = new FileWriter(prefsFile);
            PrintWriter pw = new PrintWriter(fw);
            for(Iterator i=cache.iterator(); i.hasNext(); )
            {
                String line = (String) i.next();
                pw.println(line);
            }
            fw.close();
        }
        catch (Exception e)
        {
            log.error("Can't write cache file (" + filename + ")");
        }
    }


    /*
     * =======================================================
     * Public methods.
     * =======================================================
     */

    /**
     * Initialize the cache manager.  This include identifying OS specific 
     * setup and identifying the users data directory. 
     * @param data          The reference to the global CoreInitData object.
     */
    protected static synchronized void init(CoreInitData data)
    {
        cid = data;

        if (setupComplete)
        {
            log.error(S_CACHEMGRINIT);
            return;
        }

        String eMsg = null;

        /*
         * Determine OS the client is running on.  
         * Just in case we need to do platform specific things.
         */
        setOs( System.getProperty("os.name") );

        /* Prep the data folder. */
        setHomeDir( System.getProperty("user.home") );
        if ( (getHomeDir() == null) || (getHomeDir().trim().equals("")) )
        {
            eMsg = S_NO_HOMEDIR + ": " + getHomeDir();
            log.error(eMsg);
            System.err.println(eMsg);
            System.exit(1);
        }
        String dataPath = cid.getDataFolder();
        if ( !dataPath.substring(0,1).equals(File.separator) )
            dataPath = getHomeDir() + File.separator + cid.getDataFolder();
        setDataDir(dataPath);

        /*
         * Create the data director, if necessary.
         */
        File dir = new File(dataDir);
        if ( !dir.exists() )
        {
            if ( !dir.mkdir() )
            {
                eMsg = S_CREATEFAIL_DATADIR + ": " + dataDir;
                log.error(eMsg);
                System.err.println(eMsg);
                System.exit(1);
            }
        }
        log.info(S_CREATE_DATADIR + ": " + dataDir);
        setupComplete = true;
    }


    /**
     * Save a list to the named configuration file.
     * @param filename      The name of the file to save.
     * @param entries       The array of strings to be saved to file.
     */
    public static synchronized void saveConfig(String filename, List<String> entries)
    {
        if ( filename == null )
            return;
        saveCache(filename, entries);
    }

    /**
     * Load a list to the named configuration file.
     * @param filename      The name of the file to load.
     * @return A list of Strings representing each line of the file.
     */
    public static synchronized List<String> loadConfig(String filename)
    {
        return loadCache(filename);
    }

    /* 
     *---------------------------------------------------------------------------------------
     * Setters/Getters for cache items (data, files, directories, etc.)
     *---------------------------------------------------------------------------------------
     */

    /** 
     * Set the OS to a specific value, overriding what CacheMgr initializes it to.
     * @param val   The new setting for the OS.
     */
    public static void setOs(String val) { _os = val; }

    /** 
     * Retrieve the configured OS setting.
     * @return The current setting for the OS.
     */
    public static String getOs() { return _os; }

    /** 
     * Set the home directory. This will have no affect on the location of the users data directory
     * unless it is set before the init() method is called.
     */
    public static void setHomeDir(String val) { _homeDir = val; }

    /** 
     * Set the user home directory, overridding the current setting.
     * @return The current setting for the users home directory.
     */
    public static String getHomeDir() { return _homeDir; }

    /** 
     * Set the user data directory, overridding the current setting.
     * @param val   The new setting for the data directory.
     */
    public static void setDataDir(String val) {
        if (val ==  null)
            return;
        dataDir = val;
    }

    /** 
     * Retrieve the name of the preferences directory.
     * @return A File object representing the users data directory.
     */
    public static File getDataDir() { return new File(dataDir); }
}
