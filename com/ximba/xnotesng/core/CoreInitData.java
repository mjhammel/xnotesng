package com.ximba.xnotesng.core;

/**
 * <p>
 * Stores readonly data that we pass around to requesting plugins.
 * </p>
 *
 * @author Michael J. Hammel
 * @since 4.0
 * @version $Revision: 1.1.1.1 $
 */

import java.io.*;
import org.java.plugin.PluginManager;

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class CoreInitData {

    /** Where XNotesNG data is stored. Configured in boot.properties. */
    private String dataFolder = ".xnotesng";

    /** Command line argument: enable verbose output. */
    private boolean verbose = false;
    /** Command line argument: userid for logins (not currently used). */
    private String userid = null;
    /** Command line argument: password for logins (not currently used). */
    private String passwd = null;
    /** Command line argument: server to connect to (not currently used). */
    private String server = null;
    /** Command line argument: Command line arguments other than those used by the core. */
    private String[] savedArgs = null;

    /** Command line argument: save data on exit. */
    private boolean saveOnExit = false;
    /** Command line argument: use of timer to automatically save data on periodic basis. */
    private boolean autoSave = false;
    /** Menu path to default application to run with a double click on system tray icon. */
    private String defaultApp = null;
    /** Name of the todos file. */
    private String todoFilename = null;

    /* The JPF PluginManager used to start the client. */
    private PluginManager pm = null;

    /* The Timer Manager. */
    private TimerManager tm = null;

    /*
     * Constructor fills the structure making it read-only.
     */
    public CoreInitData(
        PluginManager pm,
        String dataFolder,
        String userid,
        String passwd,
        String server,
        String[] sa,
        boolean verbose,
        boolean saveOnExit,
        boolean autoSave,
        String todoFilename,
        String defaultApp
    ) {

        this.pm = pm;
        if ( dataFolder != null )
            this.dataFolder = dataFolder;
        this.userid = userid;
        this.passwd = passwd;
        this.server = server;
        this.savedArgs = new String[sa.length];
        for (int i=0; i<sa.length; i++)
            this.savedArgs[i] = sa[i];
        this.verbose = verbose;
        this.saveOnExit = saveOnExit;
        this.autoSave = autoSave;
        this.todoFilename = todoFilename;
        this.defaultApp = defaultApp;

        this.tm = new TimerManager();
    }

    /* Verify the caller is permitted to set a value in this object. */
    private boolean ucheck(String u)
    {
        /* For now, we don't have any restrictions.  Later, we will. */
        return true;
    }

    /** Retrieve the program's plugin manager. */
    public PluginManager getManager() { return pm; }
    /** Retrieve the users configured data folder. */
    public String getDataFolder() { return dataFolder; }
    /** Retrieve the verbose state. */
    public boolean getVerbose() { return verbose; }
    /** Retrieve the save on exit state. */
    public boolean getSaveOnExit() { return saveOnExit; }
    /** Retrieve the auto save state. */
    public boolean getAutoSave() { return autoSave; }
    /** Retrieve the configured userid. */
    public String getUserid() { return userid; }
    /** Retrieve the configured passwd. */
    public String getPasswd() { return passwd; }
    /** Retrieve the configured server. */
    public String getServer() { return server; }
    /** Retrieve the command line arguments not parsed by the core. */
    public String[] getArgs() { return savedArgs; }
    /** Retrieve the name of the todos file. */
    public String getTodoFilename() { return todoFilename; }
    /** Retrieve the menu path for the defaultApp. */
    public String getDefaultApp() { return defaultApp; }
    /** Retrieve the TimerManager object. */
    public TimerManager getTimerManager() { return tm; }

    /*
     * Setters for the login data.  Only verifiable plugins can change these values.
     * This can be done via the the UI so we can't use protected methods.
     */
    public void setUserid(String u, String val) { if(ucheck(u)) userid = val; }
    public void setPasswd(String u, String val) { if(ucheck(u)) passwd = val; }
    public void setServer(String u, String val) { if(ucheck(u)) server = val; }

    /* 
     * Other setters.
     */
    public void setVerbose(boolean val) { verbose = val; }
    public void setSaveOnExit(boolean val) { saveOnExit = val; }
    public void setAutoSave(boolean val) { autoSave = val; }
}
