package com.ximba.xnotesng.common;

import java.io.*;
import java.util.*;
import org.apache.log4j.*;
import org.apache.log4j.spi.*;

/**
 * <p> 
 * Simple log4j appender class that forwards log messages to registered callbacks. 
 * </p>
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class LogAppender extends AppenderSkeleton{

    /** The list of registered callbacks for this appender. */
    static List<LogCallback> callbacks = new ArrayList<LogCallback>();

    /** 
     * Register a callback to be called when new log data arrives. 
     * @param cb The LogCallback to call.  The append() method is called when new data arrives.
     */
    public static void register(LogCallback cb) { callbacks.add(cb); }

    protected void append(LoggingEvent event) {

        /* If no layout is available, don't do anything. */
        if( this.layout == null ) {   
            errorHandler.error("No layout for appender " + name, null, ErrorCode.MISSING_LAYOUT );
            return;
        }

        String message = this.layout.format(event);
        Iterator it = callbacks.iterator();
        while (it.hasNext())
        {
            LogCallback cb = (LogCallback)it.next();
            cb.append(message);
        }
    }

    /**
     * Override requiresLayout() to return true.
     */
    public boolean requiresLayout() { return true; }

    /**
     * Override close() to be a no op.
     */
    public void close() {}

}

