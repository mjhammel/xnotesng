package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;

/**
 * <p> 
 * Search a note for the specified text.
 * </p>
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class SearchDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.SearchDialog");

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private XNoteUI          xnoteUI = null;
    private boolean          isRunning = false;
    private Text             searchText = null;
    private Button           caseButton = null;
    private Button           directionButton = null;

    public static final String S_TITLE = "Search a note";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_SEARCH = 1;
    public static final String[] bboxLabels  = {
        "Close",
        "Search",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires a referenct to a parent shell and a reference
     * to the displayed note object that will be searched.
     */
    public SearchDialog(Shell parent, XNoteUI xnoteUI) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
        this.xnoteUI = xnoteUI;
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /** Position the dialog. */
    private void positionDialog()
    {
        Point pt = Display.getDefault().getCursorLocation();
        shell.setLocation(pt);
    }

    /** Search the note and reposition it accordingly. */
    private void search()
    {
        String searchStr = searchText.getText();
        if ( searchStr.length() == 0 )
        {
            MessageDialog md = new MessageDialog(parent);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage(Messages.S_SEARCHTERMREQUIRED);
            md.open();
            return;
        }

        XNoteData xd = xnoteUI.getData();
        String data = new String(xd.pn_text);
        if ( !caseButton.getSelection() )
        {
            data = data.toLowerCase();
            searchStr = searchStr.toLowerCase();
        }

        int currentPos = -1;
        int newPos = -1;
        if ( !directionButton.getSelection() )
        {
            currentPos = xnoteUI.getCaretPosition(false);
            newPos = data.indexOf(searchStr, currentPos);
        }
        else
        {
            currentPos = xnoteUI.getCaretPosition(true);
            newPos = data.lastIndexOf(searchStr, currentPos-1);
        }
        if ( newPos != -1 )
            xnoteUI.selectText(newPos, searchStr.length());
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /** Close the dialog. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /** Build and display the dialog. */
    public void open() 
    {
        isRunning = true;

        shell = new Shell(parent);
        shell.setText(S_TITLE);
        positionDialog();

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        shell.pack();
        shell.open();
        shell.forceActive();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if (!Display.getDefault().readAndDispatch ()) Display.getDefault().sleep ();
        }

        shell.close();
        shell.dispose();
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 2 columns. */
        gl = new GridLayout();
        gl.marginWidth = 5;
        gl.marginHeight = 5;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = true;
        shell.setLayout(gl);

        searchText = new Text(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.horizontalSpan = 2;
        gd.widthHint = 230;
        searchText.setLayoutData(gd);

        /* Run the search when ENTER is pressed in the text input field. */
        searchText.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent event)
            {
                if (event.character == SWT.CR)
                    search(); 
            }
        });

        /* Two toggle buttons: Case and direction */
        caseButton = new Button(shell, SWT.TOGGLE);
        caseButton.setText("Case sensitive");
        gd = new GridData(GridData.FILL_BOTH); 
        caseButton.setLayoutData(gd);

        directionButton = new Button(shell, SWT.TOGGLE);
        directionButton.setText("Search Backward");
        gd = new GridData(GridData.FILL_BOTH); 
        directionButton.setLayoutData(gd);


        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            closeDialog();   
                            break;
                        case B_SEARCH: 
                            search(); 
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
