package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Plugin class for the Notes application of XNotesNG
 * </p>
 *
 * @adm $Revision: 1.4 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class XNotes extends Plugin implements ApplicationInterface {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.XNotes");
    private static Shell shell = null;
    private static XNotes xnotes = null;

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;
    private static final Color defaultNoteBG = new Color(Display.getDefault(), 242, 234, 0);

    /* Default width and height for notes. */
    private static NoteSize noteSmall = new NoteSize();
    private static NoteSize noteMedium = new NoteSize();
    private static NoteSize noteLarge = new NoteSize();

    String fileIconFile = null;
    String searchForwardIconFile = null;
    String searchBackIconFile = null;
    static Image fileIcon = null;
    static Image searchForwardIcon = null;
    static Image searchBackIcon = null;
    static Map<String, Image> subImages = new HashMap<String, Image>();

    /* The names of all the files. */
    private static java.util.List<XNoteData> noteData = new ArrayList<XNoteData>();

    /* This Plugin ID. */
    private static final String PLUGIN_ID = "com.ximba.xnotesng.apps.xnotes";

    /** The ApplicationRegistration for this plugin. */
    ApplicationRegistration appReg = null;

    /** The list of notes we have open. */
    static java.util.List<XNoteUI> notes = new ArrayList<XNoteUI>();

    public static final int NOTE_SIZE_SMALL    = 1;
    public static final int NOTE_SIZE_MEDIUM   = 2;
    public static final int NOTE_SIZE_LARGE    = 3;

    public static final int SMALL     = 1;
    public static final int MEDIUM    = 2;
    public static final int LARGE     = 3;
    private static Font largeFont   = null;
    private static Font mediumFont  = null;
    private static Font smallFont   = null;
    private static Font systemFont  = null;

    /*
     * =======================================================
     * Constructor
     * This plugin does not have a constructor method.
     * =======================================================
     */

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /**
     * The size of a note, in inches.  Inches should be specified
     * as float values, such 3.5 or 2.0.
     */
    public static class NoteSize {
        float x;
        float y;
    }

    /**
     * Filter a list of files for those that are prefixed with
     * "note"
     */
    class NoteFilter implements FilenameFilter {
        public boolean accept(File dir, String filename) {
            if ( filename.indexOf("note") == 0 )
                return true;
            return false;
        }
    }

    /**
     * Called when a menu entry for creating a new note 
     * is selected from the system tray menus.
     */
    class NewNote implements Callback {
        XNotes parent;
        int type;
        public NewNote(XNotes parent, int type) {
            this.parent = parent;
            this.type = type;
        }
        public void run() {
            parent.newNote(type);
        }
    }

    /**
     * Called when the system tray entry for importing
     * notes is selected.
     */
    class XNoteImport implements Callback {
        XNotes parent;
        public XNoteImport(XNotes parent) {
            this.parent = parent;
        }
        public void run() {
            parent.xnimport();
        }
    }

    /**
     * Called when the system tray entry for finding
     * a note is selected.
     */
    class Find implements Callback {
        XNotes parent;
        int type;
        public Find(XNotes parent, int type) {
            this.parent = parent;
            this.type = type;
        }
        public void run() {
            parent.find(type);
        }
    }

    /**
     * Called when the system tray entry for hiding
     * all notes is selected.
     */
    class HideAll implements Callback {
        XNotes parent;
        public HideAll(XNotes parent) {
            this.parent = parent;
        }
        public void run() {
            parent.hideAll();
        }
    }

    /**
     * Called when the system tray entry for showing
     * (unhiding) all notes is selected.
     */
    class ShowAll implements Callback {
        XNotes parent;
        public ShowAll(XNotes parent) {
            this.parent = parent;
        }
        public void run() {
            parent.showAll();
        }
    }

    /**
     * Autosave timer task will save any modified notes on the scheduled period.
     */
    class AutoSave implements TimerCallback
    {
        XNotes xn = null;
        public AutoSave(XNotes xn) {
            this.xn = xn;
        }
        public void run(TimerEvent te)
        {
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    xn.saveAll();
                }
            });

        }
        public void cancel(TimerEvent te) { }
    }



    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /**
     * Parse a note size from configuration file.
     * @param size      A size string in the format WxH, such as 1.5x3
     * @return A NoteSize object.
     */
    private NoteSize parseSize(String size)
    {
        NoteSize pt = new NoteSize();
        String[] fields = size.split("x");
        if ( fields.length == 2 )
        {
            try {
                pt.x = Float.parseFloat(fields[0]);
                pt.y = Float.parseFloat(fields[1]);
            }
            catch (Exception e) {
                pt.x = (float)3.5;
                pt.y = (float)2.0;
            }
        }
        return pt;
    }

    /*
     * =======================================================
     * JPF methods
     * =======================================================
     */

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStart">Plugin.doStart</a>}.
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStop">Plugin.doStop</a>}.
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }


    /*
     * =======================================================
     * ApplicationManagerInterface methods
     * =======================================================
     */

    /**
     * Close any note-related windows.
     */
    public void shutdown()
    {
        Iterator it = notes.iterator();
        while( it.hasNext() )
        {
            XNoteUI xn = (XNoteUI)it.next();
            xn.save();
            xn.close();
        }
        shell.dispose();
    }


    /**
     * This method is called once during application life cycle to allow
     * the plugin to initialize itself.
     * @param cid   A reference to the global CoreInitData object.
     */
    public void init(CoreInitData cid) throws Exception
    {
        xnotes = this;
        this.cid = cid;

        /* Find the home directory for the plugin. */
        home = ApplicationManager.getHome(cid, PLUGIN_ID);

        /* Setup the default size for notes. */
        String notesize = ApplicationManager.getAttr(cid, PLUGIN_ID, "defaultSizeSmall");
        noteSmall = parseSize(notesize);
        notesize = ApplicationManager.getAttr(cid, PLUGIN_ID, "defaultSizeMedium");
        noteMedium = parseSize(notesize);
        notesize = ApplicationManager.getAttr(cid, PLUGIN_ID, "defaultSizeLarge");
        noteLarge = parseSize(notesize);

        try {
            fileIconFile = home + ApplicationManager.getAttr(cid, PLUGIN_ID, "fileIcon");
            if (!new File(fileIconFile).exists()) {
                log.error("XNotes file icon not found: filename = " + fileIconFile);
            } else {
                fileIcon = new Image(Display.getDefault(), fileIconFile);
            }
            searchForwardIconFile = home + ApplicationManager.getAttr(cid, PLUGIN_ID, "searchForwardIcon");
            if (!new File(searchForwardIconFile).exists()) {
                log.error("XNotes search icon not found: filename = " + searchForwardIconFile);
            } else {
                searchForwardIcon = new Image(Display.getDefault(), searchForwardIconFile);
            }
            subImages.put("searchForwardIcon", searchForwardIcon);
            searchBackIconFile = home + ApplicationManager.getAttr(cid, PLUGIN_ID, "searchBackIcon");
            if (!new File(searchBackIconFile).exists()) {
                log.error("XNotes search icon not found: filename = " + searchBackIconFile);
            } else {
                searchBackIcon = new Image(Display.getDefault(), searchBackIconFile);
            }
            subImages.put("searchBackIcon", searchBackIcon);
        } catch (Exception e) {
            log.error("One or more XNotse icons were not found.");
        }


        /* Create a Shell for all notes to below to. */
        shell = new Shell(Display.getDefault());

        /* Open the note directory and find all the notes. */
        File dataFolder = CacheMgr.getDataDir();
        File[] files = dataFolder.listFiles( new NoteFilter() );
        if ( (files != null) && (files.length>0) )
        {
            for (int i=0; i<files.length; i++)
            {
                if ( files[i].exists() )
                {
                    try { 
                        XNoteIO nio = new XNoteIO(cid);
                        XNoteData nf = nio.loadNote( files[i].getCanonicalPath() ); 
                        noteData.add( nf );
                        XNoteUI xn = new XNoteUI(cid, shell, noteSmall, this);
                        xn.setData(nf);
                        notes.add ( xn );

                        /* Display the note, if not hidden. */
                        if ( !nf.pn_hidden )
                            xn.open();
                    }
                    catch (Exception e) {
                        log.error("Can't load note file: " + files[i].getCanonicalPath() );
                    }
                }
            }
        }

        /* Create some fonts. */
        systemFont = shell.getFont();
        FontData[] fontData = systemFont.getFontData();
        for (int i = 0; i < fontData.length; i++) {
            fontData[i].setHeight(9);
            fontData[i].setStyle(SWT.BOLD);
        }
        largeFont = new Font(Display.getDefault(), fontData);
        mediumFont = new Font(Display.getDefault(),"Courier",8,SWT.NONE);
        smallFont = new Font(Display.getDefault(),"Verdana",6,SWT.NONE);

        /* Register auto save timer. */
        TimerRepeatData trd = new TimerRepeatData();
        trd.repeatType = TimerRepeatData.OFFSET;
        trd.offsetType = Calendar.MINUTE;
        trd.offsetPeriod = 1;
        TimerEvent te = new TimerEvent();
        te.name = "XNotes Auto Save";
        te.callback = new AutoSave(this);
        te.start = Calendar.getInstance();
        te.start.add(Calendar.MINUTE, 1);
        te.repeat = trd;
        TimerManager tm = cid.getTimerManager();
        try { tm.register(te); }
        catch (Exception e) {
            log.error( e.getMessage(), e );
        }
    }

    /**
     * Register the plugin.
     * @return An ApplicationRegistration object filled with menu registrations.
     */
    public ApplicationRegistration register()
    {
        log.info("Application registration: XNotes.");
        ApplicationRegistration ar = new ApplicationRegistration(this);
        try {
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"noteSmall"), (Callback) new NewNote(this, NOTE_SIZE_SMALL));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"noteMedium"), (Callback) new NewNote(this, NOTE_SIZE_MEDIUM));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"noteLarge"), (Callback) new NewNote(this, NOTE_SIZE_LARGE));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"findAny"), (Callback) new Find(this, FindDialog.M_ANY));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"findHidden"), (Callback) new Find(this, FindDialog.M_HIDDEN));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"hideAll"), (Callback) new HideAll(this));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"showAll"), (Callback) new ShowAll(this));
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"import"), (Callback) new XNoteImport(this));
        }
        catch (Exception e) {
            log.error("Error while registering plugin: reason = " + e.getMessage(), e);
            return null;
        }
        return ar;
    }

    /**
     * Open all existing notes that need to be visible.
     */
    public void run()
    {
    }

    /*
     * =======================================================
     * Application specific methods.
     * =======================================================
     */

    /** Provide a shell for message dialogs, even for classes that don't have windows.  */
    static public Shell getShell() { return shell; }

    /** Provide a default color for all notes.  */
    static public Color getBG() { return defaultNoteBG; }

    /** Retrieve the images configured for this plugin.  */
    public static Map<String, Image> getImages() { return subImages; }

    /** Retrieve all note UI objects.  */
    public static java.util.List<XNoteUI> getNotes() { return notes; }

    /** Retrieve all note data.  */
    public static java.util.List<XNoteData> getNoteData() { return noteData; }

    /** 
     * Retrieve a predefined font. 
     * @param type      The type of font to retrieve.  One of
     *                  <ul>
     *                  <li> XNotes.SMALL - the small sized font defined in the plugin.xml </li>
     *                  <li> XNotes.MEDIUM - the medium sized font defined in the plugin.xml </li>
     *                  <li> XNotes.LARGE - the large sized font defined in the plugin.xml </li>
     *                  </ul>
     */
    static public Font getFont(int type) { 
        switch (type)
        {
            case SMALL: return smallFont;
            case MEDIUM: return mediumFont;
            case LARGE: return largeFont;
            default: return systemFont;
        }
    }

    /**
     * Retrieve configured note sizes.
     * @param type      The type of note to retrieve.  One of
     *                  <ul>
     *                  <li> XNotes.NOTE_SIZE_SMALL - the small sized note defined in the plugin.xml </li>
     *                  <li> XNotes.NOTE_SIZE_MEDIUM - the medium sized note defined in the plugin.xml </li>
     *                  <li> XNotes.NOTE_SIZE_LARGE - the large sized note defined in the plugin.xml </li>
     *                  </ul>
     */
    public static NoteSize getSize(int type)
    {
        switch ( type )
        {
            case NOTE_SIZE_SMALL : return noteSmall; 
            case NOTE_SIZE_MEDIUM: return noteMedium; 
            case NOTE_SIZE_LARGE : return noteLarge; 
            default: return noteSmall;
        }
    }


    /**
     * Find the lowest available index id.
     * @return A number representing the lowest index id not currently in use by the notes.
     */
    static public int noteIndex ()
    {
        /*
         * This is O(n**2), but the list should be small.
         */
        for ( int index = 1; ;index++ )
        {
            if ( findNote(index) == null )
                return index;
        }
    }

    /**
     * Find the specific note, by its index id.
     * @param index     The id of the note to search for.
     */
    static public XNoteData findNote ( int index )
    {
        Iterator it = noteData.iterator();
        while ( it.hasNext() )
        {
            XNoteData nf = (XNoteData) it.next();
            if ( nf.pn_index == index )
                return nf;
        }
        return null;
    }

    /**
     * Remove the specified XNoteUI from the list of notes.
     * @param xn        The XNoteUI object (re: XNote window) to remove from management.
     */
    static public void remove ( XNoteUI xn )
    {
        if ( notes.remove(xn) )
            log.error("Can't find XNoteUI to remove.");
    }

    /**
     *
     * =======================================================
     * Application specific callbacks.
     * =======================================================
     */

    /**
     * Create and display a new note and add it to the list of notes under management.
     * @param type      The predefined size of note window to create.
     */
    static public int newNote(int type)
    {
        NoteSize ns = null;
        switch ( type )
        {
            case NOTE_SIZE_SMALL : ns = noteSmall; break;
            case NOTE_SIZE_MEDIUM: ns = noteMedium; break;
            case NOTE_SIZE_LARGE : ns = noteLarge; break;
        }

        XNoteUI xn = new XNoteUI(cid, shell, ns, xnotes);
        XNoteData nf = new XNoteData();
        nf.pn_index = noteIndex();
        noteData.add ( nf );
        xn.setData(nf);
        xn.open();
        notes.add ( xn );
        return nf.pn_index;
    }

    /**
     * Import one or more notes.
     */
    private void xnimport()
    {
        ImportDialog id = new ImportDialog(cid, shell);
        id.setDirMode();
        java.util.List<XNoteData> list = id.open();
        if ( list == null )
            return;
        Iterator it = list.iterator();
        while (it.hasNext())
        {
            XNoteData nf = (XNoteData)it.next();
            XNoteUI xn = new XNoteUI(cid, shell, noteSmall, this);
            nf.pn_index = noteIndex();
            noteData.add ( nf );
            xn.setData(nf);
            xn.open();
            notes.add ( xn );
        }
    }

    /**
     * Find a note.  
     * @param type      One of the valid modes for the FindDialog.
     */
    private void find(int type)
    {
        FindDialog fd = new FindDialog(shell);
        fd.setMode(type);
        fd.open();
    }

    /**
     * Hide all notes.
     */
    private void hideAll()
    {
        Iterator it = notes.iterator();
        while( it.hasNext() )
        {
            XNoteUI xn = (XNoteUI)it.next();
            xn.hide();
        }
    }

    /**
     * Show all notes.
     */
    private void showAll()
    {
        Iterator it = notes.iterator();
        while( it.hasNext() )
        {
            XNoteUI xn = (XNoteUI)it.next();
            xn.show();
        }
    }

    /**
     * Save all notes.
     */
    private void saveAll()
    {
        Iterator it = notes.iterator();
        while( it.hasNext() )
        {
            XNoteUI xn = (XNoteUI)it.next();
            xn.save();
        }
    }
}
