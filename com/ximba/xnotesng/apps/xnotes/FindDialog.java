package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;

/**
 * <p> 
 * Search for notes (but not *in* notes)
 * </p>
 * @adm $Revision: 1.3 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class FindDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.FindDialog");

    /* Saved configuration data. */
    XNoteUI note = null;
    java.util.List<XNoteUI> notes = null;

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private boolean          isRunning = false;
    private Table            noteTable = null;
    private int              mode = M_ANY;

    public static final int M_ANY       = 1;
    public static final int M_HIDDEN    = 2;

    public static final String S_TITLE_A = "Find Notes";
    public static final String S_TITLE_H = "Find Hidden Notes";
    public static final String S_NAME    = "Note Name";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_ACCEPT = 1;
    public static final String[] bboxLabels  = {
        "Close",
        "Open Note",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor needs a parent shell to locate the dialog.
     */
    public FindDialog(Shell parent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
        notes = XNotes.getNotes();
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /** Holder for note data so it can be sorted. */
    public static class Entry implements Comparable<Entry> {
        public String title = null;
        public XNoteUI xui = null;
        public XNoteData xd = null;
        public int compareTo(Entry e) throws NullPointerException{
            if (e == null)
                throw new NullPointerException();
            return title.compareToIgnoreCase(e.title);
        }
    }

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /* Allow dialog to return data. */
    private void accept()
    {
        TableItem[] rows = noteTable.getSelection();
        if ( rows.length == 0 )
        {
            MessageDialog md = new MessageDialog(parent);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage( Messages.S_SELECTONENOTE );
            md.open();
            return;
        }
        note = (XNoteUI)rows[0].getData();
        note.show();
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /** 
     * The mode the dialog will use.
     * @param type      The mode to use.  Valid values are
     *                  <ul>
     *                  <li> FindDialog.M_ANY - show all notes in the list </li>
     *                  <li> FindDialog.M_HIDDEN - show only hidden notes in the list </li>
     *                  </ul>
     */
    public void setMode(int type)
    {
        if ( type == M_ANY ) mode = M_ANY;
        else if ( type == M_HIDDEN ) mode = M_HIDDEN;
    }

    /** Close the dialog. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /** 
     * Build and display the dialog.
     * @return An XNoteUI object representing the selected visible note (and its data)
     */
    public void open() 
    {
        isRunning = true;

        shell = new Shell( parent );
        if ( mode == M_ANY ) shell.setText(S_TITLE_A);
        else                 shell.setText(S_TITLE_H);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        shell.pack();
        shell.open();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if (!Display.getDefault().readAndDispatch ()) Display.getDefault().sleep ();
        }

        shell.close();
        shell.dispose();
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 1 column. */
        gl = new GridLayout();
        gl.marginWidth = 5;
        gl.marginHeight = 5;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        shell.setLayout(gl);

        noteTable = new Table(shell, SWT.VIRTUAL | SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.widthHint = 200;
        gd.heightHint = 400;
        noteTable.setLayoutData(gd);

        TableColumn col = new TableColumn(noteTable, SWT.LEFT);
        col.setText(S_NAME);
        col.setWidth(196);

        Iterator it = notes.iterator();
        java.util.List<Entry> entries = new ArrayList<Entry>();
        while( it.hasNext() )
        {
            XNoteUI xui = (XNoteUI)it.next();
            XNoteData xd = xui.getData();
            if ( (mode == M_ANY) || ( (mode == M_HIDDEN) && (xd.pn_hidden) ) )
            {
                Entry entry = new Entry();
                entry.xui = xui;
                entry.xd = xd;
                if ( xd.pn_name != null )
                    entry.title = xd.pn_name;
                else
                    entry.title = "Unnamed note";
                entries.add(entry);
            }
        }
        Collections.sort(entries);
        it = entries.iterator();
        while( it.hasNext() )
        {
            Entry entry = (Entry)it.next();
            TableItem row = new TableItem(noteTable, SWT.NONE);
            row.setText(0, entry.xd.pn_name);
            row.setData(entry.xui);
        }

        noteTable.addSelectionListener(new SelectionAdapter() {
            public void widgetDefaultSelected(SelectionEvent e) {
                accept(); 
            }
        });

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 1;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            closeDialog();   
                            break;
                        case B_ACCEPT: 
                            accept(); 
                            closeDialog();  
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
