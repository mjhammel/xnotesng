package com.ximba.xnotesng.apps.xnotes;

/**
 * <p> 
 * Messages, usually passed in Exceptions.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class Messages {

    public final static String S_NOSUCHFILE    = "No such file: ";
    public final static String S_READFAIL      = "Failed while reading file: ";
    public final static String S_NOTANOTE      = "File is not a note (try importing instead): ";
    public final static String S_NONOTESFOUND  = "No notes found in that directory.";
    public final static String S_FAILEDIMPORT  = "Failed to import file: ";
    public final static String S_SELECTONENOTE = "Select at least one note.";

    public final static String S_MISSINGFILENAME    = "You must select a file.";
    public final static String S_MISSINGDIRNAME     = "You must select a directory.";
    public final static String S_SEARCHTERMREQUIRED = "You must provide a search term.";
}
