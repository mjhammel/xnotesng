package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Class for loading Notes files in XNotes application.
 * </p>
 *
 * @adm $Revision: 1.2 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class XNoteIO {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.xnotes.NoteIO");

    /*
     * Post-It Note file information.  
     * Legacy note magic is handled by the NoteImporter plugin.
     */
    static final String StickyNoteMagic_v4      = "%%!!<xpstickynote.v4>";
    static final String StickyNoteFname         = "note";

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;

    /* Fields on the configuration line of a note file. */
    static final int F_MAGIC        = 0;
    static final int F_SHELLX       = 1;
    static final int F_SHELLY       = 2;
    static final int F_TEXTW        = 3;
    static final int F_TEXTH        = 4;
    static final int F_SCREENW      = 5;
    static final int F_SCREENH      = 6;
    static final int F_HIDDEN       = 7;
    static final int F_INDEX        = 8;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires reference to global CoreInitData object.
     */
    public XNoteIO(CoreInitData cid) 
    {
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */


    /*
     * =======================================================
     * I/O specific methods.
     * =======================================================
     */

    /**
     * Load a note file.  File format is as follows:
     * <ul>
     * <li> Line 1 (config, space separated): 
     *      magic shellx shelly textw texth screenw screenh hidden index </li>
     * <li> Line 2  : category name </li>
     * <li> Line 3  : note name </li>
     * <li> Line 4- : note text </li>
     * </ul>
     * @param filename  The name of to file the note data will be written to.
     * @return An XNoteData object from the data loaded from file.
     * @throws Exception if filename does not exist, can't be read or if
     * the note is not of the current version.
     */
    public XNoteData loadNote(String filename) throws Exception
    {
        File file = new File( filename );
        if ( !file.exists() )
            throw new Exception( Messages.S_NOSUCHFILE + filename );

        /* Read the file into a list, with the config line held separate. */
        StringBuilder noteText = new StringBuilder("");
        String config = null;
        String category = null;
        String name = null;
        try {
            FileReader fr = new FileReader(file);
            BufferedReader in = new BufferedReader(fr);
            String line = null;
            while ( (line=in.readLine()) != null )
            {
                if ( config == null ) config = line;
                else if ( category == null ) category = line;
                else if ( name == null ) name = line;
                else 
                {
                    if ( noteText.length() > 0 )
                        noteText.append( "\n" );
                    noteText.append( line );
                }
            }
            fr.close();
        }
        catch (Exception e) {
            throw new Exception( Messages.S_READFAIL + filename + ": " + e.getMessage() );
        }

        /* Verify the note's version */
        if (config.indexOf(StickyNoteMagic_v4) != 0)
            throw new Exception( Messages.S_NOTANOTE + filename );

        /* Allocate a note and fill it in. */
        XNoteData nf = new XNoteData();
        nf.pn_file = new String( filename );
        if ( category != null ) nf.pn_category = new String( category );

        /* Parse the config and fill in the XNoteData */
        String[] fields = config.split(" ");
        nf.pn_magic = new String( fields[F_MAGIC] );
        nf.pn_shellx = Integer.parseInt( fields[F_SHELLX] );
        nf.pn_shelly = Integer.parseInt( fields[F_SHELLY] );
        nf.pn_textw = Integer.parseInt( fields[F_TEXTW] );
        nf.pn_texth = Integer.parseInt( fields[F_TEXTH] );
        nf.pn_screenw = Integer.parseInt( fields[F_SCREENW] );
        nf.pn_screenh = Integer.parseInt( fields[F_SCREENH] );
        nf.pn_hidden = Boolean.parseBoolean( fields[F_HIDDEN] );
        nf.pn_index = Integer.parseInt( fields[F_INDEX] );

        if ( (name != null) && !name.equals("null") ) nf.pn_name = new String( name );
        else                                          nf.pn_name = new String( "Note " + nf.pn_index );

        /* Save the note text, if any. */
        if ( noteText.length() > 0 )
            nf.pn_text = new String ( noteText.toString() );

        /* Return the note. */
        return nf;
    }

    /**
     * Save a note file.  If the filename for the note is not specified then the file name
     * is set to CacheMgr.getDataDir() + File.separator + pn_index.
     * File format is as follows:
     * <ul>
     * <li> Line 1 (config, space separated): 
     *      magic shellx shelly textw texth screenw screenh hidden index </li>
     * <li> Line 2  : category name </li>
     * <li> Line 3  : note name </li>
     * <li> Line 4- : note text </li>
     * </ul>
     * @param nf    An XNoteData object to save to file.
     * @throws Exception if filename is not set and index is -1.
     */
    public void saveNote(XNoteData nf) throws Exception
    {
        /* If the filename is not set, create it. */
        if ( nf.pn_file == null )
        {
            if ( nf.pn_index != -1 )
                nf.pn_file = CacheMgr.getDataDir().getPath() + File.separator + "note" + nf.pn_index;
            else
                throw new Exception ( "Invalid note index - can't build filename.");
        }

        File file = new File( nf.pn_file );
        try {
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);

            StringBuilder line = new StringBuilder(StickyNoteMagic_v4);
            line.append(" ");
            line.append("" + nf.pn_shellx + " ");
            line.append("" + nf.pn_shelly + " ");
            line.append("" + nf.pn_textw + " ");
            line.append("" + nf.pn_texth + " ");
            line.append("" + nf.pn_screenw + " ");
            line.append("" + nf.pn_screenh + " ");
            line.append("" + nf.pn_hidden + " ");
            line.append("" + nf.pn_index);
            pw.println( line.toString() );
            pw.println( nf.pn_category );
            if ( (nf.pn_name != null) && !nf.pn_name.equals("null") ) pw.println( nf.pn_name );
            else                                                       pw.println( "Note " + nf.pn_index );
            pw.println( nf.pn_text );
            fw.close();
        }
        catch (Exception e) {
            throw new Exception ( "Failed to create note file: " + e.getMessage() );
        }
    }

    /**
     * Remove a note from disk.
     * @param nf    An XNoteData object that holds the name of the file to remove.
     */
    public void remove(XNoteData nf) 
    {
        if ( nf.pn_file == null )
            return;

        File file = new File(nf.pn_file);
        if ( file.exists() )
        {
            if ( !file.delete() )
                log.info("Failed to delete note file: " + nf.pn_file);
        }
        else
            log.info("Request to delete file failed, no such file: " + nf.pn_file);
    }

}
