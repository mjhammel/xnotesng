package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/**
 * <p> 
 * Set the name for a note.
 * </p>
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class NameDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.NameDialog");

    /* Saved configuration data. */
    boolean returnOkay = false;

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private boolean          isRunning = false;
    private Text             nameText = null;

    public static final String S_TITLE = "Specify Note Name";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_ACCEPT = 1;
    public static final String[] bboxLabels  = {
        "Cancel",
        "Accept",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires a reference to the parent shell.
     */
    public NameDialog(Shell parent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /* Allow dialog to return data. */
    private void accept()
    {
        returnOkay = true;
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /* Close any dialogs that might be open. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /** 
     * Build and display the dialog.
     * @return A text string representing a name (or anything else it might be used for.)
     */
    public String open(String noteName) 
    {
        isRunning = true;

        shell = new Shell( parent );
        shell.setText(S_TITLE);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell, noteName);
        shell.pack();
        shell.open();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if (!Display.getDefault().readAndDispatch ()) Display.getDefault().sleep ();
        }

        String name =  nameText.getText();
        shell.close();
        shell.dispose();
        if ( returnOkay )
            return name;
        else
            return null;
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell, String name) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 1 column. */
        gl = new GridLayout();
        gl.marginWidth = 5;
        gl.marginHeight = 5;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        shell.setLayout(gl);

        nameText = new Text(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.widthHint = 200;
        nameText.setLayoutData(gd);
        if (name != null )
            nameText.setText(name);

        /* When the user hits ENTER, accept the changes even if none have been made. */
        nameText.addKeyListener(new KeyAdapter() {
            public void keyReleased(KeyEvent event)
            {
                if (event.character == SWT.CR)
                {
                    accept(); 
                    closeDialog();  
                }
            }
        });

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 1;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            closeDialog();   
                            break;
                        case B_ACCEPT: 
                            accept(); 
                            closeDialog();  
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
