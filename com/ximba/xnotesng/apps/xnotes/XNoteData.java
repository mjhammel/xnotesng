package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.util.*;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

/**
 * <p> 
 * Class that holds data for a single XNote.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class XNoteData {

    /** note magic */
    public String   pn_magic = null;    

    /** name of note */
    public String   pn_name = null;     

    /** text of note */
    public String   pn_text = null;     

    /** category id */
    public String   pn_category = null; 

    /** file note will be saved in */
    public String   pn_file = null;     

    /** index number of note */
    public int      pn_index = -1;      

    /** true if note is currently hidden */
    public boolean  pn_hidden = false;  

    /** false = save button sensitive */
    public boolean  pn_saved = true;    


    /* Widgets */

    /** shell widget holding it all */
    public Shell    pn_shellwidget = null;    

    /** text widget of the note */
    public Text     pn_textwidget = null;     

    /** save button */
    public MenuItem pn_savewidget = null;     


    /* Size, Location */

    /** x coord of shell widget */
    public int  pn_shellx  = -1;    

    /** y coord of shell widget */
    public int  pn_shelly  = -1;    

    /** width of screen note was last displayed on. */
    public int  pn_screenw = -1;    

    /** height of screen note was last displayed on. */
    public int  pn_screenh = -1;    

    /** width of text widget window */
    public int  pn_textw   = -1;    

    /** height of text widget window */
    public int  pn_texth   = -1;    
}
