package com.ximba.xnotesng.apps.xnotes;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.printing.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;

/**
 * <p> 
 * Manage a print job.
 * </p>
 * <p> 
 * Most of this was shamelessly stolen from 
 * http://www.java2s.com/Code/Java/SWT-JFace-Eclipse/PrintingExample.htm
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class PrintJob extends Thread {

    String printName = null;
    Printer printer = null;
    XNoteData nd = null;
    GC gc = null;
    Font printerFont = null;
    Font font = null;
    Color printerForegroundColor = null;
    Color printerBackgroundColor = null;
    Color foregroundColor = null;
    Color backgroundColor = null;

    int lineHeight = 0;
    int tabWidth = 0;
    int leftMargin, rightMargin, topMargin, bottomMargin;
    int x, y;
    int index, end;
    String textToPrint;
    String tabs;
    StringBuffer wordBuffer;

    private static final String S_SEPARATOR = 
        "______________________________________________________________________________";

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires a Printer object and the XNoteData object to be printed.
     */
    public PrintJob(Printer printer, XNoteData nd) {
        this.printer = printer;
        this.nd = nd;
        printName = nd.pn_name;
        if ( printName == null ) printName = "note" + nd.pn_index;
        StringBuffer data = new StringBuffer("File: " + printName + "\n");
        data.append(S_SEPARATOR + "\n\n");
        data.append(nd.pn_text);
        textToPrint = data.toString();

        font = new Font(Display.getDefault(), "Courier", 10, SWT.NORMAL);
        foregroundColor = Display.getDefault().getSystemColor(SWT.COLOR_BLACK);
        backgroundColor = Display.getDefault().getSystemColor(SWT.COLOR_WHITE);
    }

    /*
     * ------------------------------------------------------------------
     * Public methods
     * ------------------------------------------------------------------
     */

    /* Run a print job in a separate thread. */
    public void run() {

        printSetup(printName);

        /* Print text to current gc using word wrap */
        printText();
        printer.endJob();

        /* Cleanup graphics resources used in printing */
        printerFont.dispose();
        printerForegroundColor.dispose();
        printerBackgroundColor.dispose();
        gc.dispose();
  
    }


    /*
     * ------------------------------------------------------------------
     * Private methods.
     * ------------------------------------------------------------------
     */

    /**
     * Setup the page layout, fonts and colors.
     * @param jobName   The name to display when the job is printed.
     */
    private void printSetup(String jobName) {
        if ( printer.startJob( jobName ) ) 
        {   
            Rectangle clientArea = printer.getClientArea();
            Rectangle trim = printer.computeTrim(0, 0, 0, 0);
            Point dpi = printer.getDPI();

            // one inch from left side of paper
            leftMargin = dpi.x + trim.x; 

            // one inch from right side of paper
            rightMargin = clientArea.width - dpi.x + trim.x + trim.width; 

            // one inch from top edge of paper
            topMargin = dpi.y + trim.y; 

            // one inch from bottom edge of paper
            bottomMargin = clientArea.height - dpi.y + trim.y + trim.height; 
      
            /* Create a buffer for computing tab width. */
            int tabSize = 4; 
            StringBuffer tabBuffer = new StringBuffer(tabSize);
            for (int i = 0; i < tabSize; i++) tabBuffer.append(' ');
            tabs = tabBuffer.toString();

            /* Create printer GC, and create and set the printer font & foreground color. */
            gc = new GC(printer);
      
            FontData fontData = font.getFontData()[0];
            printerFont = new Font(printer, fontData.getName(), fontData.getHeight(), fontData.getStyle());
            gc.setFont(printerFont);
            tabWidth = gc.stringExtent(tabs).x;
            lineHeight = gc.getFontMetrics().getHeight();
      
            RGB rgb = foregroundColor.getRGB();
            printerForegroundColor = new Color(printer, rgb);
            gc.setForeground(printerForegroundColor);
    
            rgb = backgroundColor.getRGB();
            printerBackgroundColor = new Color(printer, rgb);
            gc.setBackground(printerBackgroundColor);
        }
    }

    /** Print the text of the note to the graphics context. */
    private void printText() {
        printer.startPage();
        wordBuffer = new StringBuffer();
        x = leftMargin;
        y = topMargin;
        index = 0;
        end = textToPrint.length();
        while (index < end) 
        {
            char c = textToPrint.charAt(index);
            index++;

            if (c != 0) 
            {
                if (c == 0x0a || c == 0x0d) 
                {
                    if (c == 0x0d && index < end && textToPrint.charAt(index) == 0x0a)
                        index++; // if this is cr-lf, skip the lf
                    printWordBuffer();
                    newline();
                } 
                else 
                {
                    if (c != '\t')
                        wordBuffer.append(c);
                    if (Character.isWhitespace(c)) 
                    {
                        printWordBuffer();
                        if (c == '\t')
                            x += tabWidth;
                    }
                }
            }
        }

        if (y + lineHeight <= bottomMargin)
            printer.endPage();
    }

    /** Add a word to the graphics context, wrapping if necesary. */
    void printWordBuffer() 
    {
        if (wordBuffer.length() > 0) 
        {
            String word = wordBuffer.toString();
            int wordWidth = gc.stringExtent(word).x;
            if (x + wordWidth > rightMargin) {
                /* word doesn't fit on current line, so wrap */
                newline();
            }
            gc.drawString(word, x, y, false);
            x += wordWidth;
            wordBuffer = new StringBuffer();
        }
    }

    /** Add a new line to the graphics context. */
    private void newline() 
    {
        x = leftMargin;
        y += lineHeight;
        if (y + lineHeight > bottomMargin) {
            printer.endPage();
            if (index + 1 < end) {
                y = topMargin;
                printer.startPage();
            }
        }
    }
}
