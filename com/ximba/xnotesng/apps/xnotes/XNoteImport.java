package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.xnotes.XNotes.NoteSize;


/**
 * <p> 
 * Class for importing various file types into XNotes application.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class XNoteImport {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.xnotes.XNoteImport");

    /*
     * Post-It Note file information.  
     */
    static final String StickyNoteMagic         = "%!<postitnote>";
    static final String StickyNoteMagic_v2      = "%%!!<postitnote>";
    static final String StickyNoteMagic_v3      = "%%!!<xpstickynote.v3>";
    static final String StickyNoteMagic_v4      = "%%!!<xpstickynote.v4>";
    static final String StickyNoteFname         = "note";

    /* Saved configuration data. */
    static CoreInitData cid = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires reference to global CoreInitData object.
     */
    public XNoteImport(CoreInitData cid) 
    {
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /** 
     * Determine the version of an older version of XNotes files.
     * @param line  The string holding (possibly) the version information.
     */
    private int getNoteVersion(String line)
    {
        /* If its the current version, life is simple. */
        if ( line.indexOf(StickyNoteMagic_v3) == 0 )
            return(3);

        /* Is it a version 2 note? */
        if ( line.indexOf(StickyNoteMagic_v2) == 0 )
            return(2);

        /* Is it a version 1 note? This might not work. */
        if ( line.indexOf(StickyNoteMagic) == 0 )
            return(1);

        /* Must not be an XNote. */
        return(-1);
    }


    /*
     * =======================================================
     * I/O specific methods.
     * =======================================================
     */

    /**
     * Import older versions of XNote files.
     * <ul>
     * <li> pn_index does not get set by this method.  The caller must set it. </li>
     * <li> pn_file does not get set by this method.  The caller must set it. </li>
     * </ul>
     * @param filename      Fully qualified path to the file to import.
     * @return An XNoteData object for the imported file.
     */
    public XNoteData xnote(String filename) throws Exception
    {
        File file = new File( filename );
        if ( !file.exists() )
            throw new Exception( Messages.S_NOSUCHFILE + filename );

        /* Read the file into a list, with the config line held separate. */
        StringBuilder noteText = new StringBuilder("");
        String name = null;
        String config = null;
        try {
            FileReader fr = new FileReader(file);
            BufferedReader in = new BufferedReader(fr);
            String line = null;
            while ( (line=in.readLine()) != null )
            {
                if ( config == null ) config = line;
                else if ( name == null ) name = line;
                else 
                {
                    if ( noteText.length() > 0 )
                        noteText.append( "\n" );
                    noteText.append( line );
                }
            }
            fr.close();
        }
        catch (Exception e) {
            throw new Exception( Messages.S_READFAIL + filename + ": " + e.getMessage() );
        }

        /* Allocate a note and fill it in. */
        XNoteData nf = new XNoteData();

        /* Get the note's version */
        int version = getNoteVersion(config);

        /* First field is always the version string. */
        String[] fields = config.split(" ");
        switch ( version )
        {
            /* XNotesPlus V3. */
            case 3:
                if ( fields.length < 19 )
                    throw new Exception( Messages.S_READFAIL + filename + ": Improper format for V3 file.");
                nf.pn_magic = StickyNoteMagic_v4;
                nf.pn_shellx = Integer.parseInt( fields[1] );
                nf.pn_shelly = Integer.parseInt( fields[2] );
                nf.pn_texth = Integer.parseInt( fields[3] );
                nf.pn_textw = Integer.parseInt( fields[4] );
                // field 5 is unnecessary for an import.
                nf.pn_hidden = Boolean.parseBoolean( fields[6] );
                // field 7 is unnecessary for an import.
                nf.pn_screenh = Integer.parseInt( fields[8] );
                nf.pn_screenw = Integer.parseInt( fields[9] );
                // Rest of fields are ignored - alarms are handled differently.
                break;

            /* First major release. */
            case 2:
                if ( fields.length < 15 )
                    throw new Exception( Messages.S_READFAIL + filename + ": Improper format for V2 file.");
                nf.pn_magic = StickyNoteMagic_v4;
                nf.pn_shellx = Integer.parseInt( fields[1] );
                nf.pn_shelly = Integer.parseInt( fields[2] );
                nf.pn_texth = Integer.parseInt( fields[3] );
                nf.pn_textw = Integer.parseInt( fields[4] );
                // field 5 is unnecessary for an import.
                nf.pn_hidden = Boolean.parseBoolean( fields[6] );
                // field 7 is unnecessary for an import.
                nf.pn_screenh = Integer.parseInt( fields[8] );
                nf.pn_screenw = Integer.parseInt( fields[9] );
                // Rest of fields are ignored - alarms are handled differently.
                break;

            /* Original version, back in the dark ages of X11R4 */
            case 1:
                if ( fields.length < 6 )
                    throw new Exception( Messages.S_READFAIL + filename + ": Improper format for V1 file.");
                nf.pn_magic = StickyNoteMagic_v4;
                nf.pn_shellx = Integer.parseInt( fields[1] );
                nf.pn_shelly = Integer.parseInt( fields[2] );
                nf.pn_texth = Integer.parseInt( fields[3] );
                nf.pn_textw = Integer.parseInt( fields[4] );
                // field 5 is unnecessary for an import.
                nf.pn_hidden = Boolean.parseBoolean( fields[6] );
                nf.pn_screenw = Integer.parseInt( fields[7] );
                nf.pn_screenh = Integer.parseInt( fields[8] );
                break;

            default:
                throw new Exception( Messages.S_READFAIL + filename + ": Unknown XNotes format.");
        }

        /* The name of the note was always the second line. */
        if ( name != null ) nf.pn_name = new String( name );

        /* Error check the size and position */
        NoteSize ns = XNotes.getSize( XNotes.SMALL );
        int width = XNoteUI.getWidth(ns);
        int height = XNoteUI.getHeight(ns);
        if ( (nf.pn_textw < width) || (nf.pn_texth < height) )
        {
            nf.pn_textw = width;
            nf.pn_texth = height;
        }
        if ( (nf.pn_shellx < 0) || (nf.pn_shelly < 0) )
        {
            nf.pn_shellx = 0;
            nf.pn_shelly = 0;
        }

        /* Save the note text, if any. */
        if ( noteText.length() > 0 )
            nf.pn_text = new String ( noteText.toString() );

        /* Return the note. */
        return nf;
    }

    /**
     * Import a simple text file.
     * <ul>
     * <li> pn_index does not get set by this method.  The caller must set it. </li>
     * <li> pn_file does not get set by this method.  The caller must set it. </li>
     * </ul>
     * @param filename      Fully qualified path to the file to import.
     * @return An XNoteData object for the imported file.
     */
    public XNoteData text(String filename) throws Exception
    {
        File file = new File( filename );
        if ( !file.exists() )
            throw new Exception( Messages.S_NOSUCHFILE + filename );

        /* Read the file into a single string. */
        StringBuilder noteText = new StringBuilder("");
        try {
            FileReader fr = new FileReader(file);
            BufferedReader in = new BufferedReader(fr);
            String line = null;
            while ( (line=in.readLine()) != null )
            {
                if ( noteText.length() > 0 )
                    noteText.append( "\n" );
                noteText.append( line );
            }
            fr.close();
        }
        catch (Exception e) {
            throw new Exception( Messages.S_READFAIL + filename + ": " + e.getMessage() );
        }

        /* Allocate a note and fill it in. */
        XNoteData nf = new XNoteData();
        nf.pn_magic = StickyNoteMagic_v4;
        nf.pn_name = new String( file.getName() );
        nf.pn_textw = XNoteUI.getWidth( XNotes.getSize(XNotes.SMALL) );
        nf.pn_texth = XNoteUI.getHeight( XNotes.getSize(XNotes.SMALL) );

        /* Center the note on the display. */
        nf.pn_shellx = (int)(XNoteUI.getDisplayWidth()/2) - (int)(nf.pn_textw/2);
        nf.pn_shelly = (int)(XNoteUI.getDisplayHeight()/2) - (int)(nf.pn_texth/2);
        nf.pn_hidden = false;
        nf.pn_screenw = XNoteUI.getDisplayWidth();
        nf.pn_screenh = XNoteUI.getDisplayHeight();

        /* Save the note text, if any. */
        if ( noteText.length() > 0 )
            nf.pn_text = new String ( noteText.toString() );

        /* Return the note. */
        return nf;
    }
}
