package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.xnotes.XNotes.NoteSize;


/**
 * <p> 
 * Class for exporting note data to various file types.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class XNoteExport {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.xnotes.XNoteExport");

    /* Saved configuration data. */
    static CoreInitData cid = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires reference to global CoreInitData object.
     */
    public XNoteExport(CoreInitData cid) 
    {
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */


    /*
     * =======================================================
     * I/O specific methods.
     * =======================================================
     */

    /**
     * Export a note to the specified file as ordinary text.
     * Only the name of the note and the note text are saved.
     * @param path      Directory or fully qualified path to the file to export.
     *                  If a directory, it must exist (existence is not checked here).
     * @param xd        The XNoteData object containing the data to export.
     */
    public void text(String path, XNoteData xd) throws Exception
    {
        String filename = null;

        /* Is this a directory or a fully qualified filename? */
        if ( path.substring(path.length()-1).equals(File.separator) )
            filename = path + "note" + xd.pn_index;
        else
            filename = path;

        File file = new File( filename );
        try {
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);
            pw.println( xd.pn_text );
            fw.close();
        }
        catch (Exception e) {
            MessageDialog md = new MessageDialog(XNotes.getShell());
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage("Failed to write file (" + filename + ")");
            md.open();
            throw e;
        }
    }
}
