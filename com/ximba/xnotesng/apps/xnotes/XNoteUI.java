package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.printing.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.category.*;
import com.ximba.xnotesng.apps.xnotes.XNotes.NoteSize;


/**
 * <p> 
 * A sticky note window.
 * </p>
 *
 * @adm $Revision: 1.4 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class XNoteUI {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.XNoteUI");

    private CoreInitData cid = null;

    /* Instance Variables */
    Shell shell = null;
    Text text = null;
    Text searchText = null;
    XNoteData noteData = null;
    XNotes note = null;
    Image searchForwardIcon = null;
    Image searchBackIcon = null;
    boolean searchBackward = false;

    public static final String S_CHOOSE = "Choose a ";

    /* Menus */
    private static final int M_FILE_LOAD        = 0;
    private static final int M_FILE_SAVE        = 1;
    private static final int M_FILE_EXPORT      = 2;
    private static final int M_FILE_SEARCH      = 3;
    private static final int M_FILE_PRINT       = 4;
    private static String[] FileMenuItems = {
        "&Open",
        "&Save",
        "&Export",
        "&Search",
        "&Print",
    };
    private static final int M_OPTION_HIDE      = 0;
    private static final int M_OPTION_ERASE     = 1;
    private static final int M_OPTION_DESTROY   = 2;
    private static final int M_OPTION_NAME      = 3;
    private static final int M_OPTION_CATEGORY  = 4;
    private static final int M_OPTION_DATE      = 5;
    private static String[] OptionMenuItems = {
        "&Hide",
        "&Erase",
        "&Destroy",
        "&Name",
        "&Category",
        "Insert Da&te",
    };
    private static final int M_ALARM_SET        = 0;
    private static final int M_ALARM_CLEAR      = 1;
    private static String[] AlarmMenuItems = {
        "&Set",
        "&Clear",
    };


    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Default constructor creates the actual note.  Class methods are used to manage it.
     * The note size will be converted to pixels based on the displays DPI.
     * @param cid       A reference to the global CoreInitData object.
     * @param parent    A reference to the parent for the note (the non-visible shell of the XNotes object).
     * @param noteSize  The size of the note in inches.
     * @param note      A reference to note data to display in the note and which will be 
     *                  managed by this visible window.
     */
    public XNoteUI (CoreInitData cid, Shell parent, XNotes.NoteSize noteSize, XNotes note) { 

        FormData fd = null;

        /* Save the parent class reference. */
        this.note = note;

        Map<String, Image> images = XNotes.getImages();
        searchForwardIcon = images.get("searchForwardIcon");
        searchBackIcon = images.get("searchBackIcon");

        /* Create a modeless shell for each note. */
        shell = new Shell( parent, SWT.SHELL_TRIM | SWT.MODELESS );

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                close();
            }
        });

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        shell.setLayout(fl);

        /*
         * ------------------------------------------------------
         * Menus
         * ------------------------------------------------------
         */

        /* Add a menu bar */
        Menu menuBar = new Menu(shell, SWT.BAR);
        shell.setMenuBar( menuBar );

        /* Add File Menu */
        MenuItem menuBar_File= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_File.setText ("&File");
        Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_File.setMenu( fileMenu );

        for (int i=0; i<FileMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (fileMenu, SWT.CASCADE);
            item.setText( FileMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    fileMenuHandler(idx);
                }
            });
        }

        /* Add Option Menu */
        MenuItem menuBar_Option= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_Option.setText ("&Options");
        Menu optionMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_Option.setMenu( optionMenu );

        for (int i=0; i<OptionMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (optionMenu, SWT.CASCADE);
            item.setText( OptionMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    optionMenuHandler(idx);
                }
            });
        }

        /* Add Alarm Menu */
/*
 * Add this back in when alarms are implemented. 

        MenuItem menuBar_Alarm= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_Alarm.setText ("&Alarm");
        Menu alarmMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_Alarm.setMenu( alarmMenu );

        for (int i=0; i<AlarmMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (alarmMenu, SWT.CASCADE);
            item.setText( AlarmMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    alarmMenuHandler(idx);
                }
            });
        }
*/

        /*
         * ------------------------------------------------------
         * Search Widget
         * ------------------------------------------------------
         */

        Composite searchComposite = new Composite(shell, SWT.BORDER);
        fd = new FormData();
        fd.top = new FormAttachment(0,0);
        fd.left = new FormAttachment(0,0);
        fd.right = new FormAttachment(100,0);
        searchComposite.setLayoutData(fd);
        searchComposite.setBackground(XNotes.getBG());

        /* The top level composite has 1 column. */
        GridLayout gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 3;
        gl.makeColumnsEqualWidth = false;
        searchComposite.setLayout(gl);

        searchText = new Text(searchComposite, SWT.FLAT);
        GridData gd = new GridData(GridData.FILL_BOTH); 
        searchText.setLayoutData(gd);
        Font font = new Font(Display.getDefault(),"Verdana",8,SWT.NONE);
        searchText.setFont( font );
        searchText.setBackground(XNotes.getBG());

        Button searchButton = new Button(searchComposite, SWT.PUSH | SWT.FLAT);
        searchButton.setImage( searchForwardIcon );
        searchButton.setBackground(XNotes.getBG());
        searchButton.setToolTipText("Search forward");
        searchButton.addSelectionListener(new SelectionListener() {
            public void widgetDefaultSelected(SelectionEvent e) { }
            public void widgetSelected(SelectionEvent e) {
                searchBackward = false;
                searchIt();
            }
        });

        final Button directionButton = new Button(searchComposite, SWT.TOGGLE | SWT.FLAT);
        directionButton.setImage( searchBackIcon );
        directionButton.setBackground(XNotes.getBG());
        directionButton.setToolTipText("Search backward");
        directionButton.addSelectionListener(new SelectionListener() {
            public void widgetDefaultSelected(SelectionEvent e) { }
            public void widgetSelected(SelectionEvent e) { 
                searchBackward = true;
                searchIt();
            }
        });

        /*
         * ------------------------------------------------------
         * Note area
         * ------------------------------------------------------
         */

        /* The text are for the note. */
        text = new Text(shell, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
        text.setBackground(XNotes.getBG());

        fd = new FormData();
        fd.top = new FormAttachment(searchComposite);
        fd.left = new FormAttachment(0,0);
        fd.right = new FormAttachment(100,0);
        fd.bottom = new FormAttachment(100,0);
        text.setLayoutData(fd);

        font = new Font(Display.getDefault(),"Verdana",8,SWT.NONE);
        text.setFont( font );

        /* Set the initial size of the note. */
        Point pt = Display.getDefault().getDPI();
        int width = (int) ((float)pt.x * noteSize.x);
        int height = (int) ((float)pt.y * noteSize.y);
        text.setSize( text.computeSize(width, height, true) );
        shell.setSize( shell.computeSize(width, height, true) );
    }

    /**
     * Display the note.
     */
    public void open () {
        shell.open();
    }

    /**
     * Close the note.  This gets rid of the window and saves the contents.
     * Typically called when the application is exiting.
     */
    public void close () {
        shell.dispose();
    }

    /**
     * Set the data associated with this window.  The window methods manage this data.
     * @param nf    The note data to associate with this window.
     */
    public void setData (XNoteData nf) {
        noteData = nf;
        if ( noteData.pn_text != null )
            text.setText( noteData.pn_text );
        if ( noteData.pn_name != null )
            shell.setText( noteData.pn_name );

        /* Set the size of the note, if known. */
        if ( (nf.pn_textw != -1) && (nf.pn_texth != -1) )
        {
            Point pt = Display.getDefault().getDPI();
            int width = nf.pn_textw;
            int height = nf.pn_texth;
            text.setSize( text.computeSize(width, height, true) );
            shell.setSize( shell.computeSize(width, height, true) );
        }

        /* Set the position of the note, if known. */
        if ( (nf.pn_shellx != -1) && (nf.pn_shelly != -1) )
            shell.setLocation( nf.pn_shellx, nf.pn_shelly );

        /* Set the category if possible. */
        if ( nf.pn_category != null )
        {
            CategoryData cd = null;
            try { cd = Category.findByID( nf.pn_category ); }
            catch (Exception e) {
                log.error("Note " + nf.pn_index + " category no longer exists. Reverting to default.");
                nf.pn_category = null;
                return;
            }
            text.setBackground( cd.bgColor );
            text.setForeground( cd.fgColor );
        }

        /* Hide the note, if requested. */
        if ( noteData.pn_hidden )
            shell.setVisible(false); 
        save();
    }

    /**
     * Set the text associated with this window.
     * @param data    The new text, replacing any existing text.  A null value will clear the text.
     */
    public void setText (String data) {
        if ( data != null )
        {
            noteData.pn_text = data;
            text.setText( data );
        }
        else 
        {
            noteData.pn_text = null;
            text.setText( "" );
        }
    }

    /**
     * Set the name associated with this window.  A null value is silently ignored.
     * @param data    The new note name, replacing any existing name.
     */
    public void setName (String data) {
        if ( data != null )
        {
            noteData.pn_name = data;
            shell.setText( data );
        }
    }

    /**
     * Get the data associated with this note.
     * @return The XNoteData object, updated with any recent changes.
     */
    public XNoteData getData() {
        noteData.pn_text = text.getText();

        /* Save the size and position of the note. */
        Point pt = text.getSize();
        noteData.pn_textw = pt.x;
        noteData.pn_texth = pt.y;
        pt = text.getLocation();
        noteData.pn_shellx = pt.x;
        noteData.pn_shelly = pt.y;

        return noteData;
    }

    /** 
     * Utility for computing a pixel width.
     * @param ns    The note size to convert, in inches.
     * @return The converted pixel width.
     */
    public static int getWidth(NoteSize ns)
    {
        Point pt = Display.getDefault().getDPI();
        int val = (int) ((float)pt.x * ns.x);
        return val;
    }

    /** 
     * Utility for computing a pixel height.
     * @param ns    The note size to convert, in inches.
     * @return The converted pixel height.
     */
    public static int getHeight(NoteSize ns)
    {
        Point pt = Display.getDefault().getDPI();
        int val = (int) ((float)pt.y * ns.y);
        return val;
    }

    /** 
     * Utility for retrieve the display width.
     * @return The display width in pixels.
     */
    public static int getDisplayWidth()
    {
        Point pt = Display.getDefault().getDPI();
        return pt.x;
    }

    /** 
     * Utility for retrieve the display height.
     * @return The display height in pixels.
     */
    public static int getDisplayHeight()
    {
        Point pt = Display.getDefault().getDPI();
        return pt.y;
    }

    /** 
     * Retrieve the current position of the carat in the text.
     * @param backup    If true, move the caret to the start of the current selection, if any.
     * @return The offset from the start of the text where the caret is currently positioned.
     */
    public int getCaretPosition(boolean backup) { 

        /* if requested, backup to the front of the current selection, if any. */
        if ( backup )
        {
            if ( text.getSelectionCount() > 0 )
                text.setSelection( text.getCaretPosition() - text.getSelectionCount() );
        }
        return text.getCaretPosition(); 
    }

    /** 
     * Select a block of text and make it visible.  If the length will be longer than
     * the end of the text then the selection will only run through the end of the text.
     * @param start     The start location within the text to start the selection.
     * @param length    The length of the selection.
     */
    public void selectText(int start, int length) { 
        text.setRedraw(false);
        text.clearSelection();
        text.setSelection(start, start+length);
        text.showSelection();
        text.setRedraw(true);
    }

    /*
     * =======================================================
     * File Menu Handlers
     * =======================================================
     */

    /**
     * File Menu handlers
     */
    private void fileMenuHandler(int i)
    {
        switch (i) 
        {
            case M_FILE_LOAD       : load(); break;
            case M_FILE_SAVE       : save(); break;
            case M_FILE_EXPORT     : export(null); break;
            case M_FILE_SEARCH     : search(); break;
            case M_FILE_PRINT      : print(); break;
        }
    }

    /**
     * Load a text file into the note.
     */
    public void load() { 
        ImportDialog id = new ImportDialog(cid, shell);
        java.util.List<XNoteData> list = id.open();
        if ( list == null )
            return;
        Iterator it = list.iterator();
        while (it.hasNext())
        {
            XNoteData nf = (XNoteData)it.next();
            text.append(nf.pn_text);
        }
    }

    /**
     * Save the current note to file.
     */
    public void save() { 

        if ( text.isDisposed() )
            return;

        /* Save the size and position of the note. */
        Point pt = text.getSize();
        noteData.pn_textw = pt.x;
        noteData.pn_texth = pt.y;

        pt = shell.getLocation();
        noteData.pn_shellx = pt.x;
        noteData.pn_shelly = pt.y;

        /* Save the text from the note. */
        noteData.pn_text = text.getText();

        try {
            XNoteIO nio = new XNoteIO(cid);
            nio.saveNote( noteData ); 
        }
        catch (Exception e) {
            e.printStackTrace();
            String msg = "Failed to save note " + noteData.pn_index + ": " + e.getMessage();
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage(msg);
            md.open();
            return;
        }
    }

    /**
     * Save the note text to a specified file.
     * @param path      The path to the file to save the text.
     */
    private void export( String path ) { 
        String filename = null;
        if ( path == null )
        {
            DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
            dlg.setText(S_CHOOSE + "directory");
            filename = dlg.open();
            if ( filename == null )
                return;
            File file = new File( filename );
            if ( !file.exists() || !file.isDirectory() )
            {
                String msg = "That's not a directory.";
                MessageDialog md = new MessageDialog(shell);
                md.setMode(MessageDialog.M_ERROR);
                md.setMessage(msg);
                md.open();
                return;
            }
            /* Push a file separator on the end of the path. */
            filename = filename + File.separator;
        }
        else
        {
            /* This only gets called via the Print dialog. */
            if ( path.indexOf("file://") == 0 )
                filename = path.substring(7);
            else
                filename = path;
        }

        if ( filename == null )
            return;
        XNoteExport xne = new XNoteExport(cid);
        noteData.pn_text = text.getText();
        try { xne.text( filename, noteData ); }
        catch (Exception e) {
            e.printStackTrace();
            log.error( e.getMessage() );
        }
    }

    /**
     * Search the current note for a phrase
     */
    private void search() { 
        SearchDialog sd = new SearchDialog(shell, this);
        sd.open();
    }

    /** Search the note and reposition it accordingly. */
    private void searchIt()
    {
        String searchStr = searchText.getText();
        if ( searchStr.length() == 0 )
        {
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage(Messages.S_SEARCHTERMREQUIRED);
            md.open();
            return;
        }

        XNoteData xd = getData();
        String data = new String(xd.pn_text);
        data = data.toLowerCase();
        searchStr = searchStr.toLowerCase();

        int currentPos = -1;
        int newPos = -1;
        if ( !searchBackward )
        {
            currentPos = getCaretPosition(false);
            newPos = data.indexOf(searchStr, currentPos);
        }
        else
        {
            currentPos = getCaretPosition(true);
            newPos = data.lastIndexOf(searchStr, currentPos-1);
        }
        if ( newPos != -1 )
            selectText(newPos, searchStr.length());
    }

    /**
     * Print this note.
     */
    private void print() { 
        PrintDialog pd = new PrintDialog(shell);
        PrinterData pData = pd.open();
        if (pData == null) 
            return;

        /* Update the note data object from the widget. */
        save();

        /* If printing to file, handle that. */
        if (pData.printToFile) 
        {
            /* Ask for an output filename */
            if ( (pData.fileName == null) || (pData.fileName.length() == 0) )
            {
                /* This is a recursive call. */
                MessageDialog md = new MessageDialog(shell);
                md.setMode( MessageDialog.M_ERROR );
                md.setMessage( Messages.S_MISSINGFILENAME );
                md.open();
                print();
                return;
            }
            export(pData.fileName);
            return;
        }
    
        /* Do the printing in a background thread so that spooling does not freeze the UI. */
        final Printer printer = new Printer(pData);
        PrintJob pj = new PrintJob(printer, noteData);
        Thread printingThread = new Thread(pj);
        printingThread.start();
    }


    /*
     * =======================================================
     * Option Menu Handlers
     * =======================================================
     */

    /**
     * Options Menu handlers
     */
    private void optionMenuHandler(int i)
    {
        switch (i) 
        {
            case M_OPTION_HIDE      : hide(); break;
            case M_OPTION_ERASE     : erase(); break;
            case M_OPTION_DESTROY   : destroy(); break;
            case M_OPTION_NAME      : name(); break;
            case M_OPTION_CATEGORY  : category(); break;
            case M_OPTION_DATE      : date(); break;
        }
    }

    /** Hide the window.  This removes it from sight but does not delete it from the system. */
    public void hide() { 
        noteData.pn_hidden = true; 
        save();
        shell.setVisible(false); 
    }

    /** Show the window.  This can only be called from the top level dialogs or menus. */
    public void show() { 
        noteData.pn_hidden = false; 
        shell.setVisible(true); 
        shell.forceActive(); 
    }

    /** Remove all text from the visible note.  This does not automatically save to disk! */
    private void erase() { text.setText(""); }

    /** Destroy the window.  This removes it completely, including cleaning up its saved file. */
    public void destroy() { 
        note.remove(this);
        XNoteIO nio = new XNoteIO(cid);
        nio.remove(noteData);
        shell.dispose(); 
    }

    /** Set the name of the note. */
    private void name() { 
        NameDialog nd = new NameDialog(shell);
        String name = nd.open(noteData.pn_name);
        if (name!=null)
        {
            noteData.pn_name = name;
            shell.setText(name);
            save();
        }
    }


    /** Choose a category for this note. */
    private void category() 
    { 
        CategoryUI ui = new CategoryUI(cid, shell);
        CategoryData cd = ui.dialog();
        if ( cd != null )
        {
            noteData.pn_category = cd.id;
            save();
            text.setBackground( cd.bgColor );
            text.setForeground( cd.fgColor );
        }
    }

    /** 
     * Insert a date. This will show a dialog to select a date and request 
     * which format to insert into the note at the current cursor location.
     */
    private void date() { 
        DateDialog dd = new DateDialog(shell);
        String date = dd.open();
        if ( date != null )
            text.append( date );
    }


    /*
     * =======================================================
     * Alarm Menu Handlers
     * =======================================================
     */

    /**
     * Alarm Menu handlers
     */
    private void alarmMenuHandler(int i)
    {
        switch (i) 
        {
            case M_ALARM_SET      : alarmSet(); break;
            case M_ALARM_CLEAR    : alarmClear(); break;
        }
    }

    private void alarmSet() { }
    private void alarmClear() { }
}
