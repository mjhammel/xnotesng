package com.ximba.xnotesng.apps.xnotes;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;

/**
 * <p> 
 * Select a file or directory to import, using one of the available import methods.
 * All imported files do not have their pn_index or pn_file set.  The caller must set these manually.
 * </p>
 * @see XNoteImport
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class ImportDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.ImportDialog");
    private CoreInitData cid = null;

    /* Saved configuration data. */
    boolean returnOkay = false;

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private boolean          isRunning = false;
    private Text             nameText = null;
    private Text             errText = null;
    private boolean          fileMode = true;
    private java.util.List<XNoteData> notes = new ArrayList<XNoteData>();
    private Image            fileIcon = null;
    private int              importType = B_XNOTE;

    public static final String S_TITLE  = "Import a ";
    public static final String S_CHOOSE = "Choose a ";
    public static final String S_TYPE   = "Type of import";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_ACCEPT = 1;
    public static final String[] bboxLabels  = {
        "Cancel",
        "Accept",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    /* Import Type Buttons */
    public static final int B_XNOTE = 0;
    public static final int B_TEXT  = 1;
    public static final String[] typeButtonLabels  = {
        "Older version XNote(s)",
        "Text File(s)",
    };

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires the global CoreInitData object and a 
     * parent shell.
     */
    public ImportDialog(CoreInitData cid, Shell parent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        Map<String, Image> images = XNotes.getImages();
        this.parent = parent;
        fileIcon = images.get("fileIcon");
        this.cid = cid;
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /*
     * Perform the import of the file(s) into an XNoteData object, if possible.
     * @return True if the import succeeds.  False otherwise.
     */
    private boolean accept()
    {
        errText.setText("");
        if ( nameText.getText().length() == 0 )
        {
            if ( fileMode ) errText.setText(Messages.S_MISSINGFILENAME);
            else            errText.setText(Messages.S_MISSINGDIRNAME);
            return false;
        }

        /* Process the nameText field to create a list of XNoteData */
        XNoteImport xni = new XNoteImport(cid);
        if ( fileMode )
        {
            /* Just import the single note. */
            bboxButtons[B_ACCEPT].setEnabled(false);
            try {
                XNoteData xd = null;
                switch ( importType )
                {
                    case B_XNOTE: xd = xni.xnote( nameText.getText() ); break;
                    case B_TEXT : xd = xni.text( nameText.getText() ); break;
                }
                if (xd != null)
                    notes.add( xd );
            }
            catch (Exception e) {
                errText.setText( e.getMessage() );
                bboxButtons[B_ACCEPT].setEnabled(true);
                return false;
            }
            bboxButtons[B_ACCEPT].setEnabled(true);
        }
        else
        {
            bboxButtons[B_ACCEPT].setEnabled(false);

            /* Open the directory and get a list of note files based on the filename. */
            File dir = new File( nameText.getText() );
            if ( !dir.exists() )
            {
                MessageDialog md = new MessageDialog(shell);
                md.setMode(MessageDialog.M_ERROR);
                md.setMessage("No such directory.");
                md.open();
                bboxButtons[B_ACCEPT].setEnabled(true);
                return false;
            }
            File[] noteFiles = dir.listFiles( new FilenameFilter(){
                    public boolean accept(File dir, String name) {
                        if ( name.indexOf("note") == 0 )
                            return true;
                        return false;
                    }
                });
            if ( noteFiles.length == 0 )
            {
                errText.setText( Messages.S_NONOTESFOUND );
                bboxButtons[B_ACCEPT].setEnabled(true);
                return false;
            }

            /* Import each file with the proper name. */
            for (int i=0; i<noteFiles.length; i++)
            {
                File file = noteFiles[i];

                /* Ignore files that are not notes.  Just log that they aren't. */
                try { 
                    XNoteData xd = null;
                    switch ( importType )
                    {
                        case B_XNOTE: xd = xni.xnote( file.getAbsolutePath() ); break;
                        case B_TEXT : xd = xni.text( file.getAbsolutePath() ); break;
                    }
                    if (xd != null)
                        notes.add( xd );
                }
                catch (Exception e) {
                    log.error( Messages.S_FAILEDIMPORT + file.getAbsolutePath());
                }
            }

            if ( notes.size() == 0 )
            {
                errText.setText( Messages.S_NONOTESFOUND );
                bboxButtons[B_ACCEPT].setEnabled(true);
                return false;
            }
            bboxButtons[B_ACCEPT].setEnabled(true);
        }

        returnOkay = true;
        return true;
    }

    /** Choose a file */
    private void selectFile()
    {
        FileDialog dlg = new FileDialog(shell, SWT.OPEN);
        dlg.setText(S_CHOOSE + "file");
        String filename = dlg.open();
        if ( filename == null )
            return;
        File file = new File( filename );
        if ( !file.exists() )
            return;
        if ( !file.isFile() )
            errText.setText("That's not a file.");
        else
            nameText.setText(filename);
    }

    /** Choose a directory */
    private void selectDir()
    {
        DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
        dlg.setMessage(S_CHOOSE + "directory");
        String filename = dlg.open();
        if ( filename == null )
            return;
        File file = new File( filename );
        if ( !file.exists() )
            return;
        if ( !file.isDirectory() )
            errText.setText("That's not a directory.");
        else
            nameText.setText(filename);
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /** Change to directory mode. Default is file mode. */
    public void setDirMode()
    {
        fileMode = false;
    }

    /** Close the dialog. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /**
     * Build and display the dialog.
     * @return A list of XNoteData objects, one for each imported file.
     */
    public java.util.List<XNoteData> open() 
    {
        isRunning = true;

        shell = new Shell(parent);
        if ( fileMode ) shell.setText(S_TITLE + "file");
        else            shell.setText(S_TITLE + "directory");

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        shell.pack();
        shell.open();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if (!Display.getDefault().readAndDispatch ()) Display.getDefault().sleep ();
        }

        String name =  nameText.getText();
        shell.close();
        shell.dispose();
        if ( returnOkay )
            return notes;
        else
            return null;
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 1 column. */
        gl = new GridLayout();
        gl.marginWidth = 5;
        gl.marginHeight = 5;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);

        /* Input field for name of file/directory. */
        nameText = new Text(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.widthHint = 200;
        nameText.setLayoutData(gd);

        /* Button to select a file/directory. */
        Button button = new Button(shell, SWT.NONE);
        button.setImage( fileIcon );
        button.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                errText.setText("");
                if ( fileMode ) selectFile();
                else            selectDir();
            }
        });

        /* Import type options. */
        Group typeComposite = new Group(shell, SWT.BORDER);
        typeComposite.setText(S_TYPE);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        typeComposite.setLayoutData(gd);
        gl = new GridLayout();
        gl.marginWidth = 5;
        gl.marginHeight = 5;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        typeComposite.setLayout(gl);

        for(int i=0; i<typeButtonLabels.length; i++)
        {
            Button btn = new Button(typeComposite, SWT.RADIO);
            btn.setText( typeButtonLabels[i] );
            gd = new GridData(GridData.FILL_BOTH); 
            btn.setLayoutData(gd);
            final int idx = i;
            btn.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    importType = idx;
                }
            });
            if ( i == B_XNOTE )
                btn.setSelection( true );
        }


        /* An area for error messages. */
        errText = new Text(shell, SWT.READ_ONLY);
        gd = new GridData(GridData.FILL_BOTH); 
        gd.horizontalSpan = 2;
        errText.setLayoutData(gd);
        errText.setBackground(shell.getBackground());
        errText.setFont(XNotes.getFont(XNotes.SMALL));

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            closeDialog();   
                            break;
                        case B_ACCEPT: 
                            if ( !accept() )
                                return;
                            closeDialog();  
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
