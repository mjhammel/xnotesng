package com.ximba.xnotesng.apps.category;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * An editor window for categories.
 * </p>
 *
 * @adm $Revision: 1.2 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class CategoryUI {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.category.CategoryUI");
    private static final String F_CACHE = "categories.dat";

    private CoreInitData cid = null;
    private Callback callerCB = null;
    boolean selectMode = false;
    boolean isRunning = true;

    /* Instance Variables */
    Shell shell = null;
    Shell parent = null;
    Text text = null;
    Color bgColor = null;
    Color fgColor = null;
    Button fgColorButton = null;
    Button bgColorButton = null;
    Table catTable = null;
    java.util.List<CategoryData> categories = new ArrayList<CategoryData>();
    CategoryData currentCategory = null;
    Text errMsg = null;

    /* Menus */
    private static final int M_QUIT = 0;
    String[] FileMenuItems = {
        "&Close",
    };

    private static final int F_NAME     = 0;
    String[] inputLabels = {
        "Name",
    };
    Text[] inputText = new Text[inputLabels.length];

    /* Button Box */
    public static final int B_ADD       = 0;
    public static final int B_DEL       = 1;
    public static final int B_UPD       = 2;
    public static final int B_RST       = 3;
    public static final String[] bboxLabels  = {
        "Add",
        "Delete",
        "Update",
        "Reset",
    };

    private static final String S_TITLE_S = "Select a Category";
    private static final String S_TITLE_M = "Manager Categories";
    private static final String S_FG      = "Foreground";
    private static final String S_BG      = "Background";
    private static final String S_ERRMSG  = "Error messages";

    private static final String[] catHdrs = {"Category", "FG Color", "BG Color"};
    private static final int[] catWidths =  { 120, 50, 50};

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires a reference to the global CoreInitData
     * and the parent shell for this window.
     * @param cid       Reference to the CoreInitData object.
     * @param parent    Parent shell for this window.
     * @param callerCB  A Callback object which will be called when this window closes.
     */
    public CategoryUI (CoreInitData cid, Shell parent, Callback callerCB) { 
        this.cid = cid;
        this.parent = parent;
        this.callerCB = callerCB;
    }

    /**
     * Constructor that does not use a callback.
     * @param cid       Reference to the CoreInitData object.
     * @param parent    Parent shell for this window.
     */
    public CategoryUI (CoreInitData cid, Shell parent) { 
        this.cid = cid;
        this.parent = parent;
    }

    /*
     * =======================================================
     * Public Methods
     * =======================================================
     */

    /** Display the category editor.  */
    public void open () {
        CategoryIO cio = new CategoryIO(cid);
        categories = cio.load();
        createContent();
        fillList();
        shell.open();
    }

    /** Open the window as a dialog instead of a standalone application. */
    public CategoryData dialog () {
        selectMode = true;
        CategoryIO cio = new CategoryIO(cid);
        categories = cio.load();
        createContent();
        shell.open();
        fillList();
        while (isRunning)
            if ( !Display.getDefault().readAndDispatch() ) Display.getDefault().sleep();
        shell.dispose();
        return currentCategory;
    }

    /**
     * Close the note.  This gets rid of the window and saves the contents.
     * Typically called when the application is exiting.
     */
    public void close() {
        if ( selectMode )
            isRunning = false;
        else
            shell.dispose();

        /* Tell the caller we've closed. */
        if ( callerCB != null )
            callerCB.run();
    }

    /** Retrieve the selected category.  */
    public CategoryData getData () {
        return currentCategory;
    }

    /**
     * Allow the caller to add an entry to the list.
     */
    protected boolean add(CategoryData cd)
    {
        /* Make sure named entry does not already exist. */
        if ( cd.name.length() == 0 )
        {
            setMsg(Messages.S_MISSINGNAME);
            return false;
        }

        /* Names are case insensitive. */
        Iterator it = categories.iterator();
        while( it.hasNext() )
        {
            CategoryData data = (CategoryData)it.next();
            if ( data.name.equalsIgnoreCase( cd.name ) )
            {
                setMsg(Messages.S_EXISTS);
                return false;
            }
        }

        categories.add(cd);
        CategoryIO cio = new CategoryIO(cid);
        cio.save(categories);
        fillList();
        return true;
    }


    /*
     * =======================================================
     * Private Methods
     * =======================================================
     */

    private void createContent()
    {
        Label label = null;
        GridData gd = null;
        GridLayout gl = null;

        /* Create a modeless shell. */
        shell = new Shell( parent, SWT.SHELL_TRIM | SWT.MODELESS );
        if ( selectMode )
            shell.setText(S_TITLE_S);
        else
            shell.setText(S_TITLE_M);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                close();
            }
        });

        /* Add a menu bar */
        Menu menuBar = new Menu(shell, SWT.BAR);
        shell.setMenuBar( menuBar );

        /* Add File Menu */
        MenuItem menuBar_File= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_File.setText ("&File");
        Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_File.setMenu( fileMenu );

        for (int i=0; i<FileMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (fileMenu, SWT.CASCADE);
            item.setText( FileMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    fileMenuHandler(idx);
                }
            });
        }

        /* A composite with two sides. */
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 5;
        if ( selectMode )
            gl.numColumns = 1;
        else
            gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);

        /* Left side: a list of current categories. */
        catTable = new Table(shell, SWT.VIRTUAL | SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION | SWT.V_SCROLL);
        gd = new GridData(GridData.FILL_BOTH);
        if ( selectMode )
            gd.heightHint = 240;
        catTable.setLayoutData(gd);
        catTable.setHeaderVisible(true);
        catTable.setLinesVisible(true);

        catTable.setRedraw(false);
        for(int i=0; i<catHdrs.length; i++)
        {
            TableColumn col = new TableColumn(catTable, SWT.LEFT);
            col.setResizable(true);
            col.setWidth(catWidths[i]);
            col.setText(catHdrs[i]);
        }

        catTable.addSelectionListener(new SelectionAdapter() {
            public void widgetDefaultSelected(SelectionEvent e) {
                if ( selectMode )
                {
                    Table table = (Table) e.widget;
                    TableItem[] rows = table.getSelection();
                    if ( rows.length == 0 )
                        return;
                    currentCategory = (CategoryData) rows[0].getData();
                }
                close();
            }
            public void widgetSelected(SelectionEvent e) {
                if ( !selectMode )
                {
                    Table table = (Table) e.widget;
                    TableItem[] rows = table.getSelection();
                    if ( rows.length == 0 )
                        return;
                    currentCategory = (CategoryData) rows[0].getData();
                    inputText[F_NAME].setText(currentCategory.name);
                    fgColorButton.setBackground( getFGColor( currentCategory ) );
                    fgColorButton.setData( getFGColor( currentCategory ) );
                    bgColorButton.setBackground( getBGColor( currentCategory ) );
                    bgColorButton.setData( getBGColor( currentCategory ) );
                }
            }
        });

        if ( !selectMode )
        {
            /* Right side:
             * - Name
             * - BG Color and chooser.
             * - FG Color and chooser.
             */
            Composite rightComposite = new Composite(shell, SWT.NONE);
            gd = new GridData(GridData.FILL_BOTH);
            rightComposite.setLayoutData(gd);
            gl = new GridLayout();
            gl.marginWidth = 0;
            gl.marginHeight = 0;
            gl.verticalSpacing = 2;
            gl.horizontalSpacing = 2;
            gl.numColumns = 2;
            gl.makeColumnsEqualWidth = false;
            rightComposite.setLayout(gl);
    
            for(int i=0; i<inputLabels.length; i++)
            {
                label = new Label(rightComposite, SWT.NONE);
                label.setText(inputLabels[i]);
                gd = new GridData(GridData.HORIZONTAL_ALIGN_END);
                label.setLayoutData(gd);
    
                inputText[i] = new Text(rightComposite, SWT.BORDER);
                gd = new GridData(GridData.FILL_HORIZONTAL);
                gd.widthHint = 120;
                inputText[i].setLayoutData(gd);
            }

            label = new Label(rightComposite, SWT.NONE);
            label.setText(S_FG);
            gd = new GridData(GridData.HORIZONTAL_ALIGN_END);
            label.setLayoutData(gd);

            final Button fgbutton = new Button(rightComposite, SWT.NONE);
            fgColorButton = fgbutton;
            gd = new GridData(GridData.FILL_HORIZONTAL);
            fgbutton.setLayoutData(gd);
            fgbutton.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    ColorDialog cd = new ColorDialog(shell);
                    RGB rgb = cd.open();
                    if ( rgb == null )
                        return;
                    fgColor = new Color(Display.getDefault(), rgb);
                    fgColorButton.setBackground(fgColor);
                    fgColorButton.setData(fgColor);
                }
            });
    
            label = new Label(rightComposite, SWT.NONE);
            label.setText(S_BG);
            gd = new GridData(GridData.HORIZONTAL_ALIGN_END);
            label.setLayoutData(gd);

            final Button bgbutton = new Button(rightComposite, SWT.NONE);
            bgColorButton = bgbutton;
            gd = new GridData(GridData.FILL_HORIZONTAL);
            bgbutton.setLayoutData(gd);
            bgbutton.addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    ColorDialog cd = new ColorDialog(shell);
                    RGB rgb = cd.open();
                    if (rgb == null)
                        return;
                    bgColor = new Color(Display.getDefault(), rgb);
                    bgColorButton.setBackground(bgColor);
                    bgColorButton.setData(bgColor);
                }
            });
    
            /* A place for error messages. */
            errMsg = new Text(rightComposite, SWT.BORDER | SWT.CENTER | SWT.READ_ONLY);
            gd = new GridData(GridData.FILL_HORIZONTAL);
            gd.horizontalSpan = 2;
            errMsg.setLayoutData(gd);
            errMsg.setToolTipText( S_ERRMSG );
    
            /* A button box on the right side. */
            Composite bc = new Composite(rightComposite, SWT.NONE);
            gd = new GridData(GridData.FILL_BOTH);
            gd.horizontalSpan = 2;
            bc.setLayoutData(gd);
    
            FormLayout fl = new FormLayout();
            fl.marginWidth = 0;
            fl.marginHeight = 0;
            bc.setLayout(fl);
    
            Button prevButton = null;
            for (int i=0; i < bboxLabels.length; i++)
            {
                final int idx = i;
                Button button = new Button(bc, SWT.NONE);
                button.setText( bboxLabels[i] );
                button.addSelectionListener(new SelectionAdapter() {
                    public void widgetSelected(SelectionEvent e) {
                        switch(idx) 
                        {
                            case B_ADD: add(); break;
                            case B_DEL: delete(); break;
                            case B_UPD: update(); break;
                            case B_RST: reset(); break;
                        }
                    }
                });
                FormData fd = new FormData();
                fd.bottom = new FormAttachment(100,0);
                if ( prevButton != null )
                    fd.right = new FormAttachment(prevButton);
                else
                    fd.right = new FormAttachment(100,0);
                button.setLayoutData(fd);
                prevButton = button;
            }
        }
        shell.pack();
    }
    /**
     * File Menu handlers.
     */
    private void fileMenuHandler(int i)
    {
        switch (i) 
        {
            case M_QUIT : close(); break;
        }
    }

    /**
     * Fill the list.
     */
    private void fillList()
    {
        catTable.setRedraw( false );
        catTable.removeAll();
        Iterator it = categories.iterator();
        while( it.hasNext() )
        {
            CategoryData cd = (CategoryData)it.next();
            TableItem row = new TableItem(catTable, SWT.NONE);
            row.setData( cd );

            row.setText(0, cd.name);
            if ( cd.fgColor != null )
                row.setBackground(1, cd.fgColor);
            else
                row.setBackground(1, Category.defaultFG);
            if ( cd.bgColor != null )
                row.setBackground(2, cd.bgColor);
            else
                row.setBackground(1, Category.defaultBG);
        }
        catTable.deselectAll( );
        catTable.setRedraw( true );
        reset();
    }

    /**
     * Add the manuel edit fields as a new entry to the list.
     */
    private void add()
    {
        clearMsg();
        CategoryData cd = new CategoryData();
        cd.name = inputText[F_NAME].getText();
        cd.fgColor = (Color) fgColorButton.getData();
        cd.bgColor = (Color) bgColorButton.getData();
        if ( add(cd) )
            fillList();
    }

    /**
     * Delete the current selected entry.
     */
    private void delete()
    {
        clearMsg();
        if ( currentCategory == null )
        {
            setMsg(Messages.S_NOSELECTION);
            return;
        }

        categories.remove( categories.indexOf(currentCategory) );
        CategoryIO cio = new CategoryIO(cid);
        cio.save(categories);
        fillList();
    }

    /**
     * Update the current entry.
     */
    private void update()
    {
        clearMsg();
        if ( currentCategory == null )
        {
            setMsg(Messages.S_NOSELECTION);
            return;
        }

        currentCategory.name = inputText[F_NAME].getText();
        currentCategory.fgColor = (Color) fgColorButton.getData();
        CategoryIO cio = new CategoryIO(cid);
        cio.save(categories);
        fillList();
    }

    /**
     * Get the foreground color for the specified category.  If not set, return the default color.
     */
    private Color getFGColor( CategoryData cd )
    {
        if ( cd.fgColor != null )
            return cd.fgColor;
        return Category.defaultFG;
    }

    /**
     * Get the background color for the specified category.  If not set, return the default color.
     */
    private Color getBGColor( CategoryData cd )
    {
        if ( cd.bgColor != null )
            return cd.bgColor;
        return Category.defaultBG;
    }

    /** Set the error message field. */
    private void setMsg( String msg ) { errMsg.setText( msg ); }

    /** Clear the error message field. */
    private void clearMsg( ) { errMsg.setText( "" ); }

    /**
     * Reset the input fields.
     */
    private void reset()
    {
        currentCategory = null;
        if ( !selectMode )
        {
            inputText[F_NAME].setText("");
            fgColorButton.setBackground(Category.defaultFG);
            fgColorButton.setData( new Color(Display.getDefault(), Category.defaultFG.getRGB()) );
            bgColorButton.setBackground(Category.defaultBG);
            bgColorButton.setData( new Color(Display.getDefault(), Category.defaultBG.getRGB()) );
        }
        catTable.deselectAll();
    }
}
