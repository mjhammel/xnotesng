package com.ximba.xnotesng.apps.category;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Class for handling Category IO to and from files.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class CategoryIO {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.category.CategoryIO");
    private static final String F_CACHE = "categories.dat";

    /* Saved configuration data. */
    static CoreInitData cid = null;

    java.util.List<CategoryData> categories = new ArrayList<CategoryData>();

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires a reference to the global CoreInitData object.
     * @param cid       Reference to the CoreInitData object.
     */
    public CategoryIO(CoreInitData cid) 
    {
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */


    /*
     * =======================================================
     * I/O specific methods.
     * =======================================================
     */

    /**
     * Load the cache data.
     * @return A list of CategoryData objects.
     */
    public java.util.List<CategoryData> load()
    {
        categories.clear();
        CacheMgr cm = CacheMgr.getInstance();
        java.util.List<String> entries = cm.loadConfig(F_CACHE);
        Iterator it = entries.iterator();
        while( it.hasNext() )
        {
            String line = (String) it.next();
            if ( !line.contains(":") )
                continue;

            String[] fields = line.split(":");
            if ( fields.length < 4 )
                continue;

            CategoryData cd = new CategoryData();
            int index = 0;
            cd.id = fields[ index++ ];
            cd.name = fields[ index++ ];

            if ( fields[index].equals("null") )
                cd.fgColor = null;
            else
            {
                String[] rgb = fields[ index ].split(",");
                cd.fgColor = new Color( Display.getDefault(), 
                            Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]), Integer.parseInt(rgb[2]) );
            }
            index++;
            if ( fields[index].equals("null") )
                cd.bgColor = null;
            else
            {
                String[] rgb = fields[ index ].split(",");
                cd.bgColor = new Color( Display.getDefault(), 
                            Integer.parseInt(rgb[0]), Integer.parseInt(rgb[1]), Integer.parseInt(rgb[2]) );
            }
            categories.add( cd );
        }
        return categories;
    }

    /**
     * Write the cache data.
     * @param categories    A list of CategoryData objects.
     */
    public void save( java.util.List<CategoryData> categories )
    {
        java.util.List<String> entries = new ArrayList<String>();

        Iterator it = categories.iterator();
        while( it.hasNext() )
        {
            CategoryData cd = (CategoryData)it.next();
            StringBuilder line = new StringBuilder(cd.id + ":");
            line.append(cd.name + ":");
            if ( cd.fgColor != null )
                line.append( cd.fgColor.getRed() + "," + cd.fgColor.getGreen() + "," + cd.fgColor.getBlue() + ":");
            else
                line.append( "null:" );
            if ( cd.bgColor != null )
                line.append( cd.bgColor.getRed() + "," + cd.bgColor.getGreen() + "," + cd.bgColor.getBlue() );
            else
                line.append( "null" );
            entries.add( line.toString() );
        }
        CacheMgr cm = CacheMgr.getInstance();
        cm.saveConfig(F_CACHE, entries);
    }
}
