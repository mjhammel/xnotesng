package com.ximba.xnotesng.apps.category;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Plugin class for the Category application of XNotesNG
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class Category extends Plugin implements ApplicationInterface {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.category.Category");
    private static Shell shell = null;

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;
    private static CategoryUI categoryUI = null;

    /** Default foreground color */
    public static final Color defaultFG = new Color(Display.getDefault(), 0, 0, 0);

    /** Default background color */
    public static final Color defaultBG = new Color(Display.getDefault(), 242, 234, 0);
    // public static final Color defaultBG = new Color(Display.getDefault(), 233, 233, 233);

    /** This Plugin ID. */
    private static final String PLUGIN_ID = "com.ximba.xnotesng.apps.category";

    /** The ApplicationRegistration for this plugin. */
    ApplicationRegistration appReg = null;


    /*
     * =======================================================
     * Constructor
     * This plugin does not have a constructor method.
     * =======================================================
     */

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /** Called when the category menu is selected from the XNotesNG menus */
    class CategoryMenuCB implements Callback {
        Category parent;
        int type;
        public CategoryMenuCB(Category parent) {
            this.parent = parent;
        }
        public void run() {
            parent.show();
        }
    }


    /*
     * =======================================================
     * Private methods
     * =======================================================
     */


    /*
     * =======================================================
     * JPF methods
     * =======================================================
     */

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStart">Plugin.doStart</a>}.
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStop">Plugin.doStop</a>}.
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }


    /*
     * =======================================================
     * ApplicationManagerInterface methods
     * =======================================================
     */

    /**
     * Close any note-related windows.
     */
    public void shutdown()
    {
        if ( shell != null )
            shell.dispose();
    }


    /**
     * This method is called once during application life cycle to allow
     * the plugin to initialize itself.
     * @param cid   The reference to the global CoreInitData object.
     */
    public void init(CoreInitData cid) throws Exception
    {
        this.cid = cid;

        /* Find the home directory for the plugin. */
        home = ApplicationManager.getHome(cid, PLUGIN_ID);
    }

    /**
     * Register the plugin.
     * @return An ApplicationRegistration object used by the ApplicationManager to setup menus and callbacks.
     */
    public ApplicationRegistration register()
    {
        log.info("Application registration: Category.");
        ApplicationRegistration ar = new ApplicationRegistration(this);
        try {
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"menu"), (Callback) new CategoryMenuCB(this));
        }
        catch (Exception e) {
            log.error("Error while registering plugin: reason = " + e.getMessage(), e);
            return null;
        }
        return ar;
    }

    /**
     * No-op.  We get run through the menu interface.
     */
    public void run()
    {
    }

    /*
     * =======================================================
     * Application specific methods.
     * =======================================================
     */

    /** Provide a default background color for all notes.  */
    static public Color getBG() { return defaultBG; }

    /** Provide a default foreground color for all notes.  */
    static public Color getFG() { return defaultFG; }

    /** 
     * Find a category by its ID.
     * @param id        The ID of the category to search for.
     * @returns The matching CategoryData object.
     * @throws Exception if no matching category is found.
     */
    static public CategoryData findByID(String id) throws Exception
    { 
        CategoryIO cio = new CategoryIO(cid);
        java.util.List<CategoryData> categories = cio.load();
        Iterator it = categories.iterator();
        while ( it.hasNext() )
        {
            CategoryData cd = (CategoryData)it.next();
            if ( cd.id.equals( id ) )
                return cd;
        }
        throw new Exception(Messages.S_NOSUCHCATEGORY);
    }

    /** 
     * Find the named category.
     * @param name      The name of the category to search for.
     * @returns The matching CategoryData object.
     * @throws Exception if no matching category is found.
     */
    static public CategoryData findByName(String name) throws Exception
    { 
        /* Names are case insensitive. */
        CategoryIO cio = new CategoryIO(cid);
        java.util.List<CategoryData> categories = cio.load();
        Iterator it = categories.iterator();
        while( it.hasNext() )
        {
            CategoryData data = (CategoryData)it.next();
            if ( data.name.equalsIgnoreCase( name ) )
                return data;
        }
        throw new Exception(Messages.S_NOSUCHCATEGORY);
    }

    /** 
     * Allow other plugins to create a new category.
     * @param name      The name of the category to search for.
     * @returns The matching CategoryData object if found, null otherwise.
     */
    static public boolean addCategory(CategoryData cd) 
    { 
        if ( categoryUI != null )
            categoryUI.add(cd);
        else
        {
            /* Make sure named entry does not already exist. */
            if ( cd.name.length() == 0 )
            {
                log.error(Messages.S_MISSINGNAME);
                return false;
            }

            /* Names are case insensitive. */
            CategoryIO cio = new CategoryIO(cid);
            java.util.List<CategoryData> categories = cio.load();
            Iterator it = categories.iterator();
            while( it.hasNext() )
            {
                CategoryData data = (CategoryData)it.next();
                if ( data.name.equalsIgnoreCase( cd.name ) )
                {
                    log.error(Messages.S_EXISTS);
                    return false;
                }
            }
            categories.add(cd);
            cio.save(categories);
        }
        return true;
    }

    /**
     *
     * =======================================================
     * Application specific callbacks.
     * =======================================================
     */

    /**
     * Build the UI, if it's not been built previously.
     * Then show the UI.
     */
    private void show()
    {
        class CB implements Callback {
            public void run() {
                categoryUI = null;
            }
        }
        categoryUI = new CategoryUI(cid, shell, new CB());
        categoryUI.open();
    }
}
