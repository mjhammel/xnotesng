package com.ximba.xnotesng.apps.log;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;

/**
 * <p> 
 * Display log data in a graphical window.  Removes need for reviewing logs by editing files.
 * </p>
 * @adm $Revision: 1.2 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class Log extends Plugin implements ApplicationInterface {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.log.Log");

    /* Saved configuration data. */
    static CoreInitData cid = null;

    /* This Plugin ID. */
    private static final String PLUGIN_ID = "com.ximba.xnotesng.apps.log";

    /*
     * GUI variables.
     */
    Shell       shell = null;
    String      home = null;
    String      inMsg = null;;

    /* Save data when the window is not created. */
    static boolean saveLog = true;
    static boolean registered = false;

    static StyledText logText = null;
    static java.util.List<String> logData = new ArrayList<String>();

    private static final int M_FILE_CLOSE = 0;
    private static String[] FileMenuItems = {
        "&Close",
    };

    /* Style information for text in this window. */
    static Font        dateFont;
    static Font        levelFont;
    static Font        msgFont;
    static Color       dateColor;
    static Color       infoColor;
    static Color       errorColor;
    static Color       warnColor;
    static Color       msgColor;

    private static final String S_WELCOME = "Logging trace started.\n";

    /*
     * =======================================================
     * Constructor
     * This plugin does not have a constructor method.
     * =======================================================
     */


    /*
     * =======================================================
     * Callbacks (Inner classes)
     * =======================================================
     */

    /**
     * Anonymous callback class used to call the appropriate method 
     * when the Log menu item is selected.
     */
    class MenuCallback implements Callback {
        Log log;
        public MenuCallback(Log log) {
            this.log = log;
        }
        public void run() {
            log.run();
        }
    }


    /*
     * =======================================================
     * Private methods required by this plugin.
     * =======================================================
     */


    /*
     * =======================================================
     * Callback class used to asynchronously retrieve data.
     * =======================================================
     */
    class LogCB implements LogCallback
    {
        static final int N_INFO  = 1;
        static final int N_ERROR = 2;
        static final int N_WARN  = 3;

        static final int N_INFO_L  = 4;
        static final int N_ERROR_L = 5;
        static final int N_WARN_L  = 4;

        public void append(final String message) {
            if ( saveLog )
            {
                /* Save the data until we've created the window. */
                logData.add(message);
                return;
            }
            else
                process(message);
        }

        public void process(final String message) {
            if ( logText == null ) 
                return;

            Display.getDefault().syncExec(new Runnable() {
                public void run(){
                    synchronized (logText) 
                    {
                        /* Show the text. */
                        if ( (logText == null) || (logText.isDisposed()) )
                            return;
            
                        /* Parse the text and get required offset information for styling. */
                        final String currentLog = logText.getText();
            
                        /* Strip extraneous path from classnames. */
                        String msg = message;
                        if ( message.contains("com.ximba.xnotesng.") )
                            msg = message.replace("com.ximba.xnotesng.", "");
                        final int levelOffset = msg.indexOf("]") + 2;
            
                        /* Find Log level tags. */
                        int length = 0;
                        int type = 0;
                        if ( msg.substring(levelOffset, levelOffset+4).equals("INFO") ) { length = N_INFO_L;  type=N_INFO; }
                        if ( msg.substring(levelOffset, levelOffset+4).equals("ERRO") ) { length = N_ERROR_L; type=N_ERROR; }
                        if ( msg.substring(levelOffset, levelOffset+4).equals("WARN") ) { length = N_WARN_L; type=N_WARN; }
                        final int levelLength = length;
                        final int levelType = type;
                        final int msgLength = msg.substring(levelOffset + length + 1).length();
        
                        /* Add the message to the window. */
                        logText.append(msg);
            
                        /* Style the date stamp. */
                        StyleRange styleDS = new StyleRange();
                        styleDS.start = currentLog.length();
                        styleDS.length = levelOffset;
                        styleDS.font = dateFont;
                        styleDS.foreground = dateColor;
                        logText.setStyleRange(styleDS);
            
                        /* Style the level. */
                        StyleRange styleLevel = new StyleRange();
                        styleLevel.start = currentLog.length() + levelOffset;
                        styleLevel.length = levelLength;
                        styleLevel.font = levelFont;
                        if      ( levelType == N_INFO ) styleLevel.foreground = infoColor;
                        else if ( levelType == N_WARN ) styleLevel.foreground = warnColor;
                        else                            styleLevel.foreground = errorColor;
                        logText.setStyleRange(styleLevel);
            
                        /* Style the message. */
                        StyleRange styleMsg = new StyleRange();
                        styleMsg.start = currentLog.length() + levelOffset + levelLength + 1;
                        styleMsg.length = msgLength;
                        styleMsg.font = msgFont;
                        styleMsg.foreground = msgColor;
                        logText.setStyleRange(styleMsg);
                    }
                }
            });
        }
    }


    /*
     * =======================================================
     * Public Methods that are part of the class and interface.
     * =======================================================
     */

    /**
     * Retrieve this plugins' identifier.
     */
    public String getID() { return PLUGIN_ID; }

    /**
     * Retrieve this plugin's home directory.
     */
    public String getHome() { return home; }

    /**
     * Save incoming messages, for whatever we might want them for.
     */
    public void setMsg(final String msg) { 
        inMsg = msg;
    }

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStart">Plugin.doStart</a>}.
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStop">Plugin.doStop</a>}.
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }


    /**
     * Register the Log plugin.
     */
    public ApplicationRegistration register()
    {
        log.info("Log plugin registration called.");
        ApplicationRegistration cr = new ApplicationRegistration(this);
        try {
            cr.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"menu"), (Callback) new MenuCallback(this));
        }
        catch (Exception e) {
            log.error("Error while registering plugin: reason = " + e.getMessage(), e);
            return null;
        }

        /* 
         * Register with the log appender so we can be notified of incoming log messages.
         * We only have to do this once, no matter how many times we login/logout/login.
         */
        if ( !registered )
        {
            registered = true;
            LogAppender.register(new LogCB());
        }

        return cr;
    }

    /**
     * This method is called once during application life cycle to allow
     * the plugin to initialize itself before starting up the UI.
     */
    public void init(CoreInitData cid) throws Exception
    {
        this.cid = cid;

        /* Find the home directory for the plugin. */
        home = ApplicationManager.getHome(cid, PLUGIN_ID);

        /* Create fonts and colors for this window. */
        dateFont = new Font(Display.getDefault(),"Sans",7,SWT.ITALIC);
        levelFont = new Font(Display.getDefault(),"Sans",8,SWT.BOLD);
        msgFont = new Font(Display.getDefault(),"Verdana",8,SWT.NONE);
        dateColor = new Color(Display.getDefault(), 100, 100, 100);
        infoColor = new Color(Display.getDefault(), 50, 50, 200);
        errorColor = new Color(Display.getDefault(), 200, 50, 50);
        warnColor = new Color(Display.getDefault(), 200, 180, 50);
        msgColor = new Color(Display.getDefault(), 0, 0, 0);

    }

    /**
     * The Log plugin creates a simple text window in which log messages will
     * be formatted for display.  Log data comes from the log appender.
     * Note: This window is only created once, so it can be appended to continuously.
     */
    public void run()
    {
        if ( shell == null )
        {
            shell = new Shell(Display.getDefault());

            /* Add a menu bar */
            Menu menuBar = new Menu(shell, SWT.BAR);
            shell.setMenuBar( menuBar );

            /* Add File Menu */
            MenuItem menuBar_File= new MenuItem (menuBar, SWT.CASCADE);
            menuBar_File.setText ("&File");
            Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
            menuBar_File.setMenu( fileMenu );

            for (int i=0; i<FileMenuItems.length; i++)
            {
                final int idx = i;
                MenuItem item = new MenuItem (fileMenu, SWT.CASCADE);
                item.setText( FileMenuItems[i] );
                item.addListener (SWT.Selection, new Listener () {
                    public void handleEvent (Event e) {
                        fileMenuHandler(idx);
                    }
                });
            }

            /* Set a form layout for the composite. */
            FormLayout fl = new FormLayout();
            fl.marginWidth = 0;
            fl.marginHeight = 0;
            shell.setLayout(fl);
        
            /* A scroll manager for the text window. */
            final ScrolledComposite sc = new ScrolledComposite(shell, SWT.BORDER | SWT.V_SCROLL);
            FormData fd = new FormData();
            fd.top = new FormAttachment(0,0);
            fd.left = new FormAttachment(0,0);
            fd.bottom = new FormAttachment(100,0);
            fd.right = new FormAttachment(100,0);
            fd.width = 520;
            fd.height = 250;
            sc.setLayoutData(fd);

            /* A text window for logs. */
            logText = new StyledText(sc, SWT.WRAP | SWT.READ_ONLY);
            logText.setText(S_WELCOME);
    
            StyleRange style1 = new StyleRange();
            style1.start = 0;
            style1.length = S_WELCOME.length();
            style1.fontStyle = SWT.BOLD;
            style1.foreground = Display.getDefault().getSystemColor(SWT.COLOR_BLUE);
            logText.setStyleRange(style1);

            saveLog = false;
            if ( logData.size() > 0 )
            {
                LogCB cb = new LogCB();
                Iterator it = logData.iterator();
                while ( it.hasNext() )
                    cb.process((String)it.next());
                logData.clear();
            }

            sc.setContent(logText);
            sc.setExpandHorizontal(true);
            sc.setExpandVertical(true);
            sc.setMinWidth(520);
            sc.setMinHeight(250);
            sc.setAlwaysShowScrollBars(true);

            final StyledText lt = logText;
            sc.addControlListener(new ControlAdapter() {
                public void controlResized(ControlEvent e) {
                    Rectangle r = sc.getClientArea();
                    sc.setMinSize(lt.computeSize(r.width, SWT.DEFAULT));
                }
            });
            logText.addPaintListener(new PaintListener() {
                public void paintControl(PaintEvent e) {
                    Rectangle r = sc.getClientArea();
                    sc.setMinSize(lt.computeSize(r.width, SWT.DEFAULT));
                }
            });

            /* Force redraw to get scrollbars updated. */
            logText.redraw();

            /* Display the window. */
            shell.pack();
            shell.open();
        }
        else
            shell.setVisible(true); 
    }

    /**
     * Not used by this plugin.
     */
    public int getState()
    {
        return -1;
    }

    /**
     * Not used by this plugin.
     */
    public String getData()
    {
        return null;
    }

    /**
     * Kill off the running thread that is updating the log window.
     */
    public void shutdown()
    {
        /* Clean up the widgets. */
        if ( logText != null ) 
        {
            synchronized (logText) { 
                saveLog = true;
                logText = null; 
            }
        }
        else
            saveLog = true;
    }

    /**
     * File Menu handlers
     */
    private void fileMenuHandler(int i)
    {
        switch (i) 
        {
            case M_FILE_CLOSE: shell.setVisible(false); break;
        }
    }

}

