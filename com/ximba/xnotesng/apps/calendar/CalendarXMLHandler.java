package com.ximba.xnotesng.apps.calendar;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.SimpleDateFormat;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import org.apache.log4j.Logger;

import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.ArrayList;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Parse the Calendar data file.
 * </p> 
 *
 * @adm $Revision: 1.5 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

class CalendarXMLHandler extends DefaultHandler
{
    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.calendar.CalendarXMLHandler");

    /*
     * Element name field indexes.
     */
    private static int f_Calendar        = 0;
    private static int f_TimerEvent      = 1;
    private static int f_TimerRepeat     = 2;
    private static int f_AlarmData       = 3;
    private static int f_Name            = 4;
    private static int f_AppID           = 5;
    private static int f_PnIndex         = 6;
    private static int f_TimerID         = 7;
    private static int f_StartDate       = 8;
    private static int f_EndDate         = 9;
    private static int f_UserSchedulable = 10;
    private static int f_RepeatType      = 11;
    private static int f_OffsetType      = 12;
    private static int f_OffsetPeriod    = 13;
    private static int f_DayOfWeek       = 14;
    private static int f_Until           = 15;
    private static int f_State           = 16;
    private static int f_Amount          = 17;
    private static int f_Type            = 18;

    /* The set of elements that we recognize. Names are case insensitive. */
    private String[] elementNames = {

        /* These have subelements. */
        "Calendar",
        "TimerEvent",
        "TimerRepeat",
        "AlarmData",

        /* These do not have subelements. */
        "name",
        "appID",
        "pnIndex",
        "timerID",
        "startDate",
        "endDate",
        "userSchedulable",
        "repeatType",
        "offsetType",
        "offsetPeriod",
        "dayOfWeek",
        "until",
        "state",
        "amount",
        "type",
    };

    /* Determines when we're' in a various elements. */
    boolean inCalendar = false;
    boolean inTimerEvent = false;
    boolean inTimerRepeat = false;
    boolean inAlarmData = false;

    /* These keep track of our elements and their values. */
    private String currentElement = null;
    private ArrayList<String> currentAttr = new ArrayList<String>();
    private ArrayList<String> currentAttrValue = new ArrayList<String>();

    java.util.List<TimerEvent> events = null;

    private TimerEvent timerEvent = null;
    private TimerRepeatData timerRepeat = null;
    private AlarmData alarmData = null;

    static private Writer out = null;
    StringBuffer textBuffer = null;

    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a z", Locale.US);
    Date d1 = null;
    boolean ignoreEvent = false;

    /*
     * ----------------------------------------------------------------
     * Inner Classes
     * ----------------------------------------------------------------
     */


    /*
     * ----------------------------------------------------------------
     * Consstructor
     * ----------------------------------------------------------------
     */

    /**
     * Constructor requires a callback method to return the parsed data.
     * @param cb    A WSCallback object.
     */
    public CalendarXMLHandler(List<TimerEvent> val)
    {
        events = val;
    }


    /*
     * ----------------------------------------------------------------
     * SAX interface methods
     * ----------------------------------------------------------------
     */

    /**
     * The start of a document doesn't need to do anything for a UserFile.
     */
    public void startDocument() throws SAXException
    {
    }

    /*
     * When at the end of a document, we pass the parsed data back to
     * the main class and clean up all previous saved settings.
     */
    public void endDocument() throws SAXException
    {
    }

    /*
     * When encountering the start of an element we
     *  1. parse the element name,
     *  2. clear any previous element value we'd accumulated
     *  3. make note of which element has started.
     * We also keep a list of all attributes provided with this element.
     */
    public void startElement(
            String namespaceURI,
            String sName,               // simple name
            String qName,               // qualified name
            Attributes attrs)
            throws SAXException
    {
        /* Reset the textBuffer, which is the value this element will hold. */
        textBuffer = null;

        /* element name */
        String eName = sName;

        /* not namespace-aware */
        if ("".equals(eName))
            eName = qName;

        /* Save the current element name. */
        currentElement = eName;
        currentAttr.clear();
        currentAttrValue.clear();

        if (attrs != null) {
            for (int i = 0; i < attrs.getLength(); i++) {

                /* Attr name */
                String aName = attrs.getLocalName(i);

                if ("".equals(aName))
                    aName = attrs.getQName(i);

                currentAttr.add(aName);
                currentAttrValue.add(attrs.getValue(i));
            }
        }

        cmdStart(currentElement);
    }

    /*
     * When the end of an element is reached we note which
     * element that is, process the element value accumulated so far,
     * and then clear the element value.
     */
    public void endElement(
            String namespaceURI,
            String sName,           // simple name
            String qName            // qualified name
            )
            throws SAXException
    {
        /* Process this command. */
        if ( textBuffer != null )
            processCmd(textBuffer.toString());

        /* element name */
        String eName = sName;

        /* not namespace-aware */
        if ("".equals(eName))
            eName = qName;

        /* Reset state, if necessary. */
        cmdStop(eName);
        textBuffer = null;
    }

    /*
     * This method is what accumulates the set of characters stuffed into
     * the element value.  It is essentially anything between the start
     * and end elements.
     */
    public void characters(char buf[], int offset, int len) throws SAXException
    {
        String s = new String(buf, offset, len);
        if (textBuffer == null) {
            textBuffer = new StringBuffer(s);
        }
        else {
            textBuffer.append(s);
        }
    }

    /* treat validation errors as fatal */
    public void error(SAXParseException e)
    throws SAXParseException
    {
        throw e;
    }

    /* Dump warnings */
    public void warning(SAXParseException err) throws SAXParseException
    {
    }


    /*
     * ----------------------------------------------------------------
     * UserFileXMLHandler specific methods for parsing data.
     * ----------------------------------------------------------------
     */


    /*
     * ----------------------------------------------------------------
     * Process the start of an element.  This usually just parses
     * the attributes for the element.
     * ----------------------------------------------------------------
     */
    private void cmdStart(String element)
    {
        if (element.equalsIgnoreCase(elementNames[f_Calendar]) )
        {
            inCalendar = true;
        }

        if (element.equalsIgnoreCase(elementNames[f_TimerEvent]) )
        {
            if ( !inCalendar )
            {
                log.error("TimerEvent outside of Calendar.");
                return;
            }
            inTimerEvent = true;
            timerEvent = new TimerEvent();
        }

        if (element.equalsIgnoreCase(elementNames[f_TimerRepeat]) )
        {
            if ( !inTimerEvent )
            {
                log.error("TimerRepeat outside of Calendar.");
                return;
            }
            inTimerRepeat = true;
            timerRepeat = new TimerRepeatData();
            timerEvent.repeat = timerRepeat;
        }

        if (element.equalsIgnoreCase(elementNames[f_AlarmData]) )
        {
            if ( !inTimerEvent )
            {
                log.error("AlarmData outside of Calendar.");
                return;
            }
            inAlarmData = true;
            alarmData = new AlarmData();
            timerEvent.alarm = alarmData;
        }

    }

    /*
     * ----------------------------------------------------------------
     * Process the values that elements encompass.  It should only be valid
     * for elements that do not have subelements.
     * ----------------------------------------------------------------
     */
    private void processCmd(String value)
    {
        /*
         * We'll get called when an element closes, a newline/tab is encountered and then
         * another element closes.  In that case, we just ignore the request.
         */
        Matcher matcher = Pattern.compile("\\n*\\t*\\s*").matcher(value);
        String currentValue = matcher.replaceAll("");
        if ( currentValue.length() == 0 )
            return;
        currentValue = value;

        /*
         * Field: Name
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_Name]) )
        {
            if ( (inTimerEvent) && (timerEvent != null) )
            {
                if ( currentValue != null )
                    timerEvent.name = currentValue;
            }
            else
                log.error("Received event name outside of TimerEvent element. Ignoring.");
        }

        /*
         * Field: appID
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_AppID]) )
        {
            if ( (inTimerEvent) && (timerEvent != null) )
            {
                if ( currentValue != null )
                    timerEvent.appID = currentValue;
            }
            else
                log.error("Received appID outside of TimerEvent element. Ignoring.");
        }

        /*
         * Field: pnIndex
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_PnIndex]) )
        {
            if ( (inTimerEvent) && (timerEvent != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.pn_index= Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to pn_index. Ignoring.");
                    }
                }
            }
            else
                log.error("Received pnInex outside of TimerEvent element. Ignoring.");
        }

        /*
         * Field: Start
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_StartDate]) )
        {
            if ( (inTimerEvent) && (timerEvent != null) )
            {
                log.error("Start date: " + currentValue);
                if ( currentValue != null )
                {
                    timerEvent.start = Calendar.getInstance();
                    try { 
                        Date d1=sdf.parse(currentValue); 
                        log.error("Start date(): " + d1.toString());
                        timerEvent.start.setTime(d1);
                    }
                    catch (Exception e) { 
                        log.error("Failed to convert start date. Ignoring: " + e.getMessage());
                        ignoreEvent = true;
                    }
                }
            }
            else
                log.error("Received start date outside of TimerEvent element. Ignoring.");
        }

        /*
         * Field: EndDate
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_EndDate]) )
        {
            if ( (inTimerEvent) && (timerEvent != null) )
            {
                log.error("End date: " + currentValue);
                if ( currentValue != null )
                {
                    timerEvent.end = Calendar.getInstance();
                    try { 
                        Date d1=sdf.parse(currentValue); 
                        timerEvent.end.setTime(d1);
                    }
                    catch (Exception e) { 
                        log.error("Failed to convert end date. Ignoring: " + e.getMessage());
                        ignoreEvent = true;
                    }
                }
            }
            else
                log.error("Received end date outside of TimerEvent element. Ignoring.");
        }

        /*
         * Field: User Schedulable
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_UserSchedulable]) )
        {
            if ( (inTimerEvent) && (timerEvent != null) )
            {
                if ( currentValue != null )
                {
                    if ( currentValue.equalsIgnoreCase("True") )
                        timerEvent.userSchedulable = true;
                    else 
                        timerEvent.userSchedulable = false;
                }
            }
            else
                log.error("Received user schedulable outside of TimerEvent element. Ignoring.");
        }

        /*
         * Field: RepeatType
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_RepeatType]) )
        {
            if ( (inTimerRepeat) && (timerEvent != null) && (timerEvent.repeat != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.repeat.repeatType = Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to load repeat type. Ignoring.");
                    }
                }
            }
            else
                log.error("Received repeat type outside of TimerRepeat element. Ignoring.");
        }

        /*
         * Field: OffsetType
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_OffsetType]) )
        {
            if ( (inTimerRepeat) && (timerEvent != null) && (timerEvent.repeat != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.repeat.offsetType = Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to load offset type. Ignoring.");
                    }
                }
            }
            else
                log.error("Received offset type outside of TimerRepeat element. Ignoring.");
        }

        /*
         * Field: OffsetPeriod
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_OffsetPeriod]) )
        {
            if ( (inTimerRepeat) && (timerEvent != null) && (timerEvent.repeat != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.repeat.offsetPeriod = Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to load period type. Ignoring.");
                    }
                }
            }
            else
                log.error("Received offset period outside of TimerRepeat element. Ignoring.");
        }

        /*
         * Field: DayOfWeek
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_DayOfWeek]) )
        {
            if ( (inTimerRepeat) && (timerEvent != null) && (timerEvent.repeat != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.repeat.dayOfWeek = Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to load ay of week. Ignoring.");
                    }
                }
            }
            else
                log.error("Received day of week outside of TimerRepeat element. Ignoring.");
        }

        /*
         * Field: Until
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_Until]) )
        {
            if ( (inTimerRepeat) && (timerEvent != null) && (timerEvent.repeat != null) )
            {
                log.error("Until date: " + currentValue);
                if ( currentValue != null )
                {
                    timerEvent.repeat.until = Calendar.getInstance();
                    try { 
                        Date d1=sdf.parse(currentValue); 
                        timerEvent.repeat.until.setTime(d1);
                    }
                    catch (Exception e) { 
                        log.error("Failed to convert until date. Ignoring: " + e.getMessage());
                        ignoreEvent = true;
                    }
                }
            }
            else
                log.error("Received start date outside of TimerRepeat element. Ignoring.");
        }

        /*
         * Field: State
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_State]) )
        {
            if ( (inAlarmData) && (timerEvent != null) && (timerEvent.alarm != null) )
            {
                if ( currentValue != null )
                {
                    if ( currentValue.equalsIgnoreCase("True") )
                        timerEvent.alarm.state = true;
                    else 
                        timerEvent.alarm.state = false;
                }
            }
            else
                log.error("Received alarm state outside of AlarmData element. Ignoring.");
        }

        /*
         * Field: Amount
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_Amount]) )
        {
            if ( (inAlarmData) && (timerEvent != null) && (timerEvent.alarm != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.alarm.amount = Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to load alarm amount type. Ignoring.");
                    }
                }
            }
            else
                log.error("Received alarm amount outside of AlarmData element. Ignoring.");
        }

        /*
         * Field: Type
         */
        if ( currentElement.equalsIgnoreCase(elementNames[f_Type]) )
        {
            if ( (inAlarmData) && (timerEvent != null) && (timerEvent.alarm != null) )
            {
                if ( currentValue != null )
                {
                    try {timerEvent.alarm.type = Integer.parseInt(currentValue);}
                    catch (Exception e) { 
                        log.error("Failed to load alarm type type. Ignoring.");
                    }
                }
            }
            else
                log.error("Received alarm type outside of AlarmData element. Ignoring.");
        }

    }

    /*
     * ----------------------------------------------------------------
     * When an element ends, we might need to reset our state.
     * May not need this either.
     * ----------------------------------------------------------------
     */
    private void cmdStop(String element)
    {
        if (element.equalsIgnoreCase(elementNames[f_Calendar]) )
        {
            inAlarmData = false;
            inTimerRepeat = false;
            inTimerEvent = false;
            inCalendar = false;

            alarmData = null;
            timerRepeat = null;
            timerEvent = null;
        }
        if (element.equalsIgnoreCase(elementNames[f_TimerEvent]) )
        {
            inAlarmData = false;
            inTimerRepeat = false;
            inTimerEvent = false;
            if ( ignoreEvent ) 
            {
                ignoreEvent = false;
                return;
            }

            /* Save the TimerEvent. */
            events.add( timerEvent );
            alarmData = null;
            timerRepeat = null;
            timerEvent = null;
        }
        if (element.equalsIgnoreCase(elementNames[f_TimerRepeat]) )
        {
            inTimerRepeat = false;
        }
        if (element.equalsIgnoreCase(elementNames[f_AlarmData]) )
        {
            inAlarmData = false;
        }
    }

}


