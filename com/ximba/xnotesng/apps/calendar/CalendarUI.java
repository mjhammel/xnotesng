package com.ximba.xnotesng.apps.calendar;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.printing.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.category.*;
import com.ximba.xnotesng.apps.xnotes.*;


/**
 * <p> 
 * The Calendar Manager window.
 * </p>
 *
 * @adm $Revision: 1.17 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class CalendarUI {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.calendar.CalendarUI");

    private CoreInitData cid = null;
    Calendar parent = null;
    Label[][][] dayLabel = new Label[6][7][4];
    DateTime calendar = null;
    NewEventDialog ned = null;
    Table entryTable = null;
    Composite monthComposite = null;
    TabFolder tabFolder = null;
    TabItem dayTabItem = null;
    TabItem monthTabItem = null;

    Map<String, Image> subImages = new HashMap<String, Image>();
    Image noteIcon = null;
    Image calendarIcon = null;

    Shell shell = null;

    Color selectedBG = new Color(Display.getDefault(), 255, 255, 255);
    Color morningBG = new Color(Display.getDefault(), 255, 252, 163);
    Color middayBG = new Color(Display.getDefault(), 0, 82, 163);
    Color nightBG = new Color(Display.getDefault(), 23, 15, 68);
    Color defaultBG = null;

    public static final String S_TITLE          = "Calendar Manager";
    public static final String S_START          = "Start Time";
    public static final String S_END            = "End Time";
    public static final String S_TITLE_ENTRY    = "Event";
    public static final String S_GLANCE_DAY     = "Day at a glance";
    public static final String S_GLANCE_MONTH   = "Month at a glance";

    /* Menus */
    private static final int M_FILE_NEW       = 0;
    private static final int M_FILE_SAVE      = 1;
    private static final int M_FILE_DELETE    = 2;
    private static final int M_FILE_CLOSE     = 3;
    private static String[] FileMenuItems = {
        "&New Event",
        "&Save",
        "&Delete",
        "&Close",
    };

    /* Entry Table columns */
    private static final int F_START        = 0;
    private static final int F_END          = 1;
    private static final int F_TITLE        = 2;
    public static final String[] entryHdrs  = { S_START, S_END, S_TITLE_ENTRY };
    public static final int[] entryWidths   = { 120, 120, 260};

    public static final String[] dayNames   = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

    java.util.List<CategoryData> categories = new ArrayList<CategoryData>();
    java.util.List<TimerEvent> appts = new ArrayList<TimerEvent>();

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Default constructor creates the actual note.  Class methods are used to manage it.
     * @param cid       A reference to the global CoreInitData object.
     */
    public CalendarUI (Calendar parent, CoreInitData cid) { 

        this.parent = parent;
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */


    /*
     * =======================================================
     * Public Classes
     * =======================================================
     */

    /**
     * Display the initial data.
     */
    public void open() {
        createContent();
        filllist();
        shell.open();
    }

    /**
     * Close the note.  This gets rid of the window and saves the contents.
     * Typically called when the application is exiting.
     */
    public void close() {
        if ( ned != null )
            ned.closeDialog();
        shell.dispose();
        parent.close();
        CalendarIO cio = new CalendarIO(cid);
        try { cio.save(); }
        catch (Exception e) { e.printStackTrace(); }
    }

    /*
     * =======================================================
     * Private Methods
     * =======================================================
     */

    private void createContent()
    {
        Label label = null;
        GridData gd = null;
        GridLayout gl = null;

        /* Create a modeless shell. */
        shell = new Shell(Display.getDefault(), SWT.SHELL_TRIM | SWT.MODELESS);
        shell.setText(S_TITLE);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                close();
            }
        });

        /* Add a menu bar */
        Menu menuBar = new Menu(shell, SWT.BAR);
        shell.setMenuBar( menuBar );

        /* Add File Menu */
        MenuItem menuBar_File= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_File.setText ("&File");
        Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_File.setMenu( fileMenu );

        for (int i=0; i<FileMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (fileMenu, SWT.CASCADE);
            item.setText( FileMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    fileMenuHandler(idx);
                }
            });
        }

        /* Grid Composite with a single column. */
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        shell.setLayout(gl);


        /* Tabbed Folder to hold two types of views. */
        tabFolder = new TabFolder(shell, SWT.NONE);

        /* A DatTime widget below the tabbed folder. */
        calendar = new DateTime(shell, SWT.CALENDAR | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL); 
        calendar.setLayoutData(gd);

        /* Run updates when the user changes the date. */
        calendar.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                filllist();
            }
        });


        /* 
         * -----------------------------------------------------
         * First view: Daily - list and DateTime widget.
         * -----------------------------------------------------
         */

        dayTabItem = new TabItem(tabFolder, SWT.NONE);
        dayTabItem.setText(S_GLANCE_DAY);

        /* Grid Composite with a single column. */
        Composite dayComposite = new Composite(tabFolder, SWT.NONE);
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        dayComposite.setLayout(gl);

        dayTabItem.setControl(dayComposite);

        /* A scrolled table of events for the current day. */
        entryTable = new Table(dayComposite, SWT.VIRTUAL | SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 240;
        entryTable.setLayoutData(gd);
        entryTable.setHeaderVisible(true);
        entryTable.setLinesVisible(true);

        entryTable.setRedraw(false);
        for (int i=0; i<entryHdrs.length; i++)
        {
            TableColumn col = new TableColumn(entryTable, SWT.LEFT);
            col.setText(entryHdrs[i]);
            col.setWidth(entryWidths[i]);
        }
        entryTable.setRedraw(true);

        entryTable.addSelectionListener(new SelectionAdapter() {
            public void widgetDefaultSelected(SelectionEvent e) {
                Table table = (Table) e.widget;
                TableItem[] rows = table.getSelection();
                if ( rows.length == 0 )
                    return;
                TimerEvent te = (TimerEvent) rows[0].getData();
                newEvent( te );
            }
        });


        /* 
         * -----------------------------------------------------
         * Second View: Monthly view
         * -----------------------------------------------------
         */

        /* Composite with 7 columns, 2 rows */
        monthTabItem = new TabItem(tabFolder, SWT.NONE);
        monthTabItem.setText(S_GLANCE_MONTH);

        /* Grid Composite with a single column. */
        final Composite mComposite = new Composite(tabFolder, SWT.NONE);
        monthComposite = mComposite;
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 7;
        gl.makeColumnsEqualWidth = true;
        mComposite.setLayout(gl);

        monthTabItem.setControl(mComposite);

        /* row 1: day names */
        for (int i=0; i<dayNames.length; i++)
        {
            label = new Label(mComposite, SWT.CENTER);
            label.setText(dayNames[i]);
            gd = new GridData(GridData.FILL_HORIZONTAL | GridData.HORIZONTAL_ALIGN_CENTER);
            label.setLayoutData(gd);
        }

        /* row 2: 6x7 grid of composites, 1 column each, 4 rows. */
        for (int i=0; i<6; i++)
        {
            for (int j=0; j<7; j++)
            {
                /*
                 * Current day is colored yellow, all other uncolored
                 */
                Composite comp = new Composite(mComposite, SWT.BORDER);
                gl = new GridLayout();
                gl.marginWidth = 0;
                gl.marginHeight = 0;
                gl.verticalSpacing = 0;
                gl.horizontalSpacing = 0;
                gl.numColumns = 1;
                gl.makeColumnsEqualWidth = true;
                comp.setLayout(gl);

                /* 
                 * Each Composite has 4 rows, no lines visible
                 * Row 1: day of month
                 * Row 2: morning icon
                 * Row 3: afternoon icon
                 * Row 4: evening icon
                 */
                for (int k=0; k<4; k++)
                {
                    dayLabel[i][j][k] = new Label(comp, SWT.NONE);
                    gd = new GridData(GridData.FILL_HORIZONTAL);
                    gd.heightHint = 13;
                    dayLabel[i][j][k].setLayoutData(gd);
                    if ( defaultBG == null )
                        defaultBG = dayLabel[i][j][k].getBackground();

                    final int row = i;
                    final int col = j;
                    dayLabel[i][j][k].addMouseListener(new MouseAdapter() {
                        public void mouseDoubleClick(MouseEvent e) {
                            monthCB(row,col);
                        }
                    });
                }
            }
        }

        shell.pack();
    }

    /**
     * 
     */
    private void monthCB( int i, int j )
    {
        /* Set calendar. */
        try {
            int day = new Integer( dayLabel[i][j][0].getText() );
            calendar.setDay( day );
            tabFolder.setSelection(dayTabItem);
            TimerManager tm = cid.getTimerManager();
            java.util.List<TimerEvent> events = tm.getEvents(Calendar.appID);
            updateDay(events);
        }
        catch(Exception e) {
            log.error(e.getMessage());
        }

    }

    /**
     * 
     */
    private boolean showIt( java.util.Calendar current, TimerEvent event)
    {
        boolean showIt = false;

        java.util.Calendar eventCal = (java.util.Calendar) event.start.clone();
        int eventYear      = eventCal.get(java.util.Calendar.YEAR);
        int eventMonth     = eventCal.get(java.util.Calendar.MONTH);
        int eventDay       = eventCal.get(java.util.Calendar.DAY_OF_MONTH);
        int eventYearday   = eventCal.get(java.util.Calendar.DAY_OF_YEAR);

        int currentYear    = current.get(java.util.Calendar.YEAR);
        int currentMonth   = current.get(java.util.Calendar.MONTH);
        int currentDay     = current.get(java.util.Calendar.DAY_OF_MONTH);
        int currentWeekday = current.get(java.util.Calendar.DAY_OF_WEEK);
        int currentYearday = current.get(java.util.Calendar.DAY_OF_YEAR);

        /* If no repeat, then the day/month have to match. */
        if ( event.repeat == null )
        {
            if ( 
                (eventYear == currentYear) && 
                (eventMonth == currentMonth) && 
                (eventDay == currentDay) 
               )
                showIt = true;
        }
        else
        {
            boolean checkOK = false;
            java.util.Calendar untilCal = null;
            int untilYear = -1;
            int untilYearday = -1;

            if ( event.repeat.until != null )
            {
                untilCal = (java.util.Calendar) event.repeat.until.clone();
                untilYear = untilCal.get(java.util.Calendar.YEAR);
                untilYearday = untilCal.get(java.util.Calendar.DAY_OF_YEAR);
            }

            /* Initial check - all other checks depend on it. */
            if ( ((eventYear<currentYear) || (eventYear==currentYear) && (eventYearday<=currentYearday)) &&
                 ((untilCal==null) || ((untilYear==currentYear) && (untilYearday>=currentYearday)) ||
                  (untilYear > currentYear))
               )
               checkOK = true;

            /* If daily, then show it. */
            if ( checkOK &&
                 (event.repeat.repeatType == TimerRepeatData.OFFSET) &&
                 (event.repeat.offsetType == java.util.Calendar.DAY_OF_MONTH) )
            {
                showIt = true;
            }

            /* If weekly and either event=today or weekday name matches, then show it. */
            if ( checkOK && (event.repeat.repeatType == TimerRepeatData.DAYOFWEEK) ) 
            {
                if ( ((eventMonth == currentMonth) && (eventDay == currentDay)) || 
                     (event.repeat.dayOfWeek == currentWeekday) )
                    showIt = true;
            }

            /* If monthly and event day == today, then show it. */
            if ( checkOK &&
                 (event.repeat.repeatType == TimerRepeatData.OFFSET) &&
                 (event.repeat.offsetType == java.util.Calendar.MONTH) )
            {
                if (eventDay == currentDay)
                    showIt = true;
            }

            /* If yearly and event month/day match today, then show it. */
            if ( checkOK &&
                 (event.repeat.repeatType == TimerRepeatData.OFFSET) &&
                 (event.repeat.offsetType == java.util.Calendar.YEAR) )
            {
                if ( (eventMonth==currentMonth) && (eventDay==currentDay) )
                    showIt = true;
            }
        }

        return showIt;
    }

    /**
     * Update the Day at a Glance list. 
     */
    private void updateDay( java.util.List<TimerEvent> events )
    {
        /* Ignore empty lists. */
        if ( events == null )
            return;

        /* What month and day is being displayed? */
        java.util.Calendar displayCal = DateDialog.getCalendar( DateDialog.getString(calendar) );
        int currentYear = displayCal.get(java.util.Calendar.YEAR);
        int currentMonth = displayCal.get(java.util.Calendar.MONTH);
        int currentDay = displayCal.get(java.util.Calendar.DAY_OF_MONTH);
        int currentWeekday = displayCal.get(java.util.Calendar.DAY_OF_WEEK);
        int currentYearday = displayCal.get(java.util.Calendar.DAY_OF_YEAR);

        entryTable.setRedraw( false );
        entryTable.removeAll();

        Iterator it = events.iterator();
        while (it.hasNext())
        {
            TimerEvent event = (TimerEvent)it.next();

            if ( showIt(displayCal, event) )
            {
                TableItem row = new TableItem(entryTable, SWT.NONE);
                row.setData( event );

                java.util.Calendar cal = (java.util.Calendar) event.start.clone();
                cal.add(java.util.Calendar.MONTH, 1);
                row.setText(0, DateDialog.getString(cal));
                cal = (java.util.Calendar) event.end.clone();
                cal.add(java.util.Calendar.MONTH, 1);
                row.setText(1, DateDialog.getString(cal));
                row.setText(2, event.name);
            }
        }

        entryTable.setRedraw( true );
    }

    /**
     * Update the Month at a Glance list. 
     */
    private void setMonthDayColor()
    {
        boolean doIt = true;

        /* Clear all day colors. */

        /* Get todays date. */
        java.util.Calendar currentCal = java.util.Calendar.getInstance();
        int today = currentCal.get(java.util.Calendar.DAY_OF_MONTH);

        /* If not the same month, nothing to do. */
        java.util.Calendar displayCal = DateDialog.getCalendar( DateDialog.getString(calendar) );
        if ( displayCal.get(java.util.Calendar.MONTH) != currentCal.get(java.util.Calendar.MONTH) )
            return;

        /* Find the correct composite/label. */
        for (int i=0; i<6; i++)
        {
            for (int j=0; j<7; j++)
            {
                /* If day matches, set the date label background to white. */
                if ( dayLabel[i][j][0].getText().equals(""+today) && doIt )
                {
                    for (int k=0; k<4; k++)
                        dayLabel[i][j][k].setBackground(selectedBG);
                }
            }
        }
    }

    /**
     * Update the Month at a Glance list. 
     */
    private void updateMonth( java.util.List<TimerEvent> events )
    {
        /* Ignore empty lists. */
        if ( events == null )
            return;

        /* What month is being displayed? */
        java.util.Calendar displayCal = DateDialog.getCalendar( DateDialog.getString(calendar) );
        displayCal.set(java.util.Calendar.DAY_OF_MONTH, 1);

        /* Clear out current month settings. */
        for (int i=0; i<6; i++)
        {
            for (int j=0; j<7; j++)
            {
                for (int k=0; k<4; k++)
                    dayLabel[i][j][k].setBackground(defaultBG);
            }
        }

        /* Set the background color for todays date, if necessary. */
        setMonthDayColor();

        monthComposite.setRedraw( false );
        Iterator it = events.iterator();
        while (it.hasNext())
        {
            TimerEvent event = (TimerEvent)it.next();
            for (int i=0; i<6; i++)
            {
                for (int j=0; j<7; j++)
                {
                    if ( "".equals(dayLabel[i][j][0].getText()) )
                        continue;

                    /*
                     * If the event should be displayed, 
                     * choose morning/day/night label and color code it.
                     */
                    if ( showIt(displayCal, event) )
                    {
                        java.util.Calendar startCal = (java.util.Calendar) event.start.clone();
                        int eventTime = startCal.get(java.util.Calendar.HOUR_OF_DAY);
                        if ( eventTime < 12 )
                            dayLabel[i][j][1].setBackground(morningBG);
                        else if ( eventTime > 18 )
                            dayLabel[i][j][3].setBackground(nightBG);
                        else 
                            dayLabel[i][j][2].setBackground(middayBG);
                    }

                    /* Increment the calendar day. */
                    displayCal.add(java.util.Calendar.DAY_OF_MONTH, 1);
                }
            }
        }
        monthComposite.setRedraw( true );
    }

    /*
     * =======================================================
     * File Menu Handlers
     * =======================================================
     */

    /**
     * Populates the Day list and the Month labels
     */
    private void filllist()
    {
        /* Populate the month labels. */
        populateMonth();
        TimerManager tm = cid.getTimerManager();
        java.util.List<TimerEvent> events = tm.getEvents(Calendar.appID);
        updateMonth( events );
        updateDay( events );
    }

    /**
     * Populates the Month labels
     */
    private void populateMonth()
    {
        /* Draw the numeric days of month. */
        java.util.Calendar cal = DateDialog.getCalendar( DateDialog.getString(calendar) );
        int lastDayOfMonth = cal.getActualMaximum(java.util.Calendar.DAY_OF_MONTH);
        int dayOfMonth = cal.get(java.util.Calendar.DAY_OF_MONTH);
        cal.add(java.util.Calendar.DAY_OF_MONTH, -1*(dayOfMonth-1));
        int dayOfWeek = cal.get(java.util.Calendar.DAY_OF_WEEK) - 1;

        int idx = 1;
        boolean started = false;
        for (int i=0; i<6; i++)
        {
            for (int j=0; j<7; j++)
            {
                /* Skip to the first of the month. */
                if (!started && (j < dayOfWeek) )
                {
                    dayLabel[i][j][0].setText("");
                    continue;
                }
                started = true;

                if ( idx <= lastDayOfMonth )
                    dayLabel[i][j][0].setText(""+idx++);
                else
                    dayLabel[i][j][0].setText("");
            }
        }
    }

    /**
     * File Menu handler
     */
    private void fileMenuHandler(int i)
    {
        switch (i) 
        {
            case M_FILE_NEW     : newEvent(null); break;
            case M_FILE_SAVE    : save(); break;
            case M_FILE_DELETE  : delete(); break;
            case M_FILE_CLOSE   : close(); break;
        }
    }

    /**
     * Opens dialog to create a new event definition, then schedules it.
     */
    private void newEvent(TimerEvent te) { 
        if ( te == null )
            ned = new NewEventDialog(shell);
        else
            ned = new NewEventDialog(shell, te);
        TimerEvent event = ned.open();
        ned = null;
        if ( event == null )
            return;

        TimerManager tm = cid.getTimerManager();

        /* If we're editing an event, delete the old one first. */
        if ( te != null )
        {
            try { tm.cancel( te.getTimerID() ); }
            catch (Exception e) { log.error(e); }
        }

        /* Schedule the event. */
        event.callback = new CalendarHandler(shell);
        try { tm.register(event); }
        catch (Exception e) {
            log.error( e.getMessage(), e );
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage( e.getMessage() );
            md.open();
            return;
        }

        /* Update the display of events. */
        java.util.List<TimerEvent> events = tm.getEvents(Calendar.appID);
        updateDay(events);
        updateMonth(events);

        CalendarIO cio = new CalendarIO(cid);
        try { cio.save(); }
        catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * Save the appts list.
     */
    private void save() { 
        CalendarIO cio = new CalendarIO(cid);
        try { cio.save(); }
        catch (Exception e) { e.printStackTrace(); }
    }

    /**
     * Delete the current entry
     */
    private void delete( ) { 
        TableItem[] rows = entryTable.getSelection();
        if ( rows.length == 0 )
            return;
        TimerEvent te = (TimerEvent) rows[0].getData();

        TimerManager tm = cid.getTimerManager();
        if ( te != null )
        {
            try { tm.cancel( te.getTimerID() ); }
            catch (Exception e) { log.error(e); }
        }
        entryTable.remove( entryTable.indexOf( rows[0] ) );
    }
}
