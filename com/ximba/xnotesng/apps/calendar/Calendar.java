package com.ximba.xnotesng.apps.calendar;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Plugin class for the Calendar application of XNotesNG
 * </p>
 *
 * @adm $Revision: 1.3 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class Calendar extends Plugin implements ApplicationInterface {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.calendar.Calendar");
    private static CalendarUI calendarUI = null;

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;
    private static final Color defaultBG = new Color(Display.getDefault(), 242, 234, 0);

    /* Unique ID for this application. */
    protected static final String appID = "E66B398F-87D5-0916-6BA1-F352BA6D40F6";

    /** The data from the data file. */
    java.util.List<TimerEvent> appts = new ArrayList<TimerEvent>();

    /* This Plugin ID. */
    private static final String PLUGIN_ID = "com.ximba.xnotesng.apps.calendar";

    /** The ApplicationRegistration for this plugin. */
    ApplicationRegistration appReg = null;

    /*
     * =======================================================
     * Constructor
     * This plugin does not have a constructor method.
     * =======================================================
     */

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /**
     * Called when the menu entry for Calendar Manager is selected.
     */
    class ShowCalendar implements Callback {
        Calendar parent;
        public ShowCalendar(Calendar parent) {
            this.parent = parent;
        }
        public void run() {
            parent.run();
        }
    }

    /**
     * Autosave timer task will save any modified notes on the scheduled period.
     */
    class AutoSave extends TimerTask
    {
        Calendar calendar = null;
        public AutoSave(Calendar calendar) {
            this.calendar = calendar;
        }
        public void run()
        {
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    calendar.save();
                }
            });

        }
    }


    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /**
     * Load the calendar event data.
     */
    private void load()
    {
        Shell shell = new Shell(Display.getDefault());
        java.util.List<TimerEvent> events = null;
        CalendarIO cio = new CalendarIO(cid);
        try { events = cio.load(); }
        catch (Exception e) { log.error( e.getMessage(), e ); }
        if ( events == null )
            return;
        TimerManager tm = cid.getTimerManager();
        Iterator it = events.iterator();
        while(it.hasNext())
        { 
            TimerEvent te = (TimerEvent)it.next();
            te.callback = new CalendarHandler(shell);
            try { tm.register(te); }
            catch (Exception e) { log.error( e.getMessage(), e ); }
        }
    }

    /*
     * =======================================================
     * JPF methods
     * =======================================================
     */

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStart">Plugin.doStart</a>}.
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStop">Plugin.doStop</a>}.
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }


    /*
     * =======================================================
     * ApplicationManagerInterface methods
     * =======================================================
     */

    /**
     * Close any note-related windows.
     */
    public void shutdown()
    {
        save();
        if ( calendarUI != null )
            calendarUI.close();
    }


    /**
     * This method is called once during application life cycle to allow
     * the plugin to initialize itself.
     * @param cid   A reference to the global CoreInitData object.
     */
    public void init(CoreInitData cid) throws Exception
    {
        this.cid = cid;

        /* Find the home directory for the plugin. */
        home = ApplicationManager.getHome(cid, PLUGIN_ID);
    }

    /**
     * Register the plugin.
     * @return An ApplicationRegistration object filled with menu registrations.
     */
    public ApplicationRegistration register()
    {
        log.info("Application registration: Calendar Manager.");
        ApplicationRegistration ar = new ApplicationRegistration(this);
        try {
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"calendar"), (Callback) new ShowCalendar(this));
        }
        catch (Exception e) {
            log.error("Error while registering plugin: reason = " + e.getMessage(), e);
            return null;
        }

        /* Load existing calendar events. */
        load();

        /* Save the events back - this clears out expired events. */
        CalendarIO cio = new CalendarIO(cid);
        try { cio.save(); }
        catch (Exception e) { e.printStackTrace(); }

        return ar;
    }

    /**
     * Display the Calendar Manager.
     */
    public void run()
    {
        if ( calendarUI != null )
            return;
        calendarUI = new CalendarUI(this, cid);
        calendarUI.open();
    }

    /** 
     * Called by child class to let us know they are exiting.
     */
    protected void close()
    {
        calendarUI = null;
    }

    /*
     * =======================================================
     * Application specific methods.
     * =======================================================
     */

    /** Provide a default color.  */
    static public Color getBG() { return defaultBG; }

    /** Retrieve the images configured for this plugin.  */
    // public static Map<String, Image> getImages() { return subImages; }

    /**
     *
     * =======================================================
     * Application specific callbacks.
     * =======================================================
     */

    /**
     * Save the appointment list.
     */
    private void save()
    {
    }
}
