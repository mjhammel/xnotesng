package com.ximba.xnotesng.apps.calendar;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.printing.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.category.*;
import com.ximba.xnotesng.apps.xnotes.*;


/**
 * <p> 
 * Class that handles an event activation by displaying an alarm message.
 * </p>
 *
 * @adm $Revision: 1.2 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class CalendarHandler implements TimerCallback
{
    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.calendar.CalendarUI");

    Shell shell = null;
    public CalendarHandler(Shell val) { shell = val; }
    public void run(final TimerEvent te)
    {
        if ( te.alarm != null ) 
        {
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    MessageDialog md = new MessageDialog(shell);
                    md.setMode(MessageDialog.M_ALARM);
                    md.setMessage( te.name );
                    md.open();
                }
            });
        }
    }
    public void cancel(TimerEvent te){}
}

