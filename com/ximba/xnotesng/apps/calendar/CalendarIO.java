package com.ximba.xnotesng.apps.calendar;

import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

// SAX classes.
import org.xml.sax.*;
import org.xml.sax.helpers.*;

//JAXP 1.1
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.sax.*;
import javax.xml.datatype.DatatypeFactory;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* Calendar Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Class for loading events into Calendar application.
 * </p>
 *
 * @adm $Revision: 1.8 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class CalendarIO {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.calendar.CalendarIO");

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;

    /* If the filename is not set, create it. */
    String filename = CacheMgr.getDataDir().getPath() + File.separator + "calendar";

    /* Events retrieved from the Timer Manager. */
    java.util.List<TimerEvent> events = new ArrayList<TimerEvent>();

    static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm a z", Locale.US);

    /*
     * Legacy XNotesPlus file format.  
     */


    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires reference to global CoreInitData object.
     */
    public CalendarIO(CoreInitData cid) 
    {
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */


    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /**
     * Start a new XML document.
     * @param writer - A StringWriter object.
     * @return A TransformerHelper object that can be used for writing XML.
     */
    private TransformerHandler startDoc(StringWriter writer) throws Exception
    {
        AttributesImpl attrs = new AttributesImpl();
        StreamResult streamResult = new StreamResult(writer);
        SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
        TransformerHandler hd = tf.newTransformerHandler();
        Transformer serializer = hd.getTransformer();
        serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
        serializer.setOutputProperty(OutputKeys.INDENT,"yes");
        serializer.setOutputProperty("{http://xml.apache.org/xslt}" + "indent-amount", "4");
        serializer.setOutputProperty(OutputKeys.METHOD, "xml");
        serializer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        hd.setResult(streamResult);
        hd.startDocument();

        attrs.addAttribute("","","xmlns","CDATA","http://www.xnotes.org");
        hd.startElement("","","Calendar",attrs);
        return hd;
    }

    /**
     * Convert events to an XML document.
     * @return A String object containing the XML for the calendar events.
     */
    private String eventsToXml() throws Exception
    {
        if ( events == null )
            return null;

        /* attrs must be a zero size list if the current element has no attributes. */
        AttributesImpl attrs = new AttributesImpl();
        StringWriter sw = new StringWriter();

        /* Convert the Workflow into an XML string and add to the list. */
        TransformerHandler hd = startDoc(sw);

        Iterator it = events.iterator();
        while ( it.hasNext() )
        {
            TimerEvent te = (TimerEvent)it.next();
            String val = null;

            hd.startElement("","","TimerEvent",attrs);

            if ( te.name != null )
            {
                hd.startElement("","","name",attrs);
                hd.characters(te.name.toCharArray(),0,te.name.length());
                hd.endElement("","","name");
            }
            if ( te.appID != null )
            {
                hd.startElement("","","appID",attrs);
                hd.characters(te.appID.toCharArray(),0,te.appID.length());
                hd.endElement("","","appID");
            }
            if ( te.getTimerID() != null )
            {
                hd.startElement("","","timerID",attrs);
                hd.characters(te.getTimerID().toCharArray(),0,te.getTimerID().length());
                hd.endElement("","","timerID");
            }

            val = "" + te.pn_index;
            hd.startElement("","","pnIndex",attrs);
            hd.characters(val.toCharArray(),0,val.length());
            hd.endElement("","","pnIndex");

            /* Start Date. */
            if ( te.start != null )
            {
                String startDate = sdf.format( te.start.getTimeInMillis() );
                hd.startElement("","","startDate",attrs);
                hd.characters(startDate.toCharArray(),0,startDate.length());
                hd.endElement("","","startDate");
            }
            /* End Date. */
            if ( te.end != null )
            {
                String endDate = sdf.format( te.end.getTimeInMillis() );
                hd.startElement("","","endDate",attrs);
                hd.characters(endDate.toCharArray(),0,endDate.length());
                hd.endElement("","","endDate");
            }

            hd.startElement("","","userSchedulable",attrs);
            if ( te.userSchedulable ) hd.characters("True".toCharArray(),0,"True".length());
            else                      hd.characters("False".toCharArray(),0,"False".length());
            hd.endElement("","","userSchedulable");

            if ( te.repeat != null )
            {
                hd.startElement("","","TimerRepeat",attrs);
                {
                    val = "" + te.repeat.repeatType;
                    hd.startElement("","","repeatType",attrs);
                    hd.characters(val.toCharArray(),0,val.length());
                    hd.endElement("","","repeatType");

                    val = "" + te.repeat.offsetType;
                    hd.startElement("","","offsetType",attrs);
                    hd.characters(val.toCharArray(),0,val.length());
                    hd.endElement("","","offsetType");

                    val = "" + te.repeat.offsetPeriod;
                    hd.startElement("","","offsetPeriod",attrs);
                    hd.characters(val.toCharArray(),0,val.length());
                    hd.endElement("","","offsetPeriod");

                    val = "" + te.repeat.dayOfWeek;
                    hd.startElement("","","dayOfWeek",attrs);
                    hd.characters(val.toCharArray(),0,val.length());
                    hd.endElement("","","dayOfWeek");

                    if ( te.repeat.until != null )
                    {
                        String until = sdf.format( te.repeat.until.getTimeInMillis() );
                        hd.startElement("","","until",attrs);
                        hd.characters(until.toCharArray(),0,until.length());
                        hd.endElement("","","until");
                    }
                }
                hd.endElement("","","TimerRepeat");
            }

            if ( te.alarm != null )
            {
                hd.startElement("","","AlarmData",attrs);
                {
                    hd.startElement("","","state",attrs);
                    if ( te.alarm.state ) hd.characters("True".toCharArray(),0,"True".length());
                    else                  hd.characters("False".toCharArray(),0,"False".length());
                    hd.endElement("","","state");

                    int amount = 0;
                    switch ( te.alarm.type )
                    {
                        case AlarmData.N_MIN:   amount = te.alarm.amount; break;
                        case AlarmData.N_HOURS: amount = te.alarm.amount/60; break;
                        case AlarmData.N_DAYS:  amount = te.alarm.amount/(60*24); break;
                        default:                amount = te.alarm.amount; break;
                    }
                    val = "" + amount;
                    hd.startElement("","","amount",attrs);
                    hd.characters(val.toCharArray(),0,val.length());
                    hd.endElement("","","amount");

                    val = "" + te.alarm.type;
                    hd.startElement("","","type",attrs);
                    hd.characters(val.toCharArray(),0,val.length());
                    hd.endElement("","","type");
                }
                hd.endElement("","","AlarmData");
            }

            hd.endElement("","","TimerEvent");
        }

        /* Close up the document and return the XML strings. */
        hd.endElement("","","Calendar");
        hd.endDocument();
        return sw.toString();
    }

    /*
     * =======================================================
     * I/O specific methods.
     * =======================================================
     */

    /**
     * Load the legacy calendar data.  File format is as follows:
     * <ul>
     * <li> 
     * </ul>
     * @param filename  The name of the file the calendar data will be read from.
     * @return 
     * @throws Exception ...
     */
    public List<TimerEvent> load() throws Exception
    {
        java.util.List<TimerEvent> tes = new ArrayList<TimerEvent>();
        File file = new File( filename );
        if ( !file.exists() )
            return null;

        /* Set up the SAX XML handler. */
        DefaultHandler handler = new CalendarXMLHandler(tes);
        SAXParserFactory factory = SAXParserFactory.newInstance();

        /* Open and parse the file. */
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse( file, handler );
        }
        catch (SAXException sxe)
        {
            /* Error generated by this application (or a parser-initialization error) */
            throw(new IOException(Messages.S_INVALIDXML));
        }
        catch (ParserConfigurationException pce)
        {
            /* Parser with specified options can't be built */
            pce.printStackTrace();
            throw(new IOException(Messages.S_SAXPARSEERROR));
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
            throw(new IOException(Messages.S_SAXIOERROR));
        }
        catch (Throwable t) {
            t.printStackTrace();
            throw(new IOException(Messages.S_SAXXMLERROR));
        }
        return tes;
    }

    /**
     * Save calendar data.  If the filename for the note is not specified then the file name
     * is set to CacheMgr.getDataDir() + File.separator + calendar.
     * <ul>
     * <li> 
     * </ul>
     * @param ...
     * @throws Exception ...
     */
    public void save() throws Exception
    {
        File file = new File( filename );
        if ( file.exists() )
            file.delete();

        /* Get all registered events. */
        TimerManager tm = cid.getTimerManager();
        events = tm.getEvents(Calendar.appID);
        if ( (events == null) || (events.size() == 0) )
            return;

        String xml = eventsToXml();
        try {
            file.createNewFile();
            FileWriter fw = new FileWriter(file);
            PrintWriter pw = new PrintWriter(fw);
            pw.println(xml);
            fw.close();
        }
        catch (Exception e) {
            throw new Exception ( "Failed to create calendar file: " + e.getMessage() );
        }

    }

}
