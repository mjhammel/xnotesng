package com.ximba.xnotesng.apps.calendar;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.xnotes.*;

/**
 * <p> 
 * Create or edit a calendar event.
 * </p>
 * @adm $Revision: 1.10 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class NewEventDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.calendar.NewEventDialog");

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private boolean          isRunning = false;
    private boolean          isEdit = false;

    Text     eventDescription   = null;
    DateTime calendar           = null;
    DateTime startTime          = null;
    DateTime endTime            = null;
    DateTime repeatUntilDate    = null;
    Spinner  notifySpinner      = null;
    Combo    notifyCombo        = null;

    TimerEvent timerEvent = null;
    TimerEvent returnEvent = null;
    int repeatDay = java.util.Calendar.SUNDAY;
    int offsetType = -1;
    int pn_index = -1;

    public static final String S_TITLE_NEW  = "New Calendar Event";
    public static final String S_TITLE_EDIT = "Edit Calendar Event";
    public static final String S_DESC       = "Event Description";
    public static final String S_STARTTIME  = "Start Time";
    public static final String S_ENDTIME    = "End Time";
    public static final String S_REPEATOPTIONS = "Periodic Repeat Options";
    public static final String S_REPEATUNTIL = "Repeat ";
    public static final String S_ALARMOPTIONS = "Alarm Options";
    public static final String S_ALARM      = "Alarm ";
    public static final String S_NOTIFY     = "Notify within ";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_ACCEPT = 1;
    public static final int B_NOTE   = 2;
    public static final String[] bboxLabels  = {
        "Cancel",
        "Accept",
        "Note",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    public static final int B_NONE      = 0;
    public static final int B_DAILY     = 1;
    public static final int B_WEEKLY    = 2;
    public static final int B_MONTHLY   = 3;
    public static final int B_YEARLY    = 4;
    public static final String[] repeatTypes  = {
        "None",
        "Daily",
        "Weekly",
        "Monthly",
        "Yearly",
    };
    public Button[] repeatTypeButtons  = new Button[repeatTypes.length];

    public static final int B_SUN   = 0;
    public static final int B_MON   = 1;
    public static final int B_TUE   = 2;
    public static final int B_WED   = 3;
    public static final int B_THU   = 4;
    public static final int B_FRI   = 5;
    public static final int B_SAT   = 6;
    public static final String[] repeatDays  = {
        "Sun",
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
    };
    public Button[] repeatDayButtons  = new Button[repeatDays.length];

    public static final int B_FOREVER = 0;
    public static final int B_UNTIL   = 1;
    public static final String[] repeatUntil  = {
        "Forever",
        "Until",
    };
    public Button[] repeatUntilButtons  = new Button[repeatUntil.length];

    public static final int B_OFF = 0;
    public static final int B_ON  = 1;
    public static final String[] alarms  = {
        "Off",
        "On",
    };
    public Button[] alarmButtons  = new Button[alarms.length];

    public String[] notifyTypes = { "Minutes", "Hours", "Days" };

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor for a new event.
     */
    public NewEventDialog(Shell parent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
        log.info("New Event dialog w/o event");
    }

    /**
     * Constructor to edit an existing event.
     */
    public NewEventDialog(Shell parent, TimerEvent timerEvent) 
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
        isEdit = true;
        this.timerEvent = timerEvent;
        log.info("New Event dialog w/event");
    }

    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /* Enable/Disable the repeat type buttons. */
    private void enableType(boolean enable)
    {
        for (int i=0; i<repeatTypes.length; i++)
            repeatTypeButtons[i].setEnabled(enable);
    }

    /* Unselect the type buttons. */
    private void unselectType()
    {
        for (int i=0; i<repeatTypes.length; i++)
            repeatTypeButtons[i].setSelection(false);
    }

    /* Enable/Disable the repeat days buttons. */
    private void enableDays(boolean enable)
    {
        for (int i=0; i<repeatDays.length; i++)
            repeatDayButtons[i].setEnabled(enable);
    }

    /* Unselect the days buttons. */
    private void unselectDays()
    {
        for (int i=0; i<repeatDays.length; i++)
            repeatDayButtons[i].setSelection(false);
    }

    /* Enable/Disable the repeat until buttons. */
    private void enableUntil(boolean enable)
    {
        for (int i=0; i<repeatUntil.length; i++)
            repeatUntilButtons[i].setEnabled(enable);
    }

    /* Create a new note. */
    private void newNote()
    {
        pn_index = XNotes.newNote(XNotes.NOTE_SIZE_SMALL);
    }

    /* Open an existing note. */
    private void existingNote()
    {
        if ( pn_index == -1 )
        {
            newNote();
            return;
        }

        java.util.List<XNoteUI> xui = XNotes.getNotes();
        Iterator it = xui.iterator();
        while(it.hasNext())
        {
            XNoteUI xn = (XNoteUI)it.next();
            XNoteData xd = xn.getData();

            /* If found, display it. */
            if ( xd.pn_index == pn_index )
            {
                xn.show();
                return;
            }
        }

        /* If not found, create a new note. */
        newNote();
    }

    /* Remove a note. */
    private void removeNote()
    {
        if ( pn_index == -1 )
            return;

        java.util.List<XNoteUI> xui = XNotes.getNotes();
        Iterator it = xui.iterator();
        while(it.hasNext())
        {
            XNoteUI xn = (XNoteUI)it.next();
            XNoteData xd = xn.getData();

            /* If found, display it. */
            if ( xd.pn_index == pn_index )
            {
                xn.destroy();
                return;
            }
        }
        pn_index = -1;
    }

    /* Hide a note. */
    private void hideNote()
    {
        if ( pn_index == -1 )
            return;

        java.util.List<XNoteUI> xui = XNotes.getNotes();
        Iterator it = xui.iterator();
        while(it.hasNext())
        {
            XNoteUI xn = (XNoteUI)it.next();
            XNoteData xd = xn.getData();

            /* If found, display it. */
            if ( xd.pn_index == pn_index )
            {
                xn.hide();
                return;
            }
        }
    }


    /* Allow dialog to return data. */
    private boolean accept()
    {
        if ( eventDescription.getText().length() == 0 )
        {
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage( Messages.S_MISSINGDESC );
            md.open();
            return false;
        }

        /* Create a new timer event and fill it in. */
        returnEvent = new TimerEvent();
        returnEvent.name = eventDescription.getText();
        returnEvent.userSchedulable = true;
        returnEvent.appID = Calendar.appID;
        returnEvent.pn_index = pn_index;

        String[] fields = new String[2];
        fields = DateDialog.getString( calendar ).split(":");
        String calStr = fields[0];

        fields = DateDialog.getString( startTime ).split(":");
        String startStr = fields[1];

        fields = DateDialog.getString( endTime ).split(":");
        String endStr = fields[1];

        returnEvent.start = DateDialog.getCalendar( calStr + ":" + startStr );
        returnEvent.end = DateDialog.getCalendar( calStr + ":" + endStr );

        /* If a repeat is specified, create a TimerRepeatData object and fill it in. */
        if ( repeatTypeButtons[B_NONE].getSelection() == false )
        {
            TimerRepeatData trd = new TimerRepeatData();
            returnEvent.repeat = trd;

            if ( repeatTypeButtons[B_WEEKLY].getSelection() == true )
            {
                trd.repeatType = TimerRepeatData.DAYOFWEEK;
                trd.dayOfWeek = repeatDay;
                trd.offsetType = -1;
            }
            else
            {
                trd.repeatType = TimerRepeatData.OFFSET;
                trd.dayOfWeek = -1;
                trd.offsetType = offsetType;
                trd.offsetPeriod = 1;
            }

            if ( repeatUntilButtons[B_UNTIL].getSelection() == true )
                trd.until = DateDialog.getCalendar( DateDialog.getString( repeatUntilDate ) );
        }

        /* If Alarm is enabled, create an AlarmDAta event. */
        if ( alarmButtons[B_ON].getSelection() )
        {
            returnEvent.alarm = new AlarmData();
            returnEvent.alarm.state = true;
            returnEvent.alarm.amount = notifySpinner.getSelection();
            returnEvent.alarm.type = notifyCombo.getSelectionIndex() + 1;
        }

        return true;
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /* Close any dialogs that might be open. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /** 
     * Build and display the dialog.
     * @return A text string representing a name (or anything else it might be used for.)
     */
    public TimerEvent open() 
    {
        isRunning = true;

        shell = new Shell( parent );
        if ( isEdit )
            shell.setText(S_TITLE_EDIT);
        else
            shell.setText(S_TITLE_NEW);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        if ( timerEvent != null )
            fillContent();
        shell.pack();
        shell.open();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if ( !shell.getDisplay().readAndDispatch () ) shell.getDisplay().sleep();
        }

        if ( !shell.isDisposed() )
            shell.close();
        shell.dispose();
        return returnEvent;
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;
        Button button;

        /* The top level composite has 1 column. */
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);

        /* Event Description */
        Composite eventComposite = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        eventComposite.setLayoutData(gd);
        gl = new GridLayout();
        gl.marginWidth = 4;
        gl.marginHeight = 2;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 4;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        eventComposite.setLayout(gl);
        
        label = new Label(eventComposite, SWT.CENTER);
        label.setText(S_DESC);
        eventDescription = new Text(eventComposite, SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        eventDescription.setLayoutData(gd);

        /* Left Side:  calendar and time widgets. */
        Composite leftComposite = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        leftComposite.setLayoutData(gd);
        
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 2;
        gl.makeColumnsEqualWidth = false;
        leftComposite.setLayout(gl);

        /* A calendar to select the date */
        calendar = new DateTime(leftComposite, SWT.CALENDAR | SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        calendar.setLayoutData(gd);

        /* Event start time */
        label = new Label(leftComposite, SWT.CENTER);
        label.setText(S_STARTTIME);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        label.setLayoutData(gd);

        startTime = new DateTime(leftComposite, SWT.TIME | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        startTime.setLayoutData(gd);
        
        /* Set the initial start time to the next hour. */
        switch ( startTime.getHours() ) {
            case 23: startTime.setHours( 0 );
            default: startTime.setHours( startTime.getHours() + 1 ); break;
        }
        startTime.setMinutes( 0 );
        startTime.setSeconds( 0 );

        /* Event end time */
        label = new Label(leftComposite, SWT.CENTER);
        label.setText(S_ENDTIME);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        label.setLayoutData(gd);

        endTime = new DateTime(leftComposite, SWT.TIME | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        endTime.setLayoutData(gd);

        /* Set the initial end time to the start time + 59 minutes. */
        endTime.setHours( startTime.getHours() );
        endTime.setMinutes( 59 );
        endTime.setSeconds( 0 );

        /* Right Side:  Repeat and Alarm options. */
        Composite rightComposite = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        rightComposite.setLayoutData(gd);

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 5;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        rightComposite.setLayout(gl);

        /* Repeat Options */
        Group group = new Group(rightComposite, SWT.NONE);
        group.setText(S_REPEATOPTIONS);
        gl = new GridLayout();
        gl.marginWidth = 4;
        gl.marginHeight = 4;
        gl.verticalSpacing = 4;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);

        Composite repeatTypeComposite = new Composite(group, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        repeatTypeComposite.setLayoutData(gd);
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 3;
        gl.numColumns = 5;
        gl.makeColumnsEqualWidth = false;
        repeatTypeComposite.setLayout(gl);

        for (int i=0; i<repeatTypes.length; i++)
        {
            repeatTypeButtons[i] = new Button(repeatTypeComposite, SWT.RADIO);
            repeatTypeButtons[i].setText(repeatTypes[i]);

            final int idx = i;
            repeatTypeButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_WEEKLY: 
                            enableDays(true);   
                            break;
                        default:
                            enableDays(false);   
                            break;
                    }
                    switch(idx)
                    {
                        case B_NONE: 
                            enableUntil(false);   
                            repeatUntilDate.setEnabled(false);
                            repeatUntilButtons[B_FOREVER].setSelection(true);
                            repeatUntilButtons[B_UNTIL].setSelection(false);
                            break;
                        default:
                            repeatUntilDate.setEnabled(true);
                            enableUntil(true);   
                            break;
                    }
                    switch(idx)
                    {
                        case B_NONE: offsetType = -1; break;
                        case B_DAILY: offsetType = java.util.Calendar.DAY_OF_MONTH; break;
                        case B_WEEKLY: offsetType = -1; break;
                        case B_MONTHLY: offsetType = java.util.Calendar.MONTH; break;
                        case B_YEARLY: offsetType = java.util.Calendar.YEAR; break;
                    }
                }
            });
        }

        Composite repeatDayComposite = new Composite(group, SWT.NONE);
        gd = new GridData(GridData.FILL_BOTH);
        repeatDayComposite.setLayoutData(gd);
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 3;
        gl.numColumns = 7;
        gl.makeColumnsEqualWidth = false;
        repeatDayComposite.setLayout(gl);

        for (int i=0; i<repeatDays.length; i++)
        {
            repeatDayButtons[i] = new Button(repeatDayComposite, SWT.RADIO);
            repeatDayButtons[i].setText(repeatDays[i]);
            final int idx = i;
            repeatDayButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch (idx) {
                        case B_SUN: repeatDay = java.util.Calendar.SUNDAY; break;
                        case B_MON: repeatDay = java.util.Calendar.MONDAY; break;
                        case B_TUE: repeatDay = java.util.Calendar.TUESDAY; break;
                        case B_WED: repeatDay = java.util.Calendar.WEDNESDAY; break;
                        case B_THU: repeatDay = java.util.Calendar.THURSDAY; break;
                        case B_FRI: repeatDay = java.util.Calendar.FRIDAY; break;
                        case B_SAT: repeatDay = java.util.Calendar.SATURDAY; break;
                    }
                }
            });
        }

        /* Initialize periodic repeat options. */
        repeatTypeButtons[B_NONE].setSelection(true);
        repeatDayButtons[B_SUN].setSelection(true);
        enableDays(false);   

        /* Repeat Until... */
        Composite repeatUntilComposite = new Composite(group, SWT.NONE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        repeatUntilComposite.setLayoutData(gd);

        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 4;
        gl.makeColumnsEqualWidth = false;
        repeatUntilComposite.setLayout(gl);

        label = new Label(repeatUntilComposite, SWT.CENTER);
        label.setText(S_REPEATUNTIL);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        label.setLayoutData(gd);

        for (int i=0; i<repeatUntil.length; i++)
        {
            repeatUntilButtons[i] = new Button(repeatUntilComposite, SWT.RADIO);
            repeatUntilButtons[i].setText(repeatUntil[i]);
            gd = new GridData(GridData.FILL_HORIZONTAL);
            repeatUntilButtons[i].setLayoutData(gd);
            final int idx = i;
            repeatUntilButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_FOREVER: 
                            repeatUntilDate.setEnabled(false);
                            break;
                        default:
                            repeatUntilDate.setEnabled(true);
                            break;
                    }
                }
            });
        }
        repeatUntilDate = new DateTime(repeatUntilComposite, SWT.DATE | SWT.BORDER);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        repeatUntilDate.setLayoutData(gd);

        /* Initialize Repeat Until */
        repeatUntilButtons[B_FOREVER].setSelection(true);
        repeatUntilDate.setEnabled(false);
        enableUntil(false);   

        /* Alarms */
        group = new Group(rightComposite, SWT.NONE);
        group.setText(S_ALARMOPTIONS);
        gl = new GridLayout();
        gl.marginWidth = 4;
        gl.marginHeight = 4;
        gl.verticalSpacing = 4;
        gl.horizontalSpacing = 0;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        group.setLayout(gl);
        gd = new GridData(GridData.FILL_BOTH);
        group.setLayoutData(gd);

        Composite alarmComposite = new Composite(group, SWT.NONE);
        gl = new GridLayout();
        gl.marginWidth = 4;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 4;
        gl.numColumns = 3;
        gl.makeColumnsEqualWidth = false;
        alarmComposite.setLayout(gl);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        alarmComposite.setLayoutData(gd);

        label = new Label(alarmComposite, SWT.CENTER);
        label.setText(S_ALARM);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        label.setLayoutData(gd);

        for (int i=0; i<alarms.length; i++)
        {
            alarmButtons[i] = new Button(alarmComposite, SWT.RADIO);
            alarmButtons[i].setText(alarms[i]);
            gd = new GridData(GridData.FILL_HORIZONTAL);
            alarmButtons[i].setLayoutData(gd);
            final int idx = i;
            alarmButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_ON: 
                            notifySpinner.setEnabled(true);
                            notifyCombo.setEnabled(true);
                            break;
                        default:
                            notifySpinner.setEnabled(false);
                            notifyCombo.setEnabled(false);
                            break;
                    }
                }
            });
        }

        Composite notifyComposite = new Composite(group, SWT.NONE);
        gl = new GridLayout();
        gl.marginWidth = 4;
        gl.marginHeight = 0;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 4;
        gl.numColumns = 3;
        gl.makeColumnsEqualWidth = false;
        notifyComposite.setLayout(gl);

        label = new Label(notifyComposite, SWT.CENTER);
        label.setText(S_NOTIFY);
        label.setLayoutData(gd);

        notifySpinner = new Spinner(notifyComposite, SWT.BORDER | SWT.READ_ONLY);
        gd = new GridData();
        gd.widthHint = 30;
        notifySpinner.setLayoutData(gd);
        notifySpinner.setLayoutData(gd);
        notifySpinner.setMinimum(0);
        notifySpinner.setMaximum(100);
        notifySpinner.setIncrement(1);

        notifyCombo = new Combo(notifyComposite, SWT.READ_ONLY);
        gd = new GridData();
        gd.widthHint = 90;
        notifyCombo.setLayoutData(gd);
        for (int i=0; i<notifyTypes.length; i++)
            notifyCombo.add(notifyTypes[i]);
        notifyCombo.select(0);
        notifyCombo.pack();

        /* Initialize Alarms */
        alarmButtons[B_OFF].setSelection(true);
        notifySpinner.setEnabled(false);
        notifyCombo.setEnabled(false);

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 2;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            if ( !isEdit || ((pn_index != -1) && (timerEvent.pn_index == -1)) )
                                removeNote();
                            else if ( pn_index != -1 )
                                hideNote();
                            closeDialog();   
                            break;
                        case B_ACCEPT: 
                            hideNote();
                            if ( accept() )
                                closeDialog();  
                            break;
                        case B_NOTE: 
                            existingNote();
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( prevButton != null )
                fd.right = new FormAttachment(prevButton);
            else
                fd.right = new FormAttachment(100,0);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }

    /** 
     * Fill the dialog if called with initial data.
     * This should not be called unless this.timerEvent is not null.
     */
    private void fillContent() 
    {
        log.info("Setting event data: " + timerEvent.name);
        pn_index = timerEvent.pn_index;
        eventDescription.setText( timerEvent.name );

        int year, month, day;
        year = timerEvent.start.get(java.util.Calendar.YEAR);
        month = timerEvent.start.get(java.util.Calendar.MONTH);
        day = timerEvent.start.get(java.util.Calendar.DAY_OF_MONTH);
        calendar.setYear( year );
        calendar.setMonth( month );
        calendar.setDay( day );

        int hours, mins, sec;

        hours = timerEvent.start.get(java.util.Calendar.HOUR_OF_DAY);
        mins = timerEvent.start.get(java.util.Calendar.MINUTE);
        sec = timerEvent.start.get(java.util.Calendar.SECOND);
        startTime.setHours( hours );
        startTime.setMinutes( mins );
        startTime.setSeconds( sec );

        hours = timerEvent.end.get(java.util.Calendar.HOUR_OF_DAY);
        mins = timerEvent.end.get(java.util.Calendar.MINUTE);
        sec = timerEvent.end.get(java.util.Calendar.SECOND);
        endTime.setHours( hours );
        endTime.setMinutes( mins );
        endTime.setSeconds( sec );

        /* Check for timer repeat data */
        if ( timerEvent.repeat != null )
        {
            enableType(true);   
            unselectType();   
            enableDays(false);   
            enableUntil(true);   

            if ( timerEvent.repeat.repeatType == TimerRepeatData.DAYOFWEEK )
            {
                int btn = -1;
                switch (timerEvent.repeat.dayOfWeek) {
                    case java.util.Calendar.SUNDAY: btn = B_SUN; break;
                    case java.util.Calendar.MONDAY: btn = B_MON; break;
                    case java.util.Calendar.TUESDAY: btn = B_TUE; break;
                    case java.util.Calendar.WEDNESDAY: btn = B_WED; break;
                    case java.util.Calendar.THURSDAY: btn = B_THU; break;
                    case java.util.Calendar.FRIDAY: btn = B_FRI; break;
                    case java.util.Calendar.SATURDAY: btn = B_SAT; break;
                }
                enableDays(true);   
                unselectDays();   
                repeatDayButtons[btn].setSelection(true);
                repeatTypeButtons[B_WEEKLY].setSelection(true);
                repeatDay = timerEvent.repeat.dayOfWeek;
                offsetType = -1;
            }

            /* repeatType is not TimerRepeatData.DAYOFWEEK, so it must be OFFSET */
            else if ( timerEvent.repeat.offsetType == java.util.Calendar.DAY_OF_MONTH )
            {
                repeatTypeButtons[B_DAILY].setSelection(true);
                offsetType = java.util.Calendar.DAY_OF_MONTH;
            }
            else if ( timerEvent.repeat.offsetType == java.util.Calendar.MONTH )
            {
                offsetType = java.util.Calendar.MONTH;
                repeatTypeButtons[B_MONTHLY].setSelection(true);
            }
            else if ( timerEvent.repeat.offsetType == java.util.Calendar.YEAR )
            {
                offsetType = java.util.Calendar.YEAR;
                repeatTypeButtons[B_YEARLY].setSelection(true);
            }
            else 
            {
                offsetType = -1;
                repeatTypeButtons[B_NONE].setSelection(true);
            }

            if ( timerEvent.repeat.until != null )
            {
                repeatUntilButtons[B_UNTIL].setSelection(true);
                repeatUntilButtons[B_FOREVER].setSelection(false);
                String[] fields = DateDialog.getString( timerEvent.repeat.until ).split(":");
                String[] dateFields = fields[0].split("/");
                repeatUntilDate.setYear( new Integer(dateFields[0]).intValue() );
                repeatUntilDate.setMonth( new Integer(dateFields[1]).intValue() );
                repeatUntilDate.setDay( new Integer(dateFields[2]).intValue() );
                repeatUntilDate.setEnabled(true);
            }
            else
            {
                repeatUntilButtons[B_UNTIL].setSelection(false);
                repeatUntilButtons[B_FOREVER].setSelection(true);
            }
        }

        /* Check for timer alarm data */
        if ( timerEvent.alarm != null )
        {
            alarmButtons[B_ON].setSelection(timerEvent.alarm.state);
            alarmButtons[B_OFF].setSelection(!timerEvent.alarm.state);
            if ( timerEvent.alarm.state )
            {
                notifySpinner.setEnabled(true);
                notifyCombo.setEnabled(true);
            }
            notifyCombo.select( timerEvent.alarm.type - 1 );

            /* Spinner amount is based on type (minutes, hours, days) */
            if ( timerEvent.alarm.type == AlarmData.N_MIN )
                notifySpinner.setSelection( timerEvent.alarm.amount );
            else if ( timerEvent.alarm.type == AlarmData.N_HOURS )
                notifySpinner.setSelection( timerEvent.alarm.amount/60 );
            else 
                notifySpinner.setSelection( timerEvent.alarm.amount/60/24 );
        }
    }

}
