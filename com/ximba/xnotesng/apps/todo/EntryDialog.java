package com.ximba.xnotesng.apps.todo;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.*;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.browser.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.category.*;
import com.ximba.xnotesng.apps.xnotes.*;

/**
 * <p> 
 * Edit a single Todo Entry.
 * </p>
 * @adm $Revision: 1.3 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class EntryDialog extends Dialog {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.notes.EntryDialog");

    /*
     * GUI variables.
     */
    private Shell            shell = null;
    private Shell            parent = null;
    private boolean          isRunning = false;
    private boolean          isKilled = false;
    private boolean          inDate = false;
    private boolean          newEntry = false;
    private boolean          addedNote = false;

    java.util.List<CategoryData> categories = null;
    TodoData todoData = null;

    Map<String, Image> subImages = new HashMap<String, Image>();
    Image noteIcon = null;
    Image calendarIcon = null;

    Text titleText = null;
    Combo statusCombo = null;
    Spinner prioritySpinner = null;
    Combo categoryCombo = null;
    Table entryTable = null;
    Button dateButton = null;
    Button noteButton = null;

    public static final String S_TITLE          = "Edit a Todo Entry";
    public static final String S_TITLE_ENTRY    = "Todo Entry Title";
    public static final String S_STATUS         = "Status";
    public static final String S_PRIO           = "Priority";
    public static final String S_NOTE           = "Note";
    public static final String S_NOTETIP        = "Associate a note with an entry";
    public static final String S_DUEDATE        = "Due Date";
    public static final String S_DUEDATETIP     = "Set the due date for the entry";
    public static final String S_CATEGORY       = "Category";

    /* Button Box */
    public static final int B_CANCEL = 0;
    public static final int B_ACCEPT = 1;
    public static final int B_NONOTE = 2;
    public static final int B_NODATE = 3;
    public static final String[] bboxLabels  = {
        "Cancel",
        "Accept",
        "Remove Note",
        "Remove Date",
    };
    public Button[] bboxButtons  = new Button[bboxLabels.length];

    public static final int N_WORKING = 0;
    public static final int N_COMPLETED = 1;
    private static String[] statusTypes = {
        "Working",
        "Completed",
    };
    private static String[] priorityTypes = {
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
    };

    /*
     * ------------------------------------------------------------------
     * Constructors.
     * ------------------------------------------------------------------
     */

    /**
     * Constructor requires a referenct to a parent shell and a reference
     * to the displayed note object that will be searched.
     */
    public EntryDialog(Shell parent, java.util.List<CategoryData> categories)
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
        this.todoData = todoData;
        this.categories = categories;
        todoData = new TodoData();
        newEntry = true;
    }

    /**
     * Constructor requires a referenct to a parent shell and a reference
     * to the displayed note object that will be searched.
     */
    public EntryDialog(Shell parent, java.util.List<CategoryData> categories, TodoData todoData)
    {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
        this.parent = parent;
        this.todoData = todoData;
        this.categories = categories;
        if ( todoData == null )
        {
            todoData = new TodoData();
            newEntry = true;
        }
        else
        {
            XNoteData xnd = XNotes.findNote( todoData.pn_index );
            if ( xnd == null )
                todoData.pn_index = -1;
        }
    }


    /*
     * ----------------------------------------------------------------
     * Inner classes
     * ----------------------------------------------------------------
     */

    /*
     * ------------------------------------------------------------------
     * Private Methods
     * ------------------------------------------------------------------
     */

    /** Remove a created note. */
    private void noteReset()
    {
        removeNote();
        noteButton.setText(S_NOTE);
        noteButton.setImage(null);
        noteButton.redraw();
        todoData.pn_index = -1;
        addedNote = false;
    }

    /** Remove a created note. */
    private void removeNote()
    {
        /* Find the specified note. */
        XNoteData xnd = XNotes.findNote( todoData.pn_index );
        java.util.List<XNoteUI> xui = XNotes.getNotes();
        Iterator it = xui.iterator();
        while(it.hasNext())
        {
            XNoteUI xn = (XNoteUI)it.next();
            XNoteData xd = xn.getData();
            if ( xd.pn_index == todoData.pn_index )
            {
                xn.destroy();
                return;
            }
        }
    }

    /** Cleanup if cancel is selected. */
    private void cleanup()
    {
        if ( addedNote )
            removeNote();
        todoData = null;
    }

    /** Position the dialog. */
    private void positionDialog()
    {
        Point pt = Display.getDefault().getCursorLocation();
        shell.setLocation(pt);
    }

    /** Fill the TodoData object,then close the dialog. */
    private boolean accept()
    {
        if ( titleText.getText().equals("") )
        {
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage( Messages.S_MISSINGTITLE );
            md.open();
            return false;
        }

        todoData.pn_title = titleText.getText();
        todoData.pn_duedate = (Calendar) dateButton.getData();
        todoData.pn_priority = prioritySpinner.getSelection();
        if ( statusCombo.getText().equals( statusTypes[N_COMPLETED] ) )
            todoData.pn_completed = true;
        else
            todoData.pn_completed = false;

        if ( categories != null )
        {
            Iterator it = categories.iterator();
            while ( it.hasNext() )
            {
                CategoryData cd = (CategoryData) it.next();
                if ( categoryCombo.getText().equals( cd.name ) )
                {
                    todoData.pn_category = cd.id;
                    break;
                }
            }
        }
        return true;
    }

    /*
     * ------------------------------------------------------------------
     * Public Methods
     * ------------------------------------------------------------------
     */

    /** Kill the dialog immediately without saving. */
    public void killDialog()
    {
        isRunning = false;
        isKilled = true;
    }

    /** Close the dialog. */
    public void closeDialog()
    {
        isRunning = false;
    }

    /** Build and display the dialog. */
    public TodoData open() 
    {
        isRunning = true;

        Map<String, Image> images = Todo.getImages();
        noteIcon = images.get("noteIcon");
        calendarIcon = images.get("calendarIcon");

        shell = new Shell(parent);
        shell.setText(S_TITLE);
        positionDialog();

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                isRunning = false;
            }
        });

        createContents(shell);
        shell.pack();
        shell.open();
        shell.forceActive();

        /* Sit and spin until we're told to close. */
        while (isRunning) {
            if ( !Display.getDefault().readAndDispatch() ) Display.getDefault().sleep();
        }

        if ( !shell.isDisposed() )
            shell.close();
        shell.dispose();
        if ( isKilled )
            return null;
        else
            return todoData;
    }

    /** Create the dialog content. */
    private void createContents(final Shell shell) 
    {
        Label label;
        GridData gd;
        GridLayout gl;

        /* The top level composite has 5 columns. */
        gl = new GridLayout();
        gl.marginWidth = 2;
        gl.marginHeight = 2;
        gl.verticalSpacing = 0;
        gl.horizontalSpacing = 0;
        gl.numColumns = 5;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);

        /* Entry title */
        Group titleGroup = new Group(shell, SWT.NONE);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.horizontalSpan = 5;
        titleGroup.setLayoutData(gd);
        titleGroup.setText(S_TITLE_ENTRY);
        gl = new GridLayout();
        gl.marginWidth = 2;
        gl.marginHeight = 2;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = true;
        titleGroup.setLayout(gl);
        titleText = new Text(titleGroup, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        titleText.setLayoutData(gd);
        if ( todoData.pn_title != null )
            titleText.setText( todoData.pn_title );

        /* The Status option menu. */
        statusCombo = new Combo (shell, SWT.BORDER | SWT.READ_ONLY);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        statusCombo.setLayoutData(gd);
        for (int i=0; i<statusTypes.length; i++)
            statusCombo.add(statusTypes[i]);
        statusCombo.setToolTipText(S_STATUS);
        if ( todoData.pn_completed )
            statusCombo.select(N_COMPLETED);
        else
            statusCombo.select(N_WORKING);

        /* The Priority spinner menu. */
        final Spinner prioritySpinner = new Spinner (shell, SWT.BORDER | SWT.READ_ONLY);
        this.prioritySpinner = prioritySpinner;
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.widthHint = 50;
        prioritySpinner.setLayoutData(gd);
        prioritySpinner.setMinimum(1);
        prioritySpinner.setMaximum(10);
        prioritySpinner.setIncrement(1);
        prioritySpinner.setToolTipText(S_PRIO);
        try {
            if ( todoData.pn_priority != -1 )
                prioritySpinner.setSelection( todoData.pn_priority);
            else
                prioritySpinner.setSelection( prioritySpinner.getMinimum() );
        }
        catch (Exception e) {
            prioritySpinner.setSelection( prioritySpinner.getMinimum() );
        }

        /* The Category option menu. */
        categoryCombo = new Combo (shell, SWT.BORDER | SWT.READ_ONLY);
        gd = new GridData(GridData.FILL_HORIZONTAL);
        categoryCombo.setLayoutData(gd);
        Iterator it = categories.iterator();
        int index = 0;
        while ( it.hasNext() )
        {
            CategoryData cd = (CategoryData)it.next();
            categoryCombo.add(cd.name);
            if ( todoData.pn_category != null )
            {
                if ( todoData.pn_category.equals(cd.id) )
                    categoryCombo.select(index);
            }
            index++;
        }
        if ( categoryCombo.getSelectionIndex() == -1 )
            categoryCombo.select(0);
        categoryCombo.setToolTipText(S_CATEGORY);

        /* Due Date: opens DateDialog */
        final Button dateButton = new Button(shell, SWT.NONE);
        this.dateButton = dateButton;
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.widthHint = 160;
        dateButton.setLayoutData(gd);
        dateButton.setText(S_DUEDATE);
        dateButton.setToolTipText(S_DUEDATETIP);
        dateButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                if (inDate)
                    return;
                inDate = true;
                DateDialog dd = new DateDialog(shell);
                String date = dd.open();
                if (date != null)
                {
                    dateButton.setText(date);
                    dateButton.setData(dd.getCalendar());
                }
                inDate = false;
            }
        });
        if ( todoData.pn_duedate != null )
        {
            dateButton.setText( DateDialog.getString(todoData.pn_duedate) );
            dateButton.setData( todoData.pn_duedate );
        }
    
        /* Note: opens note selection dialog */
        final Button noteButton = new Button(shell, SWT.NONE);
        this.noteButton = noteButton;
        gd = new GridData(GridData.FILL_HORIZONTAL);
        gd.widthHint = 50;
        noteButton.setLayoutData(gd);
        noteButton.setToolTipText(S_NOTETIP);
        noteButton.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
                if ( todoData.pn_index == -1 )
                {
                    /* Create a new note. */
                    todoData.pn_index = XNotes.newNote(XNotes.NOTE_SIZE_SMALL);
                    noteButton.setImage(noteIcon);
                    noteButton.setText("");
                    addedNote = true;
                }
                else
                {
                    /* Find the specified note. */
                    XNoteData xnd = XNotes.findNote( todoData.pn_index );
                    java.util.List<XNoteUI> xui = XNotes.getNotes();
                    Iterator it = xui.iterator();
                    while(it.hasNext())
                    {
                        XNoteUI xn = (XNoteUI)it.next();
                        XNoteData xd = xn.getData();

                        /* If found, display it. */
                        if ( xd.pn_index == todoData.pn_index )
                        {
                            xn.show();
                            return;
                        }
                    }

                    /* If not found, create a new note. */
                    todoData.pn_index = XNotes.newNote(XNotes.NOTE_SIZE_SMALL);
                    noteButton.setImage(noteIcon);
                    noteButton.setText("");
                    addedNote = true;
                }
            }
        });
        if ( todoData.pn_index != -1 )
        {
            XNoteData xnd = XNotes.findNote( todoData.pn_index );
            java.util.List<XNoteUI> xui = XNotes.getNotes();
            it = xui.iterator();
            boolean foundIt = false;
            while(it.hasNext())
            {
                XNoteUI xn = (XNoteUI)it.next();
                XNoteData xd = xn.getData();

                /* If found, display it. */
                if ( xd.pn_index == todoData.pn_index )
                {
                    foundIt = true;
                    break;
                }
            }
            if ( foundIt )
            {
                noteButton.setImage(noteIcon);
                noteButton.setText("");
            }
            else
                noteReset();
        }
        else
            noteReset();
    

        /* -- Button box -- */
        Composite bc = new Composite(shell, SWT.BORDER);
        gd = new GridData(GridData.FILL_BOTH);
        gd.horizontalSpan = 5;
        bc.setLayoutData(gd);

        FormLayout fl = new FormLayout();
        fl.marginWidth = 0;
        fl.marginHeight = 0;
        bc.setLayout(fl);

        Button prevButton = null;
        for (int i=0; i < bboxLabels.length; i++)
        {
            final int idx = i;
            bboxButtons[i] = new Button(bc, SWT.NONE);
            bboxButtons[i].setText( bboxLabels[i] );
            bboxButtons[i].addSelectionListener(new SelectionAdapter() {
                public void widgetSelected(SelectionEvent e) {
                    switch(idx)
                    {
                        case B_CANCEL: 
                            cleanup();   
                            closeDialog();   
                            break;
                        case B_ACCEPT: 
                            if ( !accept() ) 
                                return; 
                            closeDialog();   
                            break;
                        case B_NONOTE: 
                            noteReset();
                            break;
                        case B_NODATE: 
                            dateButton.setText(S_DUEDATE);
                            dateButton.setData(null);
                            break;
                    }
                }
            });
            FormData fd = new FormData();
            fd.bottom = new FormAttachment(100,0);
            if ( i== B_CANCEL )
                fd.right = new FormAttachment(100,0);
            else
                fd.right = new FormAttachment(prevButton);
            bboxButtons[i].setLayoutData(fd);
            prevButton = bboxButtons[i];
        }
    }
}
