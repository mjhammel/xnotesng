package com.ximba.xnotesng.apps.todo;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;

/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;


/**
 * <p> 
 * Plugin class for the Todo application of XNotesNG
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class Todo extends Plugin implements ApplicationInterface {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.todo.Todo");
    private static TodoUI todoUI = null;

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;
    private static final Color defaultBG = new Color(Display.getDefault(), 242, 234, 0);

    String noteIconFile = null;
    String calendarIconFile = null;
    Image noteIcon = null;
    Image calendarIcon = null;
    static Map<String, Image> subImages = new HashMap<String, Image>();

    /** The data from the todo file. */
    java.util.List<TodoData> todos = new ArrayList<TodoData>();

    /* This Plugin ID. */
    private static final String PLUGIN_ID = "com.ximba.xnotesng.apps.todo";

    /** The ApplicationRegistration for this plugin. */
    ApplicationRegistration appReg = null;

    /*
     * =======================================================
     * Constructor
     * This plugin does not have a constructor method.
     * =======================================================
     */

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /**
     * Called when the menu entry for Todo Manager is selected.
     */
    class ShowTodo implements Callback {
        Todo parent;
        public ShowTodo(Todo parent) {
            this.parent = parent;
        }
        public void run() {
            parent.run();
        }
    }

    /**
     * Filter a list of files for those that are prefixed with
     * "todo"
     */
    class TodoFilter implements FilenameFilter {
        public boolean accept(File dir, String filename) {
            if ( filename.indexOf("todo") == 0 )
                return true;
            return false;
        }
    }

    /**
     * Autosave timer task will save any modified data on the scheduled period.
     */
    class AutoSave implements TimerCallback
    {
        Todo todo = null;
        public AutoSave(Todo todo) {
            this.todo = todo;
        }
        public void run(TimerEvent te)
        {
            Display.getDefault().asyncExec(new Runnable() {
                public void run() {
                    todo.save();
                }
            });

        }
        public void cancel(TimerEvent te){}
    }


    /*
     * =======================================================
     * Private methods
     * =======================================================
     */


    /*
     * =======================================================
     * JPF methods
     * =======================================================
     */

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStart">Plugin.doStart</a>}.
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * Not used by this plugin.
     * See {@link <a href="http://jpf.sourceforge.net/api/org/java/plugin/Plugin.html#doStop">Plugin.doStop</a>}.
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }


    /*
     * =======================================================
     * ApplicationManagerInterface methods
     * =======================================================
     */

    /**
     * Close any windows.
     */
    public void shutdown()
    {
        save();
        if ( todoUI != null )
            todoUI.close();
    }


    /**
     * This method is called once during application life cycle to allow
     * the plugin to initialize itself.
     * @param cid   A reference to the global CoreInitData object.
     */
    public void init(CoreInitData cid) throws Exception
    {
        this.cid = cid;

        /* Find the home directory for the plugin. */
        home = ApplicationManager.getHome(cid, PLUGIN_ID);

        try {
            noteIconFile = home + ApplicationManager.getAttr(cid, PLUGIN_ID, "noteIcon");
            if (!new File(noteIconFile).exists()) {
                log.error("XNotes file icon not found: filename = " + noteIconFile);
            } else {
                noteIcon = new Image(Display.getDefault(), noteIconFile);
            }
            calendarIconFile = home + ApplicationManager.getAttr(cid, PLUGIN_ID, "calendarIcon");
            if (!new File(calendarIconFile).exists()) {
                log.error("XNotes file icon not found: filename = " + calendarIconFile);
            } else {
                calendarIcon = new Image(Display.getDefault(), calendarIconFile);
            }
            subImages.put("noteIcon", noteIcon);
            subImages.put("calendarIcon", calendarIcon);
        } catch (Exception e) {
            log.error("One or more XNotse icons were not found.");
        }


        /* Read todo data. */
        File dataFolder = CacheMgr.getDataDir();
        File[] files = dataFolder.listFiles( new TodoFilter() );
        if ( (files != null) && (files.length>0) )
        {
            for (int i=0; i<files.length; i++)
            {
                if ( files[i].exists() )
                {
                    try { 
                        /* Only load the first todo file found. */
                        TodoIO tio = new TodoIO(cid);
                        todos = tio.load( files[i].getCanonicalPath() ); 
                        break;
                    }
                    catch (Exception e) {
                        log.error("Can't load todo file: " + files[i].getCanonicalPath() );
                        return;
                    }
                }
            }
        }

        /* Register auto save timer. */
        TimerRepeatData trd = new TimerRepeatData();
        trd.repeatType = TimerRepeatData.OFFSET;
        trd.offsetType = Calendar.MINUTE;
        trd.offsetPeriod = 1;
        TimerEvent te = new TimerEvent();
        te.name = "Todo Auto Save";
        te.callback = new AutoSave(this);
        te.start = Calendar.getInstance();
        te.start.add(Calendar.MINUTE, 1);
        te.repeat = trd;
        TimerManager tm = cid.getTimerManager();
        try { tm.register(te); }
        catch (Exception e) {
            log.error( e.getMessage(), e );
        }
    }

    /**
     * Register the plugin.
     * @return An ApplicationRegistration object filled with menu registrations.
     */
    public ApplicationRegistration register()
    {
        log.info("Application registration: Todo Manager.");
        ApplicationRegistration ar = new ApplicationRegistration(this);
        try {
            ar.addMenu(ApplicationManager.getAttr(cid,PLUGIN_ID,"todo"), (Callback) new ShowTodo(this));
        }
        catch (Exception e) {
            log.error("Error while registering plugin: reason = " + e.getMessage(), e);
            return null;
        }
        return ar;
    }

    /**
     * Display the Todo Manager.
     */
    public void run()
    {
        if ( todoUI != null )
            return;
        todoUI = new TodoUI(this, cid);
        todoUI.open();
    }

    /** 
     * Called by child class to let us know they are exiting.
     */
    protected void close()
    {
        todoUI = null;
    }

    /*
     * =======================================================
     * Application specific methods.
     * =======================================================
     */

    /** Provide a default color.  */
    static public Color getBG() { return defaultBG; }

    /** Retrieve the images configured for this plugin.  */
    public static Map<String, Image> getImages() { return subImages; }

    /**
     *
     * =======================================================
     * Application specific callbacks.
     * =======================================================
     */

    /**
     * Save the todo list.
     */
    private void save()
    {
        if ( todoUI != null )
            todoUI.save();
    }
}
