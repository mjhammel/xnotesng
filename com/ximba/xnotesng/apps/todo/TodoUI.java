package com.ximba.xnotesng.apps.todo;

import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;

/* SWT */
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.custom.*;
import org.eclipse.swt.printing.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.category.*;
import com.ximba.xnotesng.apps.xnotes.*;


/**
 * <p> 
 * The Todo Manager window.
 * </p>
 *
 * @adm $Revision: 1.2 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public class TodoUI {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.todo.TodoUI");

    private CoreInitData cid = null;
    Todo parent = null;

    Map<String, Image> subImages = new HashMap<String, Image>();
    Image noteIcon = null;
    Image calendarIcon = null;

    Shell shell = null;
    Text titleText = null;
    Combo statusCombo = null;
    Combo priorityCombo = null;
    Combo categoryCombo = null;
    Table entryTable = null;
    EntryDialog ed = null;

    String filterID = null;

    public static final String S_TITLE          = "Todo Manager";
    public static final String S_TITLE_ENTRY    = "Todo Entry Title";
    public static final String S_STATUS         = "Status";
    public static final String S_PRIO           = "Priority";
    public static final String S_NOTE           = "Note";
    public static final String S_NOTETIP        = "Associate a note with an entry";
    public static final String S_DUEDATE        = "Due Date";
    public static final String S_DUEDATETIP     = "Set the due date for the entry";
    public static final String S_CATEGORY       = "Category";
    public static final String S_CHOOSE         = "Choose a ";
    public static final String S_NONE           = "None";

    /* Menus */
    private static final int M_FILE_NEW       = 0;
    private static final int M_FILE_DELETE    = 1;
    private static final int M_FILE_IMPORT    = 2;
    private static final int M_FILE_SAVE      = 3;
    private static final int M_FILE_CLOSE     = 4;
    private static String[] FileMenuItems = {
        "&New",
        "&Delete",
        "&Import",
        "&Save",
        "&Close",
    };

    /* Entry Table columns */
    private static final int F_STATUS       = 0;
    private static final int F_PRIO         = 1;
    private static final int F_TITLE        = 2;
    private static final int F_NOTE         = 3;
    private static final int F_DUEDATE      = 4;
    private static final int F_CATEGORY     = 5;
    public static final String[] entryHdrs  = { S_STATUS, S_PRIO, S_TITLE_ENTRY, S_NOTE, S_DUEDATE, S_CATEGORY };
    public static final int[] entryWidths   = { 50, 50, 180, 50, 160, 120 };

    java.util.List<TodoData> todos = new ArrayList<TodoData>();
    java.util.List<CategoryData> categories = null;

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Default constructor creates the actual note.  Class methods are used to manage it.
     * @param cid       A reference to the global CoreInitData object.
     */
    public TodoUI (Todo parent, CoreInitData cid) { 

        this.parent = parent;
        this.cid = cid;

        /* Get the list of categories */
        CategoryIO cio = new CategoryIO(cid);
        categories = cio.load();

        Map<String, Image> images = Todo.getImages();
        noteIcon = images.get("noteIcon");
        calendarIcon = images.get("calendarIcon");
    }

    /**
     * Display the list.
     */
    public void open() {
        createContent();
        load();
        filllist();
        shell.open();
    }

    /**
     * Close the window.  This gets rid of the window and saves the contents.
     * Typically called when the application is exiting.
     */
    public void close () {
        if ( ed != null )
            ed.killDialog();
        ed = null;
        shell.dispose();
        parent.close();
    }

    /*
     * =======================================================
     * Private Methods
     * =======================================================
     */

    private void createContent()
    {
        Label label = null;
        GridData gd = null;
        GridLayout gl = null;

        /* Create a modeless shell. */
        shell = new Shell(Display.getDefault(), SWT.SHELL_TRIM | SWT.MODELESS);
        shell.setText(S_TITLE);

        /* When the shell window closes, we know it's time to exit. */
        shell.addListener(SWT.Close, new Listener() {
            public void handleEvent (Event e) {
                close();
            }
        });

        /* Add a menu bar */
        Menu menuBar = new Menu(shell, SWT.BAR);
        shell.setMenuBar( menuBar );

        /* Add File Menu */
        MenuItem menuBar_File= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_File.setText ("&File");
        Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_File.setMenu( fileMenu );

        for (int i=0; i<FileMenuItems.length; i++)
        {
            final int idx = i;
            MenuItem item = new MenuItem (fileMenu, SWT.CASCADE);
            item.setText( FileMenuItems[i] );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    fileMenuHandler(idx);
                }
            });
        }

        /* Create a Filter menu. */
        MenuItem menuBar_Filter= new MenuItem (menuBar, SWT.CASCADE);
        menuBar_Filter.setText ("&Filter");
        Menu filterMenu = new Menu(shell, SWT.DROP_DOWN);
        menuBar_Filter.setMenu( filterMenu );

        /* The "None" option for the filters. */
        MenuItem item = new MenuItem (filterMenu, SWT.CASCADE);
        item.setText( S_NONE );
        item.addListener (SWT.Selection, new Listener () {
            public void handleEvent (Event e) {
                filterMenuHandler(null);
            }
        });
        Iterator it = categories.iterator();
        while ( it.hasNext() )
        {
            final CategoryData cd = (CategoryData)it.next();
            item = new MenuItem (filterMenu, SWT.CASCADE);
            item.setText( cd.name );
            item.addListener (SWT.Selection, new Listener () {
                public void handleEvent (Event e) {
                    filterMenuHandler(cd);
                }
            });
        }

        /* A grid composite for the shell. */
        gl = new GridLayout();
        gl.marginWidth = 0;
        gl.marginHeight = 0;
        gl.verticalSpacing = 2;
        gl.horizontalSpacing = 2;
        gl.numColumns = 1;
        gl.makeColumnsEqualWidth = false;
        shell.setLayout(gl);


        /* The list of todos. */
        entryTable = new Table(shell, SWT.VIRTUAL | SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
        gd = new GridData(GridData.FILL_BOTH);
        gd.heightHint = 240;
        entryTable.setLayoutData(gd);
        entryTable.setHeaderVisible(true);
        entryTable.setLinesVisible(true);

        entryTable.setRedraw(false);
        for (int i=0; i<entryHdrs.length; i++)
        {
            TableColumn col;
            if ( (i == F_STATUS) || (i == F_PRIO) )
                col = new TableColumn(entryTable, SWT.CENTER);
            else
                col = new TableColumn(entryTable, SWT.LEFT);
            col.setText(entryHdrs[i]);
            col.setWidth(entryWidths[i]);
        }
        entryTable.setRedraw(true);

        entryTable.addSelectionListener(new SelectionAdapter() {
            public void widgetDefaultSelected(SelectionEvent e) {
                Table table = (Table) e.widget;
                TableItem[] rows = table.getSelection();
                if ( rows.length == 0 )
                    return;
                TableItem row = rows[0];
                TodoData data = (TodoData) rows[0].getData();
                ed = new EntryDialog(shell, categories, data);
                TodoData td = ed.open();
                if ( td == null )
                    return;

                row.setData(td);
                row.setText(F_TITLE, td.pn_title);
                row.setText(F_PRIO, ""+td.pn_priority);
                if ( td.pn_duedate != null )
                    row.setText(F_DUEDATE, DateDialog.getString(td.pn_duedate));
                else
                    row.setText(F_DUEDATE, "");

                if ( td.pn_category != null )
                {
                    try {
                        CategoryData cd = Category.findByID( td.pn_category );
                        row.setText(F_CATEGORY, cd.name);
                    }
                    catch (Exception ex) {
                        td.pn_category = null;
                    }
                }

                if ( td.pn_completed )
                    row.setText(F_STATUS, "C");
                else
                    row.setText(F_STATUS, "");

                if ( td.pn_index == -1 )
                {
                    row.setText(F_NOTE, "");
                    row.setImage(F_NOTE, null);
                }
                else
                    row.setImage(F_NOTE, noteIcon);
                save();
            }
        });


        shell.pack();
    }

    /*
     * =======================================================
     * File Menu Handlers
     * =======================================================
     */

    /**
     * Load the list of Todos.
     */
    private void load()
    {
        String filename = CacheMgr.getDataDir().getPath() + File.separator + cid.getTodoFilename();
        try {
            TodoIO todoIO = new TodoIO(cid);
            todos = todoIO.load(filename);
        }
        catch (Exception e){
            log.error(e.getMessage());
            todos = new ArrayList<TodoData>();
        }
    }

    /**
     * Fill the table, possibly filtered by category.
     */
    private void filllist()
    {
        try {
            if ( todos != null )
            {
                Iterator it = todos.iterator();
                entryTable.setRedraw(false);
                entryTable.removeAll();
                while(it.hasNext())
                {
                    TodoData td = (TodoData)it.next();
                    if ( filterID == null )
                        newEntry(td);
                    else if ( filterID.equals(td.pn_category) )
                        newEntry(td);
                }
                entryTable.setRedraw(true);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            entryTable.setRedraw(true);
        }
    }


    /**
     * File Menu handler
     */
    private void fileMenuHandler(int i)
    {
        switch (i) 
        {
            case M_FILE_NEW     : newEntry(null); break;
            case M_FILE_DELETE  : deleteEntry(); break;
            case M_FILE_IMPORT  : importFile(); break;
            case M_FILE_SAVE    : save(); break;
            case M_FILE_CLOSE   : close(); break;
        }
    }

    /**
     * Filter Menu handler
     */
    private void filterMenuHandler(CategoryData cd)
    {
        if ( cd == null ) filterID = null;
        else              filterID = cd.id;
        filllist();
    }

    /**
     * Create a new entry in the todo list.
     */
    public void newEntry(TodoData td) { 
        if ( td == null )
        {
            ed = new EntryDialog(shell, categories);
            td = ed.open();
            if ( td == null )
                return;
            todos.add(td);
            save();
        }

        TableItem row = new TableItem(entryTable, SWT.NONE);
        row.setData(td);
        row.setText(F_TITLE, td.pn_title);
        row.setText(F_PRIO, ""+td.pn_priority);
        if ( td.pn_duedate != null )
            row.setText(F_DUEDATE, DateDialog.getString(td.pn_duedate));
        else
            row.setText(F_DUEDATE, "");

        if ( td.pn_category != null )
        {
            try {
                CategoryData cd = Category.findByID( td.pn_category );
                row.setText(F_CATEGORY, cd.name);
            }
            catch (Exception e) {
                td.pn_category = null;
            }
        }

        if ( td.pn_completed )
            row.setText(F_STATUS, "C");
        else
            row.setText(F_STATUS, "");

        if ( td.pn_index == -1 )
            row.setText(F_NOTE, "");
        else
            row.setImage(F_NOTE, noteIcon);
    }

    /**
     * Save the todo list.
     */
    public void save() { 
        TodoIO tio = new TodoIO(cid);
        try {
            tio.save(todos);
        }
        catch (Exception e) {
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage( e.getMessage() );
            md.open();
        }
    }

    /** Remove a created note. */
    private void removeNote(TodoData todoData )
    {
        /* Find the specified note. */
        XNoteData xnd = XNotes.findNote( todoData.pn_index );
        java.util.List<XNoteUI> xui = XNotes.getNotes();
        Iterator it = xui.iterator();
        while(it.hasNext())
        {
            XNoteUI xn = (XNoteUI)it.next();
            XNoteData xd = xn.getData();
            if ( xd.pn_index == todoData.pn_index )
            {
                xn.destroy();
                return;
            }
        }
    }

    /**
     * Delete the current entry
     */
    private void deleteEntry( ) { 
        int index = entryTable.getSelectionIndex();
        if ( index == -1 )
            return;
        TableItem[] rows = entryTable.getSelection();
        TodoData td = (TodoData)rows[0].getData();
        removeNote( td );
        entryTable.remove( index );
        todos.remove( todos.indexOf(td) );
        save();
        filllist();
    }

    /**
     * Import a todo file.
     */
    private void importFile( ) { 
        java.util.List<TodoData> imports = null;
        FileDialog dlg = new FileDialog(shell, SWT.OPEN);
        dlg.setText(S_CHOOSE + "file");
        String filename = dlg.open();
        if ( filename == null )
            return;
        File file = new File( filename );
        if ( !file.exists() )
            return;
        if ( !file.isFile() )
        {
            MessageDialog md = new MessageDialog(shell);
            md.setMode(MessageDialog.M_ERROR);
            md.setMessage( Messages.S_NOTAFILE );
            md.open();
            return;
        }
        else
        {
            try {
                TodoIO tio = new TodoIO(cid);
                imports = tio.importer(filename);
            }
            catch (Exception e) {
                MessageDialog md = new MessageDialog(shell);
                md.setMode(MessageDialog.M_ERROR);
                md.setMessage( e.getMessage() );
                md.open();
                return;
            }
        }

        /* This should never happen. */
        if ( imports == null )
            return;

        /* If we get this far, we've got the imported data.  Add them to the list. */
        Iterator it = imports.iterator();
        while (it.hasNext())
        {
            TodoData td = (TodoData)it.next();
            todos.add(td);
        }
        save();
        filllist();
    }
}
