package com.ximba.xnotesng.apps.todo;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import org.apache.log4j.Logger;

/* SAX classes */
import org.xml.sax.*;
import org.xml.sax.helpers.*;


/* JAXP 1.1 */
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.sax.*;
import javax.xml.datatype.DatatypeFactory;


/* JPF */
import org.java.plugin.*;
import org.java.plugin.registry.*;

/* XNotesNG Imports */
import com.ximba.xnotesng.common.*;
import com.ximba.xnotesng.core.*;
import com.ximba.xnotesng.apps.category.*;
import com.ximba.xnotesng.apps.xnotes.*;


/**
 * <p> 
 * Class for loading and saving Todo files in XNotes application.
 * </p>
 *
 * @adm $Revision: 1.1.1.1 $
 * @author Michael J. Hammel
 * @since 4.0
 */

/*
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
*/

public final class TodoIO {

    private static final Logger log = Logger.getLogger("com.ximba.xnotesng.apps.todo.TodoIO");

    private static final String F_CACHE = "todos.dat";
    java.util.List<TodoData> todos = new ArrayList<TodoData>();

    /* Saved configuration data. */
    static CoreInitData cid = null;
    private String home = null;

    static final String S_ENTRYMARK     = "<%XN>";

    /*
     * =======================================================
     * Constructor
     * =======================================================
     */

    /**
     * Constructor requires reference to global CoreInitData object.
     */
    public TodoIO(CoreInitData cid) 
    {
        this.cid = cid;
    }

    /*
     * =======================================================
     * Inner Classes
     * =======================================================
     */

    /**
     * The format for entries in the old Todo file.
     */
    private class TodoEntry {
        public static final int F_ATTRIBUTE     = 0;
        public static final int F_RECORD        = 1;
        public static final int F_CATEGORY      = 2;
        public static final int F_PRIORITY      = 3;
        public static final int F_COMPLETED     = 4;
        public static final int F_DUE           = 5;
        public static final int F_DESCRIPTION   = 6;
        public static final int F_NOTE          = 7;

        int Attribute = -1;
        int Record = -1;
        String Category = null;
        int Priority = -1;
        boolean Completed = false;
        String Due = null;
        String Description = null;
        String Note = null;
    }

    /*
     * =======================================================
     * Private methods
     * =======================================================
     */

    /**
     * Start a new XML document.
     * @param writer        A StringWriter object.
     * @return A TransformerHelper object that can be used for conversion to XML.
     */
    private TransformerHandler startDoc(StringWriter writer) throws Exception
    {
        AttributesImpl attrs = new AttributesImpl();
        try {
            StreamResult streamResult = new StreamResult(writer);
            SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
            tf.setAttribute("indent-number", new Integer(4));
            TransformerHandler hd = tf.newTransformerHandler();
            Transformer serializer = hd.getTransformer();
            serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
            serializer.setOutputProperty(OutputKeys.INDENT,"yes");
            serializer.setOutputProperty(OutputKeys.METHOD, "xml");
            serializer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
            hd.setResult(streamResult);
            hd.startDocument();

            attrs.addAttribute("","","xmlns","CDATA","http://www.graphics-muse.org");
            attrs.addAttribute("xsd","xsd","xmlns:xsd","CDATA","http://www.w3.org/2001/XMLSchema");
            return hd;
        }
        catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * Convert a list of TodoData objects to an XML document.
     * @return A String object containing the XML representing the todo list.
     */
    private String toXML(List<TodoData> todos) throws Exception
    {
        if ( todos == null )
            throw new Exception(Messages.S_MISSINGTODOS);

        /* attrs must be a zero size list if the current element has no attributes. */
        AttributesImpl attrs = new AttributesImpl();
        StringWriter sw = new StringWriter();

        /* Convert the Workflow into an XML string and add to the list. */
        TransformerHandler hd = startDoc(sw);
        try {
            /* Start the list of items. */
            hd.startElement("","","XNTodo",attrs);

            Iterator it = todos.iterator();
            while (it.hasNext())
            {
                String val = null;
                TodoData td = (TodoData) it.next();

                /* Save the completed state of the todo entry. */
                attrs.addAttribute("","","completed","CDATA", Boolean.toString(td.pn_completed) );

                /* Start an item */
                hd.startElement("","","Item",attrs);

                /* Reset the attributes list. */
                attrs.clear();

                /* Title */
                hd.startElement("","","Title",attrs);
                hd.characters(td.pn_title.toCharArray(),0,td.pn_title.length());
                hd.endElement("","","Title");

                /* Priority */
                val = new Integer(td.pn_priority).toString();
                hd.startElement("","","Priority",attrs);
                hd.characters(val.toCharArray(), 0, val.length());
                hd.endElement("","","Priority");

                /* Due Date */
                if ( td.pn_duedate != null )
                {
                    val = DateDialog.getString(td.pn_duedate);
                    hd.startElement("","","DueDate",attrs);
                    hd.characters(val.toCharArray(), 0, val.length());
                    hd.endElement("","","DueDate");
                }

                /* Category */
                if ( td.pn_category != null )
                {
                    hd.startElement("","","Category",attrs);
                    hd.characters(td.pn_category.toCharArray(),0,td.pn_category.length());
                    hd.endElement("","","Category");
                }

                /* Note index */
                val = new Integer(td.pn_index).toString();
                hd.startElement("","","NoteIndex",attrs);
                hd.characters(val.toCharArray(), 0, val.length());
                hd.endElement("","","NoteIndex");

                /* End the item */
                hd.endElement("","","Item");

            }

            /* End the list of items. */
            hd.endElement("","","XNTodo");
            hd.endDocument();
            return sw.toString();
        }
        catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }



    /*
     * =======================================================
     * I/O specific methods.
     * =======================================================
     */

    /**
     * Load the todo file.  The file is formatted in XML.
     * File format is as follows:
     * <ul>
     * <li> tbd. </li>
     * </ul>
     * @param filename  The name of to file the data will be read from.
     * @return A list of TodoData objects from the data loaded from file.
     * @throws Exception if filename does not exist or can't be read.
     */
    public java.util.List<TodoData> load(String filename) throws Exception
    {
        final List<TodoData> todos = new ArrayList<TodoData>();

        /* The parse handler */
        class TodoXMLHandler extends DefaultHandler {

            /* Element names and field indexes. */
            private static final int f_Item         = 0;
            private static final int f_Title        = 1;
            private static final int f_Priority     = 2;
            private static final int f_Completed    = 3;
            private static final int f_Duedate      = 4;
            private static final int f_Category     = 5;
            private static final int f_NoteIndex    = 6;

            /*
             * The set of elements that we recognize.
             * These must match the order of the f_XXX indices listed above.
             * None of these have subelements, so this is very simple.
             */
            private String[] elementNames = {
                "Item",
                "Title",
                "Priority",
                "Completed",
                "Duedate",
                "Category",
                "NoteIndex",
            };


            /* Where we store the parsed data. */
            TodoData td = null;
        
            /* These keep track of our elements and their values. */
            private String       currentElement   = null;
            private List<String> currentAttr      = new ArrayList<String>();
            private List<String> currentAttrValue = new ArrayList<String>();
            private StringBuffer textBuffer       = null;
        

            /* Start of document - do nothing. */
            public void startDocument() throws SAXException { }
        
            /* End of document - do nothing. */
            public void endDocument() throws SAXException { }
        
            /*
             * When encountering the start of an element we
             *  1. parse the element name,
             *  2. clear any previous element value we'd accumulated
             *  3. make note of which element has started.
             * We also keep a list of all attributes provided with this element.
             */
            public void startElement(
                    String namespaceURI,
                    String sName,               // simple name
                    String qName,               // qualified name
                    Attributes attrs)
                    throws SAXException
            {
                /* Reset the textBuffer, which is the value this element will hold. */
                textBuffer = null;
        
                /* element name */
                String eName = sName;
        
                /* not namespace-aware */
                if ("".equals(eName))
                    eName = qName;
        
                /* Save the current element name. */
                currentElement = eName;
                currentAttr.clear();
                currentAttrValue.clear();
                if (attrs != null) {
                    for (int i = 0; i < attrs.getLength(); i++) {
        
                        /* Attr name */
                        String aName = attrs.getLocalName(i);
        
                        if ("".equals(aName))
                            aName = attrs.getQName(i);
        
                        currentAttr.add(aName);
                        currentAttrValue.add(attrs.getValue(i));
                    }
                }
                cmdStart(currentElement);
            }

            /*
             * When the end of an element is reached we note which
             * element that is, process the element value accumulated so far,
             * and then clear the element value.
             */
            public void endElement(
                    String namespaceURI,
                    String sName,           // simple name
                    String qName            // qualified name
                    )
                    throws SAXException
            {
                /* Process this command. */
                if ( textBuffer != null )
                    processCmd(textBuffer.toString());
        
                /* element name */
                String eName = sName;
        
                /* not namespace-aware */
                if ("".equals(eName))
                    eName = qName;
        
                /* Reset state, if necessary. */
                cmdStop(eName);
                textBuffer = null;
            }

            /*
             * This method is what accumulates the set of characters stuffed into
             * the element value.  It is essentially anything between the start
             * and end elements.
             */
            public void characters(char buf[], int offset, int len) throws SAXException
            {
                String s = new String(buf, offset, len);
                if (textBuffer == null) {
                    textBuffer = new StringBuffer(s);
                }
                else {
                    textBuffer.append(s);
                }
            }
        
            /* treat validation errors as fatal */
            public void error(SAXParseException e) throws SAXParseException
            {
                throw e;
            }
        
            /* ignore warnings */
            public void warning(SAXParseException err) throws SAXParseException { }
        
        
            /*
             * ----------------------------------------------------------------
             * Process the start of a commands.
             * ----------------------------------------------------------------
             */
            private void cmdStart(String element)
            {
                if (element.equalsIgnoreCase(elementNames[f_Item]) )
                {
                    td = new TodoData();
                    for (int i=0; i<currentAttr.size(); i++)
                    {
                        String attr = (String)currentAttr.get(i);
                        String attrValue = (String)currentAttrValue.get(i);

                        /* The type is valid for both in and out of subscriber elements. */
                        if ( attr.equalsIgnoreCase("Completed") )
                        {
                            if ( attrValue.equalsIgnoreCase("True") )
                                td.pn_completed = true;
                        }
                    }
                }
            }
        
            /*
             * ----------------------------------------------------------------
             * Process the values that elements encompass.  It should only be valid
             * for elements that do not have subelements.
             * ----------------------------------------------------------------
             */
            private void processCmd(String value)
            {
                /*
                 * We'll get called when an element closes, a newline/tab is encountered and then
                 * another element closes.  In that case, we just ignore the request.
                 */
                String currentValue = value;
                Matcher matcher = Pattern.compile("\\n*\\t*\\s*").matcher(value);
                String testValue = matcher.replaceAll("");
                if ( testValue.length() == 0 )
                    return;
        
                /* Title */
                if ( currentElement.equalsIgnoreCase(elementNames[f_Title]) )
                {
                    td.pn_title = currentValue.trim();
                    return;
                }

                /* Priority */
                if ( currentElement.equalsIgnoreCase(elementNames[f_Priority]) )
                {
                    try { td.pn_priority = Integer.parseInt(currentValue.trim()); }
                    catch (Exception e){ }
                    return;
                }

                /* Due Date */
                if ( currentElement.equalsIgnoreCase(elementNames[f_Duedate]) )
                {
                    if ( currentValue.trim().length() > 0 )
                        td.pn_duedate = DateDialog.getCalendar(currentValue.trim());
                    return;
                }

                /* Category */
                if ( currentElement.equalsIgnoreCase(elementNames[f_Category]) )
                {
                    td.pn_category = currentValue.trim();
                    return;
                }

                /* Index */
                if ( currentElement.equalsIgnoreCase(elementNames[f_NoteIndex]) )
                {
                    try { td.pn_index = Integer.parseInt(currentValue.trim()); }
                    catch (Exception e){ }
                    return;
                }
            }
        
            /*
             * ----------------------------------------------------------------
             * When an element ends, we might need to reset our state.
             * ----------------------------------------------------------------
             */
            private void cmdStop(String element)
            {
                if (element.equalsIgnoreCase(elementNames[f_Item]) )
                    todos.add(td);
            }
        }

        /* If no file is passed in, do nothing. */
        if ( filename == null )
            throw new Exception(Messages.S_NOSUCHFILE + filename);

        /* Make sure file exist. */
        File file = new File(filename);
        if ( !file.exists() )
            throw new Exception(Messages.S_NOSUCHFILE + filename);

        /* Set up the SAX XML handler. */
        DefaultHandler handler = new TodoXMLHandler();
        SAXParserFactory factory = SAXParserFactory.newInstance();

        /* Open and parse the file. */
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse( file, handler ); 
        }
        catch (Exception e) {
            throw new Exception(Messages.S_INVALIDFILEFORMAT);
        }
        catch (Throwable t) {
            throw new Exception("SAX XML Parse error");
        }
        return todos;
    }

    /**
     * Save the todo file to the default todo filename.   
     * The file is formatted in XML.
     * <ul>
     * <li> tbd. </li>
     * </ul>
     * @param todos    A list of TodoData objects representing the todo list to save.
     * @throws Exception ...tbd...
     */
    public void save(java.util.List<TodoData> todos) throws Exception
    {
        save(todos, cid.getTodoFilename());
    }

    /**
     * Save the todo file to the named file.  
     * The file is formatted in XML.
     * <ul>
     * <li> tbd. </li>
     * </ul>
     * @param todos    A list of TodoData objects representing the todo list to save.
     * @throws Exception ...tbd...
     */
    public void save(java.util.List<TodoData> todos, String filename) throws Exception
    {
        String data = toXML(todos);

        if ( !filename.substring(0,1).equals(File.separator) )
            filename = CacheMgr.getDataDir().getPath() + File.separator + filename;

        File saveFile = new File(filename);
        if ( saveFile.exists() )
            saveFile.delete();

        saveFile.createNewFile();
        FileWriter fw = new FileWriter(saveFile);
        PrintWriter pw = new PrintWriter(fw);
        pw.println(data);
        fw.close();
    }

    /**
     * Import the pre-4.0 formatted todo file.  The format is as follows:
     * <ul>
     * <li> Magic </li>
     * <li> One or more category names </li>
     * <li> One or more todo entries </li>
     * </ul>
     * Each entry has the following elements, each prefixed with the "<%XN>" marker. Note
     * that some of these fields are left over from supporint sync with a Pilot, which is
     * no longer supported as of XNotesNG V4.0.
     * <ul>
     * <li> Attribute: int </li>
     * <li> Record: int </li>
     * <li> Category: name </li>
     * <li> Priority: int </li>
     * <li> Completed: Yes or No </li>
     * <li> Due: "No Date" or "2009-02-07 (Sat)" </li>
     * <li> Description: multi-line value </li>
     * <li> Note: multi-line value </li>
     * </ul>
     * The description element is actually the todo title.
     * To do the import, each element is parsed and mapped to corresponding field, if any, of a new TodoData object.
     * If Note text exists (strip white space and newlines to see if it does) it is converted into a new note.
     * If the category does not exist, it is created.
     * @param filename    The name of the file to import.  It must be prefixed with the magic header.
     * @throws Exception if the file cannot be read or if errors are encountered while importing.
     */
    public java.util.List<TodoData> importer(String filename) throws Exception
    {
        File file = new File( filename );
        if ( !file.exists() )
            throw new Exception( Messages.S_NOSUCHFILE + filename );

        /* Read the file into a list, with the config line held separate. */
        StringBuilder todoText = new StringBuilder("");
        String magic = null;
        List<String> categories = new ArrayList<String>();
        TodoEntry entry = null;
        List<TodoEntry> entries = new ArrayList<TodoEntry>();
        String line = null;
        boolean startedEntry = false;
        boolean inEntry = false;
        boolean inDesc = false;
        int fieldID = TodoEntry.F_ATTRIBUTE;
        StringBuilder str = new StringBuilder("");
        try {
            FileReader fr = new FileReader(file);
            BufferedReader in = new BufferedReader(fr);

            while ( (line=in.readLine()) != null )
            {
                /* If the magic line has not been read, start with that. */
                if ( magic == null ) magic = line;

                /* If we haven't started entries yet, add the line to the categories. */
                else if ( !startedEntry ) {
                    if ( line.indexOf(S_ENTRYMARK) == 0 )
                    {
                        startedEntry = true;
                        entry = new TodoEntry();
                    }
                    else 
                        categories.add(line);
                }

                /* We've started processing entries. Process based on field position. */
                if ( startedEntry ) {
                    String[] fields = line.split(":");
                    String data;
                    if ( fields.length == 2 ) data = fields[1].trim();
                    else                      data = line.trim();
                    switch(fieldID) {
                        case TodoEntry.F_ATTRIBUTE:
                            entry.Attribute = Integer.parseInt(data);
                            fieldID++;
                            break;
                        case TodoEntry.F_RECORD:
                            entry.Record = Integer.parseInt(data);
                            fieldID++;
                            break;
                        case TodoEntry.F_CATEGORY:
                            entry.Category = data;
                            fieldID++;
                            break;
                        case TodoEntry.F_PRIORITY:
                            entry.Priority = Integer.parseInt(data);
                            fieldID++;
                            break;
                        case TodoEntry.F_COMPLETED:
                            if ( data.equalsIgnoreCase("Yes") ) entry.Completed = true;
                            else                                entry.Completed = false;
                            fieldID++;
                            break;
                        case TodoEntry.F_DUE:
                            entry.Due = data;
                            fieldID++;
                            break;
                        case TodoEntry.F_DESCRIPTION:
                            if ( !inDesc )
                            {
                                inDesc = true;
                                str.append(data);
                            }
                            else {
                                if ( line.indexOf(S_ENTRYMARK) == -1 )
                                    str.append("\n" + data);
                                else {
                                    entry.Description = str.toString();
                                    inDesc = false;

                                    /* Grab the initial data for the Note field. */
                                    str = new StringBuilder("");
                                    str.append(data);
                                    fieldID++;
                                }
                            }
                            break;
                        case TodoEntry.F_NOTE:
                            if ( line.indexOf(S_ENTRYMARK) == -1 )
                                str.append("\n" + data);
                            else {
                                entry.Note = str.toString();
                                entries.add( entry );
                                entry = new TodoEntry();

                                /* We already have it, so save the data for the Attribute field. */
                                str = new StringBuilder("");
                                fieldID = TodoEntry.F_ATTRIBUTE;
                                entry.Attribute = Integer.parseInt(data);
                                fieldID++;
                            }
                            break;
                    }
                }
            }
            fr.close();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception( Messages.S_READFAIL + filename + ": " + e.getMessage() );
        }

        System.err.println("Magic: " + magic );
        System.err.println("Number of categories parsed: " + categories.size() );
        System.err.println("Number of todo entries parsed: " + entries.size() );

        /* Create missing categories */
        Iterator it = categories.iterator();
        while ( it.hasNext() )
        {
            String category = (String)it.next();
            CategoryData cd = new CategoryData();
            cd.name = category;
            Category.addCategory(cd);
        }

        /* Convert the TodoEntry's to TodoData */
        it = entries.iterator();
        while ( it.hasNext() )
        {
            TodoEntry te = (TodoEntry)it.next();
            TodoData td = new TodoData();
            td.pn_title = te.Description;
            td.pn_priority = te.Priority;
            td.pn_completed = te.Completed;

            /* Find the associated category id, or remove the category if not found. */
            if ( te.Category != null )
            {
                CategoryData cd = Category.findByName(te.Category);
                td.pn_category = cd.id;
            }

            /* Create a new note, if necessary. */
            if ( te.Note.length() > 0 )
            {
                td.pn_index = XNotes.newNote(XNotes.NOTE_SIZE_SMALL);

                /* find the ui */
                java.util.List<XNoteUI> xui = XNotes.getNotes();
                Iterator xit = xui.iterator();
                while(xit.hasNext())
                {
                    XNoteUI xn = (XNoteUI)xit.next();
                    XNoteData xd = xn.getData();

                    /* If found, update its text. */
                    if ( xd.pn_index == td.pn_index )
                    {
                        xn.setText( te.Note );
                        xn.setName( td.pn_title );
                        xn.hide();
                        break;
                    }
                }
            }
            todos.add( td );
        }
        return todos;
    }

}
