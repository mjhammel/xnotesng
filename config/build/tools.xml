<!-- =======================================================================
   - Build Tools - these just provide some extra helpers.
   -
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
   - ======================================================================= -->

    <property name="title.tools"       value="Build Tools"/>
    <property name="tools.dep.file"    value="${dependency.dir}/tools.dep"/>
    <property name="build.dir.tools"   value="${build.dir}/tools"/>
    <property name="classname.tools"   value="tools/genGuid"/>


    <!-- =============================================================
    - client.*.classpath is where you find the JBOSS client
    -    class libraries.
    ============================================================= -->
    <path id="classpath.tools">
        <pathelement location="${build.dir.common}"/>
        <pathelement location="${build.dir.tools}"/>
    </path>

    <!-- =============================================================
    - Print out useful messages.
    ============================================================= -->
    <target name="-YesBT" if="tools.Required">
        <antcall target="-YesTS"> <param name="target.name" value="${title.tools}"/> </antcall>
    </target>
    <target name="-NoBT" unless="tools.Required">
        <antcall target="-NoTS"> <param name="target.name" value="${title.tools}"/> </antcall>
    </target>
    <target name="-CompleteBT" if="tools.Required">
        <antcall target="-CompleteTS"> <param name="target.name" value="${title.tools}"/> </antcall>
    </target>

    <!-- =============================================================
    - Check if the source has been updated.
    ============================================================= -->
    <target name="-testBT" depends="">
        <!-- Test if the source needs to be rebuilt -->
        <condition property="tools.Required">
            <or>
                <!-- If we've never built the client before, we will now. -->
                <not> <available file="${tools.dep.file}" /> </not>
    
                <!-- If any of the source was touched, we rebuild. -->
                <not> 
                    <!-- Tested backwards, so we invert the result of the test. -->
                    <uptodate targetfile="${tools.dep.file}" >
                        <srcfiles dir="${src.dir.tools}" includes="**/*.java" />
                    </uptodate>
                </not> 
            </or>
        </condition>
    </target>

    <!-- =============================================================
    - Compile the various clients.
    ============================================================= -->
    <target name="-btTestCompile" if="tools.Required">
        <mkdir dir="${build.dir.tools}"/>
        <javac srcdir="${src.dir.tools}" destdir="${build.dir.tools}" debug="true" deprecation="on">
            <classpath>
                <path refid="classpath.jakarta"/>
                <path refid="classpath.tools"/>
            </classpath>
            <include name="**/*.java"/>
            <compilerarg value="-Xlint:unchecked"/>
        </javac>
    </target>

    <!-- =============================================================
    - A Front End to building the client code.
    ============================================================= -->
    <target name="tools" 
        description="A set of tools for use by developers."
        depends="init, -testBT, -YesBT, -NoBT, -btTestCompile, -CompleteBT" >
    </target>
    <target name="tools.clean" description="Force build tools rebuild">
        <delete file="${tools.dep.file}"/>
    </target>
    
    <target name="tools.gen.guid" 
        description="Generate a GUID string."
        depends="tools">
        <java dir="${build.dir.tools}" classname="${classname.tools}" fork="true" failonerror="true">
            <classpath>
                <path refid="classpath.jakarta"/>
                <path refid="classpath.tools"/>
            </classpath>
            <arg value="-g" />
        </java>
    
    </target>
    
