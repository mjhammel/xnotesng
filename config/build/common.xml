<!-- =======================================================================
   - Compile common code:
   -
   - Copyright (c) 2009 Michael J. Hammel
   -
   - Permission is hereby granted, free of charge, to any person obtaining a copy
   - of this software and associated documentation files (the "Software"), to deal
   - in the Software without restriction, including without limitation the rights
   - to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   - copies of the Software, and to permit persons to whom the Software is
   - furnished to do so, subject to the following conditions:
   -
   - The above copyright notice and this permission notice shall be included in
   - all copies or substantial portions of the Software.
   -
   - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   - IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   - FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   - AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   - LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   - OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   - THE SOFTWARE.
   - ======================================================================= -->

    <property name="title.common"     value="Common Code"/>
    <property name="dep.file.common"  value="${dependency.dir}/common.dep"/>
    <property name="build.dir.common" value="${build.dir}/common"/>

<!-- =============================================================
   - Print out useful messages about whether or not we rebuilt the code.
   - ============================================================= -->

    <target name="-YesCMN" if="Required.common">
        <antcall target="-YesTS"> <param name="target.name" value="${title.common}"/> </antcall>
    </target>
    <target name="-NoCMN" unless="Required.common">
        <antcall target="-NoTS"> <param name="target.name" value="${title.common}"/> </antcall>
    </target>
    <target name="-CompleteCMN" if="Required.common">
        <antcall target="-CompleteTS"> <param name="target.name" value="${title.common}"/> </antcall>
    </target>


<!-- =============================================================
   - Check if the common source has been updated.
   - ============================================================= -->

   <target name="-testCommon">
        <condition property="Required.common">
            <or>
                <!-- If we've never built the server before, we will now. -->
                <not> <available file="${dep.file.common}" /> </not>

                <!-- If any of the common source was touched, we rebuild. -->
                <!-- Tested backwards, so we invert the result of the test. -->
                <not> 
                    <uptodate targetfile="${dep.file.common}" >
                        <srcfiles dir="${src.dir.common}" 
                            includes="**/*.java" 
                            />
                    </uptodate>
                </not> 
            </or>
        </condition>
    </target>


<!-- =============================================================
   - Compile the common code.
   - ============================================================= -->

    <target name="-common" if="Required.common">
        <mkdir dir="${build.dir.common}"/>
        <javac srcdir="${src.dir.common}" destdir="${build.dir.common}" debug="true" deprecation="off">
            <include name="*.java"/>
            <compilerarg value="-Xlint:unchecked"/>
            <classpath>
                <path refid="classpath.jakarta"/>
            </classpath>
        </javac>
        <jar jarfile="${build.dir.common}/xnotesng-common.jar" >
            <fileset dir="${build.dir.common}">
                <include name="**/*.class"/>
            </fileset>
        </jar>
        <touch file="${dep.file.common}"/>
    </target>

    <!-- A Front End to building the common code. -->
   <target name="common" description="Compile common source code."
            depends="init, -createBuildInfo, -testCommon, -YesCMN, -NoCMN, -common, -CompleteCMN" >
   </target>

   <target name="common.clean" description="Clean up after the common files compile." >
        <delete file="${src.dir.common}/BuildInfo.java" failonerror="false"/>
   </target>


